<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Clean up tasks Knime](#clean-up-tasks-knime)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Clean up tasks Knime

Created: 2020-04-15 12:17:16 +0200

Modified: 2020-04-15 12:17:31 +0200

---


    -   Tout nettoyer

    -   Calibration a inclure dans le dashboard
-   Lifestyle

    -   Calibration dans le dashboard
-   Elec

    -   Calibration dans le dashboard
-   Land-use

    -   Documenter la calibration des émissions (Vincent)
-   Agriculture

    -   Reprendre module actuel EUCalc?
