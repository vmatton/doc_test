<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Etat des lieux](#etat-des-lieux)
- [Dashboards](#dashboards)
  - [EUCalc](#eucalc)
  - [BECalc](#becalc)
- [Logique d'implémentation actuelle](#logique-dimpl%C3%A9mentation-actuelle)
  - [Dans le metanode secteur](#dans-le-metanode-secteur)
  - [Dans le workflow](#dans-le-workflow)
  - [Archétypes](#arch%C3%A9types)
- [État des lieux](#%C3%A9tat-des-lieux)
- [Axes d'amélioration](#axes-dam%C3%A9lioration)
- [Todo](#todo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Etat des lieux

Created: 2020-04-15 12:16:25 +0200

Modified: 2020-10-09 13:52:19 +0200

---


# Dashboards

## EUCalc

![Calibration Activities Dimension non-residential: CH4 non-residential: C02 non-residential: N20 non-residential: hot-water non-residential: lighting non-residential: space-cooling non-residential: space-heating residential: CH4 residential: C02 residential: N20 residential: appliances residential: cooking residential: hot-water residential: lighting residential: space-cooling residential: space-heating 142% 1638% Max weight Comme Mean Min 749769212! 10100% 114% 58307602% 100% 100% 100% 101% 650379291' 173% 115212341' 437% 100% 100% 100% 100% 81% 32727510} 4% 288% 0% 2618 100% 100% 100% 100% 100% 100% 37% 326% 100% 567900000 3007% 0% 356000000 171% 826% 75% 100% 100% 100% 100% 100% 100% 100% 64% 115% 0% 27% 6% 8% 2% 22% 0% 0% 1% 6% 13% 1% 0% 41% ](../media/Calibration-Etat-des-lieux-image1.png){width="5.385416666666667in" height="4.520833333333333in"}



## BECalc

![Fi Dimenson 4 alumimum cgnent ceramic Dimenson 5 D,'7Ensnn 3 ccmbustion-CH4 canbustion-C02 combustion-N20 process-CH4 prtxess-C02 prcxess-N20 chemical-chlorine cnemical-olefin chemical-other glass lime nm-ferrcws other-inclustries steel wood 1 owo 307% 63% 141% 380% 246% 84% 13121% 100% 123% 14 118% 37% 18% 391% 1021 0.0% 0.0% 00/0 160.1 100. 49 _ 0.0% 0.0% 00,'0 0.0% 0.0% 0.0% 00/0 ](../media/Calibration-Etat-des-lieux-image2.png){width="8.395833333333334in" height="3.59375in"}



# Logique d'implémentation actuelle

## Dans le metanode secteur

![C 』 ra Califstdn Of m 0 ](../media/Calibration-Etat-des-lieux-image3.png){width="2.1875in" height="1.8333333333333333in"}

Les outputs inférieurs "cal_rate" sont tous groupés avec des Joiner avant d'être extraits par un port dédié.



## Dans le workflow

Il existe un metanode qui écrit les cal_rates de façon standardisée dans une Google Sheet:

![Machine generated alternative text: ](../media/Calibration-Etat-des-lieux-image4.png){width="1.2083333333333333in" height="1.0729166666666667in"}



## Archétypes
-   Calibration directe: les données sortantes sont les données calibrées, c'est-à-dire les années passées sont égales aux données de calibration, et les données après la baseyear ont été multipliées par le calibration rate de la baseyear.
-   Calibration indirecte: les cal_rates sont appliquées à d'autres variables que celles qui ont servi à la calibration. Par exemple, dans agriculture, on calibre le total des terres agricole, puis on applique les cal_rates aux pâtures et champs séparément.
-   Calibration indirecte partielle: un mix des deux approches ci-dessus.



# État des lieux





<table><colgroup><col style="width: 15%" /><col style="width: 11%" /><col style="width: 12%" /><col style="width: 61%" /></colgroup><thead><tr class="header"><th><strong>Secteur</strong></th><th><strong>Calibration</strong></th><th><strong>Archétype</strong></th><th><strong>Statut</strong></th></tr></thead><tbody><tr class="odd"><td>Industry/Ammonia</td><td>Product demand</td><td>Directe</td><td><blockquote><p></p></blockquote></td></tr><tr class="even"><td></td><td>Material demand</td><td>Directe</td><td><blockquote><p></p></blockquote></td></tr><tr class="odd"><td></td><td>Energy</td><td>Directe</td><td><blockquote><p></p></blockquote></td></tr><tr class="even"><td></td><td>Emissions</td><td>Indirecte (calibration sur material appliquée ensuite sur material-technology)</td><td><blockquote><p></p></blockquote></td></tr><tr class="odd"><td>Agriculture</td><td>Activity: demande agricole livestock</td><td></td><td><blockquote><p></p></blockquote></td></tr><tr class="even"><td></td><td>Activity: population livestock</td><td></td><td>Output pas utilisé dans le workflow: Amaury</td></tr><tr class="odd"><td></td><td>Activity: feed consumption</td><td></td><td>Output pas utilisé dans le workflow</td></tr><tr class="even"><td></td><td>Activity: crop production</td><td></td><td>Output pas utilisé dans le workflow</td></tr><tr class="odd"><td></td><td>Residues</td><td>Indirecte (calibré sur somme des résidus, ensuite appliqué sur liquide/solide)</td><td><blockquote><p></p></blockquote></td></tr><tr class="even"><td></td><td>Agriculture land</td><td></td><td>Output pas utilisé dans le workflow</td></tr><tr class="odd"><td></td><td>Energy</td><td>Directe</td><td><blockquote><p></p></blockquote></td></tr><tr class="even"><td><blockquote><p></p></blockquote></td><td>Emissions</td><td>Indirecte partielle (calibration par gaz-usage, le facteur CO2 est ensuite appliqué sur les vecteurs de l'energy demand)</td><td><blockquote><p></p></blockquote></td></tr><tr class="odd"><td>Land use</td><td>Agricultural land</td><td>Indirecte (calibration sur total agricultural land appliquée sur pastures et crop)</td><td><blockquote><p></p></blockquote></td></tr><tr class="even"><td></td><td>Emissions</td><td><blockquote><p>Pas clair</p></blockquote></td><td><blockquote><p>Pas dans la feuille RAW ni dashboard</p><p>Très mal documenté: <strong>Vincent</strong></p><p><img src="../media/Calibration-Etat-des-lieux-image5.png" style="width:4.08333in;height:1.63542in" alt="&#39; : 一 q = uo!ss!tua 、 00 " /></p></blockquote></td></tr><tr class="odd"><td>Lifestyles</td><td>Food demand</td><td>Directe</td><td><blockquote><p>Facteurs pas envoyés vers le dashboard, utilise encore le node EUCalc: Michel?</p><p>Pas de commentaires sur les metanodes, on ne sait pas sur quoi on calibre</p></blockquote></td></tr><tr class="even"><td></td><td>Food waste</td><td>Directe</td><td>Idem</td></tr><tr class="odd"><td></td><td>Passenger-km demand</td><td>Directe</td><td>Idem</td></tr><tr class="even"><td></td><td>Residential floor area</td><td>Directe</td><td>Idem</td></tr><tr class="odd"><td>Transport</td><td>Activity: pkm passenger</td><td>Indirecte (calibre sur demande en Gpkm pour road-private, road-public, rail, aviation puis appliqués sur les modes)</td><td><blockquote><p>Facteurs pas envoyés vers le dashboard</p><p>Duplication avec calibration dans lifestyles?</p></blockquote></td></tr><tr class="even"><td></td><td>Activity: tkm freight</td><td>Directe</td><td><blockquote><p>Facteurs pas envoyés vers le dashboard</p><p>Calibration directe mais implémentée comme indirecte (relation 1-1 entre facteurs et variables calibrées)</p><p>Remarque indiquant que calibration LDV n'est pas utilisée</p></blockquote></td></tr><tr class="odd"><td></td><td>Energy</td><td>Indirect (calibrate energy demand by vector puis appliquée par mode-tech)</td><td><blockquote><p>Facteurs pas envoyés vers le dashboard</p><p>Les 2 metanodes pour appliquer les facteurs de calibration pourraient être réduits à 1 tree split node</p><p>Manque de commentaires de nodes dans le cadre calibration</p><p>Box d'erreur rouge non résolue?</p></blockquote></td></tr><tr class="even"><td><blockquote><p></p></blockquote></td><td>Emissions</td><td>Indirect (calibration sur gaz-usage (domestic, bunkers aviation, bunkers navigation) puis appliquée sur chaque gaz-usage-mode-tech)</td><td><blockquote><p>Facteurs pas envoyés vers le dashboard</p><p>Le metanode pour appliquer les facteurs de calibration pourrait être réduit à 1 tree split node</p><p>Manque de commentaires de nodes dans le cadre calibration</p></blockquote></td></tr><tr class="odd"><td>Buildings</td><td>Emissions</td><td>Indirect (calibration sur sub-sector (ex. Resid)</td><td><p>Feuille RAW mais pas de dashboard: OK</p><p>==&gt; Note : non-residential : CH4 : problématique !</p><p><img src="../media/Calibration-Etat-des-lieux-image6.png" style="width:4.21875in;height:0.96875in" alt="Champ calcu/é 1 Dimension _ 4 Dimension 3 CH4 non-residential 18389% C02 N20 residential 9299% 86.44% 99.63% 101.44% 101.84% " /></p><p></p></td></tr><tr class="even"><td></td><td>Energy</td><td>Indirect (calibration sur sub-sector x end-use x vector (ex. Resid x cooking x elec)</td><td><p>Feuille RAW mais pas de dashboard : Ok</p><p>==&gt; Note : liquid-ff en non-residential : probélamtique !</p><p><img src="../media/Calibration-Etat-des-lieux-image7.png" style="width:5in;height:2.25in" alt="Champ calcu/é 1 Dimension 3 Dimension non-residenti cooking cooling heating hot,vater lighting others non-residential Total 4 Dimension 5 elec 6228% 8371 116.87% liquid-ff solid-biomass 16526% 16538% solid-ff 120.12% 18632% residential appliances-all cooking cooling heating hot,vater lighting residential Total Grand Total 10571 76.28 6228% 10530 106.75% 10502 13304% 69.21% 104.06 10530% 0.622797121 0.9602830899 Grand Total 62.28 8371 116.87 120.12 10571 76.28 62.28 10530 106.75 10502 133.04 69.21 104.06 10530 0.622797121 " /></p><p></p></td></tr><tr class="odd"><td>Electricity</td><td>Emissions</td><td></td><td><ul class="incremental"><li><p></p></li></ul></td></tr><tr class="even"><td></td><td>Energy</td><td>Direct</td><td><p>==&gt; Note : problem with oil</p><p><img src="../media/Calibration-Etat-des-lieux-image8.png" style="width:4.21875in;height:0.75in" alt="Champ calcu/é 1 Dimension 4 Dimension 3 fossil gas 98.11% oil 87.62% 25.91% " /></p><p></p></td></tr><tr class="odd"><td></td><td>Energy demand</td><td>Direct</td><td><ul class="incremental"><li><p>==&gt; Note : résultat limite (145%)</p></li></ul><blockquote><p><img src="../media/Calibration-Etat-des-lieux-image9.png" style="width:3.1875in;height:0.96875in" alt="Champ calculé 1 Dimension Dimension 3 oil fossil Grand Total 145.66% 145.66% Grand Total 145.66% 145.66% " /></p></blockquote><ul class="incremental"><li><p>Pourquoi n'y a-t-il que oil ici?</p></li></ul></td></tr><tr class="even"><td></td><td>Gross demand</td><td>Direct</td><td><p>Pourquoi n'y a-t-il que oil ici?</p><p></p></td></tr><tr class="odd"><td></td><td>Net production</td><td>Direct</td><td><p>Pourquoi n'y a-t-il que oil ici?</p><p></p></td></tr><tr class="even"><td>Climate</td><td>?</td><td></td><td></td></tr></tbody></table>



Modules sans calibration:
-   Technology
-   Air pollution

Modules non vérifiés:
-   Electricity :

    -   pas de metanode Calibration Dashboard==> données anciennes qui doivent être actualisées quand Knime refonctionne !?)

    -   Différence entre "energy", "energy demand", etc. n'est pas très claire: utilise-t-on la bonne nomenclature?
-   Buildings : +/- ok : cfr notes plus bas
-   Climate : calibration présente ! Maisn'utilise pas de EUCalc / BECalc Calibration metanode mais des codes python ! ==> Aucune sortie de type cal_rate et pas de googleSheet

![Python Script (2---1) Compute calibration factors Constant Value Column Add Country column Calibration (EU Total Emissions) Column Resorter Son Column Rename Rename rates ](../media/Calibration-Etat-des-lieux-image10.png){width="7.645833333333333in" height="2.3541666666666665in"}







Note pour Buildings (BECALC) :
-   Cadre en orange: ok
-   Description des cadres : +/- ok pour "Energy" (les inputs et outputs pourraient être plus détaillés (unités, ...) - à compléter pour CO2 Emissions (manque les inputs et outputs)
-   "Energy" : calibration indirecte (somme par subsector x end-use x vector)
-   "Emissions CO2" : idem mais plus grossière ; juste réalisée au niveau des sub-sector
-   Ok pour les sorties cal_rate : join et sortie GoogleSheet **mais utilise noeud EUCalc plutôt que BECalc pour Dashborad : problématique ? OUI**



![Machine generated alternative text: ](../media/Calibration-Etat-des-lieux-image11.png){width="6.21875in" height="4.145833333333333in"}







Note pour Electricity (BECALC) :



**5.1 Electricity** :

Un metanoeud (Energy demand from sector) don't les sorties cal_rate ne sont pas sauvegardées (port sans connexion) :

![Calibration Column Filter Energy demand from ectors Calibration Column Re Node 1505 mn Rename ](../media/Calibration-Etat-des-lieux-image12.png){width="4.8125in" height="3.0in"}

Les deux autres noeuds (Emissions et Energy) : ok directe et cal_rat sont "joint" ensemble

**Refinery** :

Net fossil production

Gross fossil production

Energy Demand from sectors

Emission

==> Ok joint ensemble



Problème 1 : les 5.1 Electricity et Refinery ne semblent pas être joint ensemble...

Problème 2 : pas de lien avec la google Sheet + les joint ne sont pas les bons (on va chercher les "anciens" cal_ plutôt que les ceux calcilés dans les cadre orange : cfr image ci-dessous)



![Calibration ELC calib ion Node 1718 for other module <climate emiss ions--- Joiner Column Filter calibratio Column Filt refinery calibration Joiner Column Column Rename name (Regex) name correction Node -network calib 1738 elecU ty calibr n resuts Power Su standard na mes L2 te interface Goog les hee ](../media/Calibration-Etat-des-lieux-image13.png){width="8.104166666666666in" height="4.15625in"}





# Axes d'amélioration
-   Détection secteurs manquants

    -   Actuellement secteurs de calibration manquants pour lesquels on a des data génèrent des warnings, et si donnée à calibrer manquante ça génère une erreur.

    -   Du coup quand on a le cas on l'enlève de la calibration pour la remettre après, ce qui n'est pas idéal.

# Todo
-   Modifier metanode Calibration Dashboard pour utiliser la variable de flux projet (EU ou BE)
-   10100%? >vincent
-   Demander à chaque module owner quelles fonctionalité manque
-   Format dashboard avec min-mean-max













