<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Compte rendu](#compte-rendu)
- [État des lieux](#%C3%A9tat-des-lieux)
  - [Industry](#industry)
    - [Calibration ](#calibration)
    - [Quality Check](#quality-check)
  - [Agriculture](#agriculture)
    - [Calibration](#calibration)
    - [Quality Check](#quality-check-1)
  - [Land Use](#land-use)
    - [Calibration](#calibration-1)
    - [Quality Check](#quality-check-2)
  - [Lifestyle](#lifestyle)
    - [Calibration](#calibration-2)
    - [Quality Check](#quality-check-3)
  - [Transport](#transport)
    - [Calibration](#calibration-3)
    - [Quality Check](#quality-check-4)
  - [](#)
  - [Buidlings](#buidlings)
    - [Calibration](#calibration-4)
    - [Quality Check](#quality-check-5)
  - [](#-1)
  - [Electricity](#electricity)
    - [Calibration](#calibration-5)
    - [Quality Check](#quality-check-6)
  - [Climate](#climate)
    - [Calibration](#calibration-6)
    - [Quality Check](#quality-check-7)
- [Axes d'amélioration](#axes-dam%C3%A9lioration)
- [Todo](#todo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Compte rendu

Created: 2020-04-15 12:16:50 +0200

Modified: 2020-04-15 12:17:10 +0200

---


**Quality Check** :
-   Quels quality check ont été utilisés ?

**Exemple** : on décompose des variables (en fonction de pourcentages) et on veut regarder que le total est toujours bon après plusieurs calculs effectués sur les pourcentages

**Habituels** :
-   Pct des leviers = 1
-   Calculs corrects
-   Join : sortie table droite, table gauche et jonction : vérifier que tout le contenu d'une table est bien passé dans la jonction (pas de colonne pas présentes dans la jonction, ...)





# État des lieux



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="industry">Industry</h2><p></p><h3 id="calibration">Calibration </h3><p></p><ol class="incremental" type="1"><li><p><strong>Product demand</strong> : directe</p></li></ol><blockquote><p>Statut : ok</p><p></p></blockquote><ol class="incremental" start="2" type="1"><li><p><strong>Material demand</strong> : directe</p></li></ol><blockquote><p>Statut : ok</p><p></p></blockquote><ol class="incremental" start="3" type="1"><li><p><strong>Energy</strong> : directe</p></li></ol><blockquote><p>Statut : ok</p><p></p></blockquote><ol class="incremental" start="4" type="1"><li><p><strong>Emissions</strong> : indirecte (calibration sur material appliquée ensuite sur material-technology)</p></li></ol><blockquote><p>Statut : ok</p></blockquote><p></p><h3 id="quality-check">Quality Check</h3><p>A COMPLETER</p><p></p></th></tr></thead><tbody></tbody></table>





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="agriculture">Agriculture</h2><p></p><h3 id="calibration-1">Calibration</h3><p></p><ol class="incremental" type="1"><li><p><strong>Activity - demande agricole livestock</strong> : directe ? Indirecte ?</p></li></ol><blockquote><p>Statut : ok</p><p></p></blockquote><ol class="incremental" start="2" type="1"><li><p><strong>Activity – population livestock</strong> : D/I ?</p></li></ol><blockquote><p>Output pas utilisé dans le workflow</p><p></p></blockquote><ol class="incremental" start="3" type="1"><li><p><strong>Activity – feed consumption</strong> : D/I ?</p></li></ol><blockquote><p>Output pas utilisé dans le workflow</p><p></p></blockquote><ol class="incremental" start="4" type="1"><li><p><strong>Activity – crop production</strong> : D/I ?</p></li></ol><blockquote><p>Output pas utilisé dans le workflow</p><p></p></blockquote><ol class="incremental" start="5" type="1"><li><p><strong>Residues</strong> : Indirecte (calibré sur somme des résidus, ensuite appliqué sur liquide/solide)</p></li></ol><blockquote><p>Statut : ok</p><p></p></blockquote><ol class="incremental" start="6" type="1"><li><p><strong>Agriculture land</strong> : D/I ?</p></li></ol><blockquote><p>Output pas utilisé dans le workflow</p><p></p></blockquote><ol class="incremental" start="7" type="1"><li><p><strong>Energy</strong> : directe</p></li></ol><blockquote><p>Statut : ok</p><p></p></blockquote><ol class="incremental" start="8" type="1"><li><p><strong>Emissions</strong> : Indirecte partielle (calibration par gaz-usage, le facteur CO2 est ensuite appliqué sur les vecteurs de l'energy demand)</p></li></ol><blockquote><p>Statut : ok</p></blockquote><p></p><h3 id="quality-check-1">Quality Check</h3><p>A COMPLETER</p><p></p></th></tr></thead><tbody></tbody></table>



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="land-use">Land Use</h2><p></p><h3 id="calibration-2">Calibration</h3><p></p><ol class="incremental" type="1"><li><p><strong>Agricultural land</strong> : indirecte : (calibration sur total agricultural land appliquée sur pastures et crop)</p></li></ol><blockquote><p>Statut : ok</p><p></p></blockquote><ol class="incremental" start="2" type="1"><li><p><strong>Emissions</strong> : Indirecte ? Directe ?</p></li></ol><blockquote><p>Statut : pas clair ; mal documenté ==&gt; Note : feuille Dashborad a été créée mais la feuille RAW n'est plus générée...</p><p></p><p><img src="../media/Calibration-Compte-rendu-image1.png" style="width:4.08333in;height:1.63542in" alt="&#39; : 一 q = uo!ss!tua 、 00 " /></p><p></p></blockquote><p></p><h3 id="quality-check-2">Quality Check</h3><p>A COMPLETER</p><p></p></th></tr></thead><tbody></tbody></table>



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="lifestyle">Lifestyle</h2><p></p><h3 id="calibration-3">Calibration</h3><p></p><ol class="incremental" type="1"><li><p><strong>Food demand</strong> : directe</p></li></ol><blockquote><p>Statut :</p></blockquote><ul class="incremental"><li><p>Facteurs non envoyés dans le dashboard</p></li><li><p>Utilise encore le node EUCalc ==&gt; Passer en BECalc</p></li><li><p>Pas de commentaires sur les métanoeuds : sur quoi calibre-t-on ?</p></li></ul><p></p><ol class="incremental" start="2" type="1"><li><p><strong>Food waste</strong> : directe</p></li></ol><blockquote><p>Statut :</p></blockquote><ul class="incremental"><li><p>Facteurs non envoyés dans le dashboard</p></li><li><p>Utilise encore le node EUCalc ==&gt; Passer en BECalc</p></li><li><p>Pas de commentaires sur les métanoeuds : sur quoi calibre-t-on ?</p></li></ul><blockquote><p></p></blockquote><ol class="incremental" start="3" type="1"><li><p><strong>Passenger-km demand</strong> : directe</p></li></ol><blockquote><p>Statut :</p></blockquote><ul class="incremental"><li><p>Facteurs non envoyés dans le dashboard</p></li><li><p>Utilise encore le node EUCalc ==&gt; Passer en BECalc</p></li><li><p>Pas de commentaires sur les métanoeuds : sur quoi calibre-t-on ?</p></li></ul><blockquote><p></p></blockquote><ol class="incremental" start="4" type="1"><li><p><strong>Residential Floor Area</strong> : directe</p></li></ol><blockquote><p>Statut :</p></blockquote><ul class="incremental"><li><p>Facteurs non envoyés dans le dashboard</p></li><li><p>Utilise encore le node EUCalc ==&gt; Passer en BECalc</p></li><li><p>Pas de commentaires sur les métanoeuds : sur quoi calibre-t-on ?</p></li></ul><blockquote><p></p></blockquote><p></p><h3 id="quality-check-3">Quality Check</h3><p>A COMPLETER</p><p></p></th></tr></thead><tbody></tbody></table>



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="transport">Transport</h2><p></p><h3 id="calibration-4">Calibration</h3><p></p><ol class="incremental" type="1"><li><p><strong>Activity – pkm passenger</strong> : Indirecte (calibre sur demande en Gpkm pour road-private, road-public, rail, aviation puis appliqués sur les modes)</p></li></ol><blockquote><p>Statut :</p></blockquote><ul class="incremental"><li><p>Facteurs pas envoyés vers le dashboard</p></li><li><p>Question : duplication avec la calibration dans lifestyles ? Si oui, pourquoi ?</p></li></ul><p></p><ol class="incremental" start="2" type="1"><li><p><strong>Activity – tkm freight</strong> : directe</p></li></ol><blockquote><p>Statut :</p></blockquote><ul class="incremental"><li><p>Facteurs par envoyés vers le dashboard</p></li><li><p>Calibration directe mais implémentée comme indirecte (relation 1-1 entre facteurs et variables calibrées)</p></li><li><p>Remarque indiquant que calibration LDV n'est pas utilisée</p></li></ul><p></p><ol class="incremental" start="3" type="1"><li><p><strong>Energy</strong> : Indirect (calibrate energy demand by vector puis appliquée par mode-tech)</p></li></ol><blockquote><p>Statut :</p></blockquote><ul class="incremental"><li><p>Facteurs par envoyés vers le dashboard</p></li><li><p>Les deux métanodes pour appliquer les facteurs de calibration pourraient être réduits à 1 tree split node</p></li><li><p>Manque de commentaires de nodes dans le cadre de la calibration</p></li><li><p>Box d'erreur rouge non résolu ?</p></li></ul><blockquote><p></p></blockquote><ol class="incremental" start="4" type="1"><li><p><strong>Emissions</strong> : Indirect (calibration sur gaz-usage (domestic, bunkers aviation, bunkers navigation) puis appliquée sur chaque gaz-usage-mode-tech)</p></li></ol><blockquote><p>Statut :</p></blockquote><ul class="incremental"><li><p>Facteurs par envoyés vers le dashboard</p></li><li><p>Le métanode pour appliquer les facteurs de calibration pourraient être réduits à 1 tree split node</p></li><li><p>Manque de commentaires de nodes dans le cadre de la calibration</p></li></ul><p></p><p></p><h3 id="quality-check-4">Quality Check</h3><p>A COMPLETER</p><p></p></th></tr></thead><tbody></tbody></table>

## 

<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="buidlings">Buidlings</h2><p></p><h3 id="calibration-5">Calibration</h3><p></p><ol class="incremental" type="1"><li><p><strong>Emissions</strong> : Indirect (calibration sur sub-sector (ex. Resid)</p></li></ol><blockquote><p>Statut : ok</p><p>Note : non-residential : CH4 = problématique !</p><p><img src="../media/Calibration-Compte-rendu-image2.png" style="width:4.21875in;height:0.96875in" alt="Champ calcu/é 1 Dimension _ 4 Dimension 3 CH4 non-residential 18389% C02 N20 residential 9299% 86.44% 99.63% 101.44% 101.84% " /></p><p></p></blockquote><ol class="incremental" start="2" type="1"><li><p><strong>Energy</strong> : Indirect (calibration sur sub-sector x end-use x vector (ex. Resid x cooking x elec)</p></li></ol><blockquote><p>Statut : ok</p><p>Note : liquid-ff en non-resid = problématique !</p><p><img src="../media/Calibration-Compte-rendu-image3.png" style="width:5in;height:2.25in" alt="Champ calcu/é 1 Dimension 3 Dimension non-residenti cooking cooling heating hot,vater lighting others non-residential Total 4 Dimension 5 elec 6228% 8371 116.87% liquid-ff solid-biomass 16526% 16538% solid-ff 120.12% 18632% residential appliances-all cooking cooling heating hot,vater lighting residential Total Grand Total 10571 76.28 6228% 10530 106.75% 10502 13304% 69.21% 104.06 10530% 0.622797121 0.9602830899 Grand Total 62.28 8371 116.87 120.12 10571 76.28 62.28 10530 106.75 10502 133.04 69.21 104.06 10530 0.622797121 " /></p><p></p><p></p></blockquote><p>Note pour Buildings (BECALC) :</p><ul class="incremental"><li><p>Description des cadres (oranges) : +/- ok pour "Energy" (les inputs et outputs pourraient être plus détaillés (unités, …) - à compléter pour CO2 Emissions (manque les inputs et outputs)</p></li><li><p>Au niveau des sorties cal_rate : <strong>utilise noeud EUCalc plutôt que BECalc pour Dashborad : problématique ? OUI, donc à changer</strong></p></li></ul><blockquote><p></p><p><img src="../media/Calibration-Compte-rendu-image4.png" style="width:6.21875in;height:4.14583in" alt="Machine generated alternative text: " /></p></blockquote><p></p><h3 id="quality-check-5">Quality Check</h3><p>A COMPLETER</p><p></p></th></tr></thead><tbody></tbody></table>

## 

<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="electricity">Electricity</h2><p></p><h3 id="calibration-6">Calibration</h3><p></p><ol class="incremental" type="1"><li><p>Emission : directe</p></li></ol><blockquote><p>Statut : ok</p></blockquote><p></p><ol class="incremental" start="2" type="1"><li><p>Energy : directe</p></li></ol><blockquote><p>Statut : ok</p><p>Note : oil = problématique</p><p></p></blockquote><ol class="incremental" start="3" type="1"><li><p>Energy demand : directe</p></li></ol><blockquote><p>Statut : ok ==&gt; pourquoi que oil ici ?</p><p>Note : résultats limite (145%!)</p><p><img src="../media/Calibration-Compte-rendu-image5.png" style="width:3.1875in;height:0.96875in" alt="Champ calculé 1 Dimension Dimension 3 oil fossil Grand Total 145.66% 145.66% Grand Total 145.66% 145.66% " /></p><p></p><p></p></blockquote><ol class="incremental" start="4" type="1"><li><p>Gross demand : directe</p></li></ol><blockquote><p>Statut : ok ==&gt; pourquoi que oil ici ?</p><p></p><p></p></blockquote><p>Note : pas de metanode "Calibration Dashboard" ==&gt; Donc les données qiu sont affichées dans la google sheet de calibration sont des anciennes données qui doivent être actualisées quand on a ajouté le noeud dans Knime</p><p></p><p>Question : quelle est la différence entre "energy", "energy demand" ? Utilise-t-on la bonne nomenclature?</p><p></p><p></p><p>Autres modifications à faire :</p><p>Un metanoeud (Energy demand from sector) : les sorties cal_rate ne sont pas sauvegardées (port sans connexion) : à connecter, donc.</p><p><img src="../media/Calibration-Compte-rendu-image6.png" style="width:4.8125in;height:3in" alt="Calibration Column Filter Energy demand from ectors Calibration Column Re Node 1505 mn Rename " /></p><p>Problème 1 : les 5.1 Electricity et Refinery ne semblent pas être joint ensemble...</p><p>Problème 2 : pas de lien avec la google Sheet + les joint ne sont pas les bons (on va chercher les "anciens" cal_ plutôt que les ceux calcilés dans les cadre orange : cfr image ci-dessous)</p><p></p><p><img src="../media/Calibration-Compte-rendu-image7.png" style="width:8.10417in;height:4.15625in" alt="Calibration ELC calib ion Node 1718 for other module &lt;climate emiss ions— Joiner Column Filter calibratio Column Filt refinery calibration Joiner Column Column Rename name (Regex) name correction Node -network calib 1738 elecU ty calibr n resuts Power Su standard na mes L2 te interface Goog les hee " /></p><p></p><blockquote><p></p></blockquote><p></p><h3 id="quality-check-6">Quality Check</h3><p>A COMPLETER</p><p></p></th></tr></thead><tbody></tbody></table>





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="climate">Climate</h2><p></p><h3 id="calibration-7">Calibration</h3><p>Climate : calibration présente ! Maisn'utilise pas de EUCalc / BECalc Calibration metanode mais des codes python ! ==&gt; Aucune sortie de type cal_rate et pas de googleSheet</p><p><img src="../media/Calibration-Compte-rendu-image8.png" style="width:7.64583in;height:2.35417in" alt="Python Script (2—1) Compute calibration factors Constant Value Column Add Country column Calibration (EU Total Emissions) Column Resorter Son Column Rename Rename rates " /></p><p></p><blockquote><p></p></blockquote><h3 id="quality-check-7">Quality Check</h3><p>A COMPLETER</p><p></p></th></tr></thead><tbody></tbody></table>





Modules sans calibration:
-   Technology
-   Air pollution





# Axes d'amélioration
-   Détection secteurs manquants

    -   Actuellement secteurs de calibration manquants pour lesquels on a des data génèrent des warnings, et si donnée à calibrer manquante ça génère une erreur.

    -   Du coup quand on a le cas on l'enlève de la calibration pour la remettre après, ce qui n'est pas idéal.

# Todo
-   Modifier metanode Calibration Dashboard pour utiliser la variable de flux projet (EU ou BE)
-   10100%? >vincent
-   Demander à chaque module owner quelles fonctionalité manque
-   Format dashboard avec min-mean-max








