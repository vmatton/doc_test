<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Create new project](#create-new-project)
  - [For deployment](#for-deployment)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Create new project

Created: 2020-04-15 08:37:32 +0200

Modified: 2021-02-15 10:55:54 +0100

---


This section is about creating new project.

If you are working on an already existing project (eg. BE2050) and want to make some modifications on it, cfr the Update section ([Updating existing project](onenote:#Updating%20existing%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={687D9522-BD6D-49C2-8DF3-23154AA366D9}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



| New project : improving XCalc or new branch ? |
|-----------------------------------------------|



**Keep in mind : clone master or create branch ?**

When you want to create a new project, you should analyze how much XCalc will be modified with this project. If a new project is too different from XCalc, a branch of the master should be created. Here are the rules ;



<table><colgroup><col style="width: 28%" /><col style="width: 59%" /><col style="width: 12%" /></colgroup><thead><tr class="header"><th><strong>Action</strong></th><th><strong>Example</strong></th><th><strong>Master or Branch ?</strong></th></tr></thead><tbody><tr class="odd"><td>Your project consists of <strong>using other values</strong> for input data</td><td>You want to have the calculator for another country</td><td>Master</td></tr><tr class="even"><td><strong>Small modifications</strong> into the calculation process</td><td>New lever, new metrics but no big changes (eg. ELIA)</td><td>Master</td></tr><tr class="odd"><td>Lot of modifications and <strong>profound changes</strong></td><td><p>You will not use all the existing modules, you want to use an input file formatting in a very different way of input data in XCalc, interactions between modules will deeply change.</p><p>E.g. : VLAIO is based on what was done in XCalc but with a lot of modifications and only the Industry module is used.</p></td><td>Branch</td></tr><tr class="even"><td>Adding a <strong>new module</strong> with only few modifications to other modules</td><td>Eg. Add Air Quality module</td><td>Master</td></tr></tbody></table>



**Branches are not created for each folder**. Here are the rules :
-   Dev (Knime) :

    -   _common : only master (no branch needed !)

    -   _interactions : master or branch depending on modifications that will be made

    -   Others modules (eg. Industry) : master or branch depending on modifications that will be made
-   Webtool : only master
-   Knimetopython : only master (except if we are not sure of what will be done, if internships, ... ; make a branch to work in parallel)



How to create a branch is described in the following sections (when required).





| Bitbucket and local desktop |
|-----------------------------|



Once you have determined if you need to create a branch or use the master, you can create a folder for your project on your local desktop.

*For more information : User Manual > Bitbucket > Install and create account* (section *Next Steps*).



For each new project with **master branch** :

No need to create a new project folder on your local desk (use the XCalc (or BECalc) folder).

Nevertheless, as your project will bring new input and output data, you should create a new folder in each subfolders as followed :



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>&gt; dev (in your project file)</strong></p><p><strong>&gt; _interactions</strong></p><p><strong>&gt; data</strong></p><p><strong>&gt; EU</strong></p><p><strong>&gt; BE</strong></p><p><strong>&gt; YOUR_PROJECT_NAME</strong></p><p><strong>&gt; …</strong></p><p><strong>&gt; configuration</strong></p><p><strong>&gt; EU</strong></p><p><strong>&gt; BE</strong></p><p><strong>&gt; YOUR_PROJECT_NAME</strong></p><p><strong>&gt; …</strong></p><p><strong>&gt; _common (<em>ok : no need to be changed</em>)</strong></p><p><strong>&gt; industry (and/or other modules)</strong></p><p><strong>&gt; data</strong></p><p><strong>&gt; EU</strong></p><p><strong>&gt; BE</strong></p><p><strong>&gt; YOUR_PROJECT_NAME</strong></p><p><strong>&gt; ...</strong></p><p><strong>&gt; output</strong></p><p><strong>&gt; EU</strong></p><p><strong>&gt; BE</strong></p><p><strong>&gt; YOUR_PROJECT_NAME</strong></p><p><strong>&gt; ...</strong></p><p></p><p><strong>&gt; webtool (<em>Node JS/React</em>) ==&gt; Rien à faire : Realease différente du webtool en fonction du projet ==&gt; en test pour le moment ; il y aura des choses à changer (ex. Adresse du serveur, filtre sur les types de données (ex.Belgique ou Bulgarie)) NOrmalement ; pas grand chose, mais à voir encore... Ex. Ne faire tourner que sur certains pays... (pour gardr petite taille de serveur)?</strong></p><p></p><p><strong>&gt; knime2python (<em>Python</em>) :</strong></p><p><strong>&gt; config</strong> : <strong>create two configuration files for your project (one for the remote server and one to test your code on local desktop)</strong></p><p></p><p><img src="../media/Technical-Documentation-Create-new-project-image1.png" style="width:3.20833in;height:2.5625in" alt="config old &amp; drafts config_be.yml config_elia.yml config_eu2050.ymI config_vlaio.yml credentials.Json token.pickle " /></p><p></p><p><strong>&gt; scripts : create a python script to test your code on local desktop (Figure 1). This code will set where to find the configuration file (cfr config folder) (Figure 2).</strong></p><p></p><p><em>Figure 1 : there is one python code by project (eg. Becalc-app.py, eliacalc-app.py, …)</em></p><p><img src="../media/Technical-Documentation-Create-new-project-image2.png" style="width:3.6875in;height:3.58333in" alt="token.plckle old &amp; drafts becalc-app.py becalc_scenarios.xlsx eliacalc-app.py eu20SO-app.py gtap_scenarios.py install dockersh interactive.py precalculate.py quickscan.py vlaio-app.py " /></p><p></p><p><em>Figure 2 : In each project-calc.py, we indicate where the config file is located.</em></p><p><img src="../media/Technical-Documentation-Create-new-project-image3.png" style="width:8.3125in;height:1.98958in" alt="- if name warnings . ignore # Extract command line arguments argparse C) -c&quot;, -configuration&quot; , parser. -no-extra -scenarios&quot; parser — run arg cf configuration file (defaults to config,&#39; config_bel.yml&quot; default—os . path . join s. n Nth . dirname(os . path . realpath( file_)), &#39; config config _ be , yml&#39; ) not run extra scenarios, just the first one&quot; t ue&quot;) s — parser. parse args( " /></p><p></p><p></p></th></tr></thead><tbody></tbody></table>



For each new project with a **new branch** :

Create a local file with your project name. At the end, the structure files should look like this :



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>C:</strong></p><p><strong>&gt; project_name (eg. VLAIO, ELIA, BECalc, …)</strong></p><p><strong>&gt; dev (<em>Knime</em>)</strong></p><p><strong>&gt; _interactions</strong></p><p><strong>&gt; _common</strong></p><p><strong>&gt; industry (and/or other modules)</strong></p><p><strong>&gt; webtool (<em>Node JS/React</em>)</strong></p><p><strong>&gt; knimetopython (<em>Python</em>)</strong></p><p></p></th></tr></thead><tbody></tbody></table>



Note :
-   dev file is compulsory to make sure everybody has the same file structure. Webtool and knimetopython files are only useful for developers.
-   In dev file, we have a git by module (eq. A git for _interactions, a git for _common, ...)
-   We only have one git in the webtool folder
-   We only have one git in the knimetopython folder



Once the folder is cerated, **clone (master)** all the module you need and **create branch**.

If input data are different from BECalc, EUCalc, ... Create subfolder "**YOUR_PROJECT_NAME**" as it is done for projects with master branch (cfr previously).





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>HOW TO CLONE MASTER AND CREATE BRANCH ?</strong></p><p></p><p><strong>Note : no need to make a branch for the _common module, but make sure it has been clone here !</strong></p><p></p><p>These following steps <strong>should be done for all the modules you need</strong> in your project (at least : _interactions)</p><p>E.g. for Vlaio : industry and _interactions.</p><p></p><p>Step 1 : create a branch on the master branch of each modules needed in your project</p><p></p><ol class="incremental" type="a"><li><p>Go to C:<strong>XCalc</strong>dev<strong>module_needed</strong></p></li></ol><blockquote><p>If you do not have this folder yet or if you do not have clone some module yet ; cfr <em>User Manual &gt; Git &gt; Using Git</em> ("<em>Using it for Knime project – section a.</em>") to do it before going through the next steps.</p></blockquote><p></p><ol class="incremental" start="2" type="a"><li><p>Right click and select "Git Bash Here"</p></li></ol><p></p><ol class="incremental" start="3" type="a"><li><p>Command line :</p><ol class="incremental" type="i"><li><p>$ git checkout --b <strong>project_name</strong> (e.g. $ git checkout --b vlaio) ==&gt; create a branch named "vlaio" and switch to it</p></li><li><p>$ git push origin <strong>project_name</strong> (e.g. $ git push origin vlaio) ==&gt; push the branch "vlaio" on the remote (origin)</p></li></ol></li></ol><p></p><blockquote><p>On bitbucket, check if the new branch has been properly added in each module required for your project.</p><p>E.g. : For the _interactions module : go to "Branches" and check if your branch (here, vlaio) has been added.</p></blockquote><p></p><p><img src="../media/Technical-Documentation-Create-new-project-image4.png" style="width:5in;height:3.11458in" alt="Climact / XCalc / Branches Search branches interactions All branches Branch v master MAIN vlaio DEVELOPMENT " /></p><p></p><p></p><p>Step 2 : clone master and switch to new branch</p><p></p><p><strong>Note : when we clone a module, only the master branch is cloned. If we want to work with the other branches on it, we have to create a local branch and then copy the content of the remote branch to this freshly created branch.</strong></p><p>For this, go to <strong>C:project_namedev</strong>, the right click and select "Git Bash Here" :</p><p></p><p>By module needed :</p><ul class="incremental"><li><p>If not already clone : $ git clone … (cfr <em>User Manual &gt; Git &gt; Using Git</em> ("Using it for Knime project – section a.") for more details on how to use clone)</p></li><li><p>$ git checkout -b vlaio origin/vlaio ==&gt; create a local branch named "vlaio" (cfr checkout -b vlaio) and copy the content of origin/vlaio in it (origin /vlaio = the branch named vlaio which are on the origin (remote) server)</p></li></ul><blockquote><p>==&gt; checkout : switch from master to -b vlaio (once it done, each time we open our module, we are working on this branch and not the master one)</p></blockquote><ul class="incremental"><li><p>$ git remote -v : Manage set of tracked repositories and (-v) show remote url after name</p></li></ul><p></p><p></p></th></tr></thead><tbody></tbody></table>







| Next steps |
|------------|



Most of the steps that are done after the "bitbucket step" are the same than Updating section.

Therefore, for all the following steps (Google Sheet (data collection, interface and scenario builder), Knime (module, _interactions and _common) and deployment), cfr the sections as mentioned below :



<table><colgroup><col style="width: 26%" /><col style="width: 56%" /><col style="width: 17%" /></colgroup><thead><tr class="header"><th><strong>Action</strong></th><th><strong>Example</strong></th><th><strong>Following steps</strong></th></tr></thead><tbody><tr class="odd"><td>Your project consists of <strong>using other values</strong> for input data</td><td>You want to have the calculator for another country</td><td><a href="onenote:#Update%20Data&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={6DF631E4-8ED1-4113-AE6B-D2C726B51EF0}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Update Data</a></td></tr><tr class="even"><td><strong>Small modifications</strong> into the calculation process</td><td>New lever, new metrics but no big changes (eg. ELIA)</td><td><p><a href="onenote:#New%20metrics%20(module%20modification)&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={DA8E63DF-F01D-4825-86C6-67875E985DBD}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">New metrics (module modification)</a></p><p>And</p><p><a href="onenote:#New%20lever%20(module%20modification)&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={606E6ED0-21AD-4706-9190-1EBD83AF8A78}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">New lever (module modification)</a></p></td></tr><tr class="odd"><td><p>Lot of modifications and <strong>profound changes</strong></p><p></p></td><td>You will not use all the existing modules, you want to use an input file formatting in a very different way of input data in XCalc, interactions between modules will deeply change (eg. VLAIO)</td><td><a href="onenote:#New%20branch&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={20BF6592-705B-47D9-A854-E6D6691B088C}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">New branch</a></td></tr><tr class="even"><td>Adding a <strong>new module</strong> with only few modifications to other modules</td><td><p>Eg. Add Air Quality module</p><p></p></td><td><a href="onenote:#New%20module&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={4344E2DF-514A-4233-AE84-0DA44AA58C72}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">New module</a></td></tr></tbody></table>



**Other steps are required before that. There are mentioned below :**



In Google Sheet

1.  Create a new folder on Google drive (name of the folder = name of your project). This folder will contain all the data and files needed for your project.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>/! Make sure this folder is editable by everyone whose has the link, otherwise it will not be possible to use your files in PowerBI and so on /!</strong></p><p><strong>To do so</strong> :</p><ol class="incremental" type="a"><li><p>Go to your project file</p></li><li><p>Select "Get shareable link"</p></li></ol><blockquote><p><img src="../media/Technical-Documentation-Create-new-project-image5.png" style="width:3.71875in;height:1.71875in" alt="CLIMACT builder collection &gt; TEST2050 Open with New folder Share Get shareable link Move to Add to Starred " /></p><p></p></blockquote><p></p><ol class="incremental" start="3" type="a"><li><p>Then make sure button is "green" (anyone with the link can …")</p></li></ol><blockquote><p><img src="../media/Technical-Documentation-Create-new-project-image6.png" style="width:3.71875in;height:2.1875in" alt="TEST2050 Link sharing on Anyone with the link can view Sharing settings " /></p><p></p></blockquote><p></p><ol class="incremental" start="4" type="a"><li><p>Go to "Sharing settings" and select "anyone with the link <strong>can edit</strong>"</p></li></ol><blockquote><p><img src="../media/Technical-Documentation-Create-new-project-image7.png" style="width:3.84375in;height:2.95833in" /></p><p></p></blockquote><p></p><p></p><ol class="incremental" start="5" type="a"><li><p>In this folder, create a "scenarios builder" file. At the end, folder structure should look like :</p></li></ol><p></p><p><img src="../media/Technical-Documentation-Create-new-project-image8.png" style="width:3.90625in;height:1.36458in" alt="Mon Drive Nom &gt; CLIMACT &gt; TEST2050 scenarios builder " /></p><p></p></th></tr></thead><tbody></tbody></table>





2.  Copy/Paste some Google Sheet (from BE2050 folder) to your project folder in Google Drive. For that :

    a.  Go to the sheet you want to copy

    b.  Select File > Make a copy

![BE data collection File Edit View Insert Format Data Tools Add-ons Sha New Open Import .0 .00 123. Calibri th all the national and regional data ctrl+0 mp ete, Wit a t ena Make a copy lame Sheet d ](../media/Technical-Documentation-Create-new-project-image9.png){width="3.75in" height="2.0in"}





c.  Choose the destination (your project folder -- eg. TEST2050) - By default : select "shared with the same people"



![Copy document Name BE data collection Folder TEST20SO Share it with the same people COPY comments Include resolved comments Cancel x OK ](../media/Technical-Documentation-Create-new-project-image10.png){width="2.625in" height="2.78125in"}





**Copy are made to be sure we keep the same structure that already exist in XCalc. Once they are copied, you can delete the value inside and just keep the sheet you need.**

**Note : if you want to use values that already exist, no need to keep these pages, just point to them on the google_sheet_ids.xlsx file. Cfr User Manual > Google Sheets > Data Collection > "Note about data collection".**



Sheets which should be copied are :
-   BE data collection (contains all the input data)
-   BE2050_Interfaces (check if tables are ok, send data to pathway explorer for PowerBI, ...)
-   BE2050_Calibration : not compulsory to make XCalc works, but it allow each module owner to check if their data are well calibrated

**==> Better to rename them according to your project (BE and BE2050 to be replaced by your project name)**

![Mon Drive Nom > CLIMACT > BE2050 E model description scenarios builder BE data collection BE data collection_back-up BE_data_industry BE-data-collection_shared BE2050_calibration BE2050_input_data_regions BE2050_lnterfaces BE2050_Regional-data-collection_Update scenarios_BE2050 Working_doc ](../media/Technical-Documentation-Create-new-project-image11.png){width="4.1875in" height="6.46875in"}



3.  Copy/Paste the "BECalc - scenarios builder" Google Sheet (from BE2050 > scenarios builder folder) to your project folder in Google Drive (project_name > scenarios builder). Do not copy the json files ; they will be autogenerated from the scenario builder Excel sheet.



![Mon Drive Nom > CLIMACT > BE2050 > scenarios builder BECalc - scenarios builder BECalc - scenarios builder BECalc - scenarios builder BECalc - scenarios builder BECalc - scenarios builder - federal and regions - federal and regions - All_l .json - federal and regions - All_2.json Files - federal and regions - All_3.json - federal and regions - All_4.json ](../media/Technical-Documentation-Create-new-project-image12.png){width="7.15625in" height="2.90625in"}











In Knime

You will have to use your project prefix as value for the workflow variable "google_sheet_prefix".



![A WorWlow Variable Administration Name bas eyear disable_graphics country_filter test mode db host db_port db_table_output db_table_cube_output db schema Type • STRING • STRING • STRING • STRING • STRING • INTEGER • STRING • STRING • STRING • STRING Value 2015 true false localhost output cube levers Cancel ](../media/Technical-Documentation-Create-new-project-image13.png){width="4.53125in" height="3.8020833333333335in"}



You should also change some metanodes (in _common) and change the Excel files set in _common/configuration (cfr [_common](onenote:#_common&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5FEB34D3-BC69-490D-BB58-58B5DD156BD6}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



Note about data collection

Data are collected in Google Sheets. Knime knows in which google sheets it has to look for each project thanks to the google_sheet_ids.xslx file in *_common / configuration /project_name* folder.

For some reasons, **if you need to use some of the data coming from another project** (eg. Keep transport values coming from BE2050 for your project, but change values for other modules) you can do it by pointing to the appropriate google sheet id ; **no need to copy/paste these data into the google sheet "data collection" of your project**. This avoids redundancy and reduces risk of errors while updating values from a file and forgetting to apply the same modification on your file.



*Example* :

Prefix of your project : **PROJ**

Prefix for another project whose you need data : **BE**

Id of your google sheet "data collection" : Id_1

Id of the google sheet "data collection" for BE : Id_2

Module whose data comes from BE : transport and air_pollution

In the google_sheet_ids.xslx file you shoud have something like this :

| **google_sheet_prefix** | **module**    | **google_sheet_id** |
|-------------------------|---------------|---------------------|
| **BE**                  | transport     | Id_2                |
| **BE**                  | buidings      | Id_2                |
| **BE**                  | ...          | ...                |
| **BE**                  | air_pollution | Id_2                |
| **PROJ**                | transport     | Id_2                |
| **PROJ**                | buidings      | Id_1                |
| **PROJ**                | Interface     | Id_1                |
| **PROJ**                | ...          | ... (Id_1)          |
| **PROJ**                | air_pollution | Id_2                |



*Example* : Project TEST2050. Google sheet id used for New module and Old modules is the google sheet id of BE project. Only interface was created from scratch here.



![TEST2050 TEST2050 TEST2050 TEST2050 TEST2050 TEST2050 TEST2050 TEST2050 TEST2050 TEST2050 EST 2050 air_pollution air_mortality agricu ure industry lifestyle transport buildings NEW MODULE OLD MODULES electricity_supply technology interface climate interface INTERFACE IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IJYMkFH31Ezrcoe48dQwAAotjhEw-VRdnWEVOCmUjOV8 IWFrH6_Eg018gMZLe1AgACem90RiQhmjlL7DsOsUAsnE ](../media/Technical-Documentation-Create-new-project-image14.png){width="6.239583333333333in" height="2.21875in"}





For more details : Cfr *User Manual > Knime > Using Knime* (section "Using Knime variables") or here : [_common](onenote:#_common&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5FEB34D3-BC69-490D-BB58-58B5DD156BD6}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)





## For deployment

A configuration file should be created for your project (cfr [Configuration Files](onenote:#Configuration%20Files&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={74BD6C1F-8EF9-4961-B3AC-B17C62138312}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).














