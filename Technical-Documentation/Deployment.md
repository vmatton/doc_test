<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Deployment](#deployment)
- [Steps](#steps)
  - [Update _interactions in Knime](#update-_interactions-in-knime)
  - [Test converter and API before deploying](#test-converter-and-api-before-deploying)
    - [Test converter](#test-converter)
    - [Test API](#test-api)
  - [Knime2Python](#knime2python)
  - [Configuration](#configuration)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Deployment

Created: 2020-04-15 08:52:57 +0200

Modified: 2021-03-12 13:46:19 +0100

---




<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="steps">Steps</h1></th></tr></thead><tbody></tbody></table>



## Update _interactions in Knime



1.  Pull the latest version of each modules (that have been modified) + the latest version of _interactions and _common

2.  Check if these modules are properly written **using Quickscan**

    a.  If QuickScan failed, this means there are some errors in module. Send the errors message to each modelers concerned by them so that they can fix it. Once, pull these modules and QuickScan them again.

    b.  If QuickScan set no error message, go to step 3.

```{=html}
<!-- -->
```
1.  Open _interactions workflows in Knime : data-preprocessing and data-processing. Updates nodes (cfr CTRL+A followed by CTRL+ALT+U). **Use the drag and drop of a node only when update does not work properly.**

2.  Save _interactions workflows : data-preprocessing and data-processing (otherwise python codes will not use the proper version of them).



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>If new input data / lever / module</strong></p><ol class="incremental" type="1"><li><p>Run data-preprocessing workflow to get the latest values of them (to save time, just run the module whose data/lever have changed ; no need to run other modules).</p></li><li><p>Run data-processing workflow to update metrics with new values.</p></li><li><p>Save both workflows.</p></li></ol><p></p><p><strong>If new calculation process (including or not new calculated metrics)</strong></p><ol class="incremental" type="1"><li><p>Run data-processing (no need to update data-preprocessing if no change where made there).</p></li><li><p>Save data-processing</p></li></ol><p></p></th></tr></thead><tbody></tbody></table>





## Test converter and API before deploying

### Test converter

Tests are in test_data_processing.py (cfr [Test : Knime to Python converter](onenote:#Test%20%20Knime%20to%20Python%20converter&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={860C7EBA-031A-43B9-839B-A87C205CEA00}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).

To save time, you could directly use Test_data_processing_output_comparison.
-   If no error message, no need to use Test_current_data_processing_for_debug_excel_reader
-   If error message, use Test_current_data_processing_for_debug_excel_reader tosee where the problem comes from.

While Test_data_processinf_output_comparison runs without sending error message, test if scenario update do not change the output values with test_data_processing_multi_run.



### Test API

Test for the API is set in scripts folders (cfr [Test : API](onenote:#Test%20%20API&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7793D578-71A9-469F-A6E1-7399AD36EE4A}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).

Usually, we just run the API to see if everything is alright but we do not test request on it, except if we see there is some error while using the web application (eg. Some graphs do not appear properly).

If problem with the web application, see for which request we have a problem and apply the same request here, in the test.





## Knime2Python

Once each test has run without error message, we pull knime2python folder on remote. To do so, you can use the Terminal of PyCharm. Command line are the same than the ones used for Knime (git add --all, ...).

![Texte de remplacement généré par une machine: Terminal: Local Microsoft Windows [version 18363.720 (c) 2B19 Microsoft Corporation. Tous droits réset"'és. C : XXCaIc Nknime2python> 6: TODO 2: Version Control Terminal Bfthon Console ](../media/Technical-Documentation-Deployment-image1.png){width="5.0in" height="3.53125in"}







MobaXTerm

1.  Pull the last version of knime2python folder.

2.  Build and run dockers (more informations : [MobaXTerm](onenote:#MobaXTerm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={D305507A-BF69-46A1-AB14-BDD34B4CF290}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).





## Configuration

They are multiples configuration files to update when you want to do a release (docker config files). We also want to update external tools (like scenario benchmarking tool) during each release. Tools has been build to support these operation (see [Release Support](onenote:#Release%20Support&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).

