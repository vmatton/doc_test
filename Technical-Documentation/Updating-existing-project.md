<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Updating existing project](#updating-existing-project)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Updating existing project

Created: 2020-04-15 08:38:22 +0200

Modified: 2020-04-27 10:15:23 +0200

---




**Each time you modify a project, you should make sure your modifications do not impact the others projects. Otherwise, apply your modifications to these projects as well !**

**This is particularly true when you add/remove metrics/levers !**



If you just need to add a new scenario on the PowerBI, cfr [Scenario Builder](onenote:#Scenario%20Builder&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E0C26574-D2CA-4B89-B165-ADED2D317096}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one).



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>Note</strong> : if modifications are made in a module whose data you use :</p><ul class="incremental"><li><p>Make sure these modifications impacts only the values and not the file structure (column names, number of columns, …)</p></li></ul><blockquote><p><strong>If only values are changed</strong> ;</p></blockquote><ul class="incremental"><li><p>If these changes are in the same value range than before, nothing special to do</p></li><li><p>If not : copy/paste again the <strong>output</strong> file into your <strong>data</strong> file to update it (on local server). On remote server ; nothing to do as data are all written in a shared Google Sheet (everything is automatically updated)</p></li></ul><ul class="incremental"><li><p><strong>If modifications are made on the file structure</strong> ; make sure the output could still be connected to your input (if not : modifications should be done on your module or the module which has changed to make the "output – input" link works). Once this is done, copy/paste the <strong>output</strong> file into your <strong>data</strong> file to update it (on local server). On remote server ; nothing to do as data are all written in a shared Google Sheet (everything is automatically updated)</p></li></ul><p></p></th></tr></thead><tbody></tbody></table>



**General process is** :



![LOCAL DESKTOP User 1 REMOTE LOCAL DESKTOP User 2 REMOTE Modification in workflow (ex. bld) /! Shared nodes when modifications are done /! Commit and Push modified workflow (ex. bld) Remote server Pull modified workflow (ex. bld) Updates interactions nodes In order to catch modifications in shared nodes (ex. bld) Run data-preprocessing Run data-processing Check it properly runs Save data-processing Check converter (knime2python) properly runs Commit and Push interactions Commit and Push knime2python (if modified) Remote server Run all the process to update PowerBi/web site ](../media/Technical-Documentation-Updating-existing-project-image1.png){width="6.520833333333333in" height="5.3125in"}





