<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Update API](#update-api)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Update API

Created: 2020-04-15 08:52:30 +0200

Modified: 2021-08-10 11:51:47 +0200

---




Entry point = app.py



Build-app :
-   Get configuration values (in config files) (ex. Lever path, ...)
-   Interact with cache
-   Is launched by dockers (we reference build-app directly to gunicorn)



Model :
-   Runner = compute outputs (according to aggregation id, ...)
-   Cube trans = transformation of "the cube" => cfr aggregation id in interface

    -   Cube ini = read interface and keep in memory variables that are computed by Knime and the one that need to be created with aggregation id

**=> If we modify the type of graphs / output table ; this should be changed in depth !**



Build-model :
-   Create workflow builder
-   Run scenarios



Model-init (and rest of the code for model) :
-   Try to abstract some functions



Get-results :
-   Look inside cache. If exists in cache : get results -- else : do init steps + run runner



Configuration.py : get all configuration values and apply few changes



Cache.py = create a cache and save it in the DB




