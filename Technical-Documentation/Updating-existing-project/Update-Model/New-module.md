<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [New module](#new-module)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# New module

Created: 2020-04-15 08:47:21 +0200

Modified: 2020-05-29 11:22:23 +0200

---

|-----------------------------|



If new project, cfr [Create new project](onenote:#Create%20new%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={2BBA0D6C-CB72-46C2-B669-136224CC3529}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) as first steps to be done.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>When a modification is done on one project, make sure it is also updated in each other project linked to XCalc (eg. ELIA, BE2050, EU2050)</strong></p><p></p></th></tr></thead><tbody></tbody></table>



Bitbucket

Cfr *User Manual > Bitbucket > Install / Create account* - section "New module".



Local Desktop

Add a folder for your module into the dev folder ;



![dev (in your project file) > interactions > data > YOUR PROJECT NAME > configuration > YOUR PROJECT NAME > output > YOUR PROJECT NAME > _common (ok : no need to be changed) > ustry an > data orot er u es included new module > YOUR PROJECT NAME > output > YOUR PROJECT NAME ](../../../media/Technical-Documentation-New-module-image1.png){width="2.84375in" height="5.0in"}





Clone the remote module here. As the gitignore file is created on Bitbucket, it should be clone here



Your folder should, at least, contains the following files (cfr *User Manual > Knime > Using Knime*). If it does not, add them manually.
-   Data
-   Output
-   Workflows
-   Metanodes
-   A gitignore file



![( 0 ) 》 XCalc 》 dev 》 ai 「 _pollution 》 Nom 冖 」 README.md 冖 」 C 工 ANGELOG.md 一 3 9 ゴ 9n0 「 e ロ wo 「 一 ow い ](../../../media/Technical-Documentation-New-module-image2.png){width="2.5625in" height="2.65625in"}





| Google Sheets : data collection |
|---------------------------------|



Create new pages for your module (OTS, LL and CP).

Be sure you select the proper project google sheet for data collection. Ideally, you should be sure your data could be used by any of the existing projects (BE, EU, ELIA, ...) and workflows still run for all these projects !



**More information about this step on :** *User Manual > Google Sheet > Data Collection*



**Note** : if your project use data from other projects and you do not want to copy/paste all of them, cfr [Create new project](onenote:#Create%20new%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={2BBA0D6C-CB72-46C2-B669-136224CC3529}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) "Note about data collection".



**On interface** : create the RAW page for your module (will collect data from your module).





| Knime : isolated module and _common |
|--------------------------------------|

**Remember to pull a module before working on it !**



Your module

Note : before creating your module, you need to create input table coming from other modules (cfr section below). Therefore, modify each module you relied on, run them and copy/paste their output to your data folder.

Create you module and group actions in metanodes to be used easily in _interactions. Be sure your metanodes are shared (save them in ***dev/your_new_module/metanodes***).

For more details about the structure, cfr : [Knime structure](onenote:#Knime%20structure&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5CF3B63A-43BB-44BA-83D6-0ACD4D5E0A7A}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)

For more details about best practices : cfr *User Manual > Knime*



Other module you import the data from

Create table that will be used as input in your module. Send them to the Pathway to Explorer metanodes and to an Excel writer metanodes.

More information on *User Manual > Knime > Best Practices*.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p>Steps</p><p></p><p>1) For each module you relied on :</p><p></p><ol class="incremental" type="A"><li><p>Go to their workflow, select their main metanode (where all calculations are made). Disconnect it and then change the setup ==&gt; add a new out port. Do not forget to add description to it !</p></li></ol><p></p><p><img src="../../../media/Technical-Documentation-New-module-image3.png" style="width:6.71875in;height:3.3125in" alt="Configure... Execute Remove gualty remove fitter Execute and Open Views update v + att On: pos Cancel ge n em Reset —j Edit Node Description... .1 dui New Workflow Annotation 00 Connect selected nodes OXO Disconnect selected nodes Create Metanode... Create Component... 21 aui&#39;din( Component Interactive View: 2.1 Buildings 2.1 Buildinl Compare Nodes with qual Show Flow Variable Ports Cut Copy Shift+FIO Alt+F2 Ctrl+L Ctrl+Shift+L Open Expand Setupm Convert to Metanode Share... Update Link Ctrl+A1t+U " /></p><p></p><p><img src="../../../media/Technical-Documentation-New-module-image4.png" style="width:6.1875in;height:4.5625in" alt="Setup Component Wizard Specfiy the name of the node and define the number and type of the desired in and out pots. Component Name: 2.1 Buildings In Ports: in_l (Data) in_2 (Data) in_3 (Data) in_4 (Data) in_5 (Data) in 6 Data Add Meta Port Port Type: x Out Ports: out I (Data) out 2 (Data) out 3 (Data) out 4 (Data) out 5 (Data) out 6 (Data) out_7 (Data) out 8 (Data) Finish Remove Cancel " /></p><p></p><p></p><ol class="incremental" start="2" type="A"><li><p>Open the metanode. Create the output table needed and connect it to the new port you just have created.</p></li></ol><p></p><p><img src="../../../media/Technical-Documentation-New-module-image5.png" style="width:3.73958in;height:3.46875in" /></p><p></p><ol class="incremental" start="3" type="A"><li><p>Once it is done, make sure you share your netanode !</p></li></ol><p></p><p><img src="../../../media/Technical-Documentation-New-module-image6.png" style="width:5in;height:2.61458in" alt="Remove gualty c remove fitter update v + att ge n em&#39;* —j .1 Buil OXO 21 Building 2.1 Buildin with qual Configure... Execute Execute and Open Views Cancel Reset Edit Node Description... New Workflow Annotation Connect selected nodes Disconnect selected nodes Create Metanode... Create Component... Component Interactive View: 2.1 Buildings Compare Nodes Show Flow Variable Ports Cut Copy Paste r other modu Shift+FIO Alt+F2 Ctrl+L Ctrl+Shift+L Open Ex pand Setup Convert to Metanode Sharem Update Link Disconnect Link Chanae Link Woe... Ctrl+A1t+U " /></p><p></p><ol class="incremental" start="4" type="A"><li><p>Add a metanode <em>XCalc - Write data</em> for other module in order to give excel file from this module (all excel file are written in dev/module_name/output/project_name and sould be copy/paste in dev/new_module_name/data/project_name)</p></li></ol><p></p><p><img src="../../../media/Technical-Documentation-New-module-image7.png" style="width:5in;height:4.53125in" alt="e qualty check„ fitterof&quot;öelgium tion meta nodes On possibilty of double lge n emissions ! 2.1 Buildings Buildings module I Buildings - backup vith quality checks h I Buildings module xcecl- r module A Dialog - 43762 - XCaIc - Write data for othe... Optons Flow Variables Memory P olicy Source module Destina bon module äir_pollu bon Change Change OK e cute Apply alc - Wr data for other m ule Air aualty " /></p><p></p><ol class="incremental" start="5" type="A"><li><p>Add link to the Update interface L2 – Google Sheet metanode</p></li></ol><p></p><p><img src="../../../media/Technical-Documentation-New-module-image8.png" style="width:2.84375in;height:2.64583in" alt="Write output to Excel files anc Google Sheets Update interface L2 GoogleSheet - BLD " /></p><p><img src="../../../media/Technical-Documentation-New-module-image9.png" style="width:5in;height:3.59375in" /></p><p></p><blockquote><p>Choose appropriate module name ==&gt; if it does not exist ; modify option in this metanode (more information on the first red frame here : <a href="onenote:#Interface&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Interface</a>)</p><p><strong>/! Before doing that : pull _common and when modified commit and push _common</strong></p></blockquote><p></p><p></p><ol class="incremental" start="6" type="A"><li><p>Run your module to be sure everything is alright. Then, reset all nodes and save workflow (reset nodes decreases the loading time).</p></li></ol><p></p><p><img src="../../../media/Technical-Documentation-New-module-image10.png" style="width:4.3125in;height:4.1875in" alt="Column v Query DB colu Database (le v Manipu Workflow Abstr Configurati NME Explorer READM v buildings configu metano output workfl buildi climate o Rename... Configure... Execute... Cancel execution Workflow Credentials... Workflow Variables... Edit Meta Information... Refresh Copy Location Compare Cut Copy Paste Ctrl+X Ctrl+C Ctrl+V " /></p><p></p><p></p></th></tr></thead><tbody></tbody></table>



_common

Change the google_sheet_ids.xlsx file ; add your module to **each existing project** !

Change the short_long_names.xlsx file.

Cfr [_common](onenote:#_common&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5FEB34D3-BC69-490D-BB58-58B5DD156BD6}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more details.



**Do not forget to share your metanodes and push your modifications (_common, your module, other modules your relied on).**



**Be sure all the modules you relied on (if new metrics come from them) and your own module have been run on local to update the Interface Google Sheet.**





| Google Sheet : interface and scenario builder |
|-----------------------------------------------|



Interface

Create a sheet for your module, it will retrieve values from the RAW page. Add formula to this page.

In each page of the modules your relied on (receive data from them or sending data to them) ; add a column for your module.

Change Lever page and Pathway to Explorer page.

Make sure accuracy is set to 100%.

All information are available at : [Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one).



Once your modifications are made :



![1 download interlace google sheet into configuration/XX folder using excel format 2 rename the file: intenace_xlsx XX is the name of the project (BE, EL', ELIA VLAIO) ](../../../media/Technical-Documentation-New-module-image11.png){width="3.71875in" height="1.53125in"}





Scenario Builder

Each time the interface google sheet has been modified, update the variable_list page with new metrics and check in the variable page if metrics are still available in both PowerBi and Pathway Explorer.

Cfr [Scenario Builder](onenote:#Scenario%20Builder&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E0C26574-D2CA-4B89-B165-ADED2D317096}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



Once they are modified, export them and add them to appropriate file (eg. _interaction / configuration / *project_name* / interface.xlsx for the interface google sheet). More information are available on the links mentioned above.



Do not forget to update the lever_position.json file (cfr [Lever_position.json](onenote:#Lever_position.json&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E8945928-71D4-43AA-A17B-E6EC07067912}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))





| Knime : _interactions |
|------------------------|

**In Quickscan : add your module to the workflow_dict and metanode_dict of each project (cfr [Test : Quickscan](onenote:#Test%20%20Quickscan&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={DEC080F0-83A1-4C22-9E64-5321074C4129}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).**

Change data-preprocessing and data-processing **(after using Quickscan !)**.

All information are available at : [Knime structure](onenote:#Knime%20structure&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5CF3B63A-43BB-44BA-83D6-0ACD4D5E0A7A}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)

**Note : when new module, you should send you PathWay Explorer output table to "Assemble all Pathway Explorer output" metanode.**

**When it is done, you have to change the converter to reflect this : cfr assemble_all_input_metanodes.py - class assembleAllOutputNode : add a new in_ports node**

![Visut semMe Pathway output ("74) ](../../../media/Technical-Documentation-New-module-image12.png){width="1.8958333333333333in" height="1.9583333333333333in"}



![class : (self, u, •L file, node _ type, (i d, xml_file, node_type, 2: None, 3: S: None, 7: None, 8 : 'kne, g : None, lø:uone, 12 : def start timer() t • start self. (t, "01JTL0", ' ' ( EMPTY) 811 Output ](../../../media/Technical-Documentation-New-module-image13.png){width="10.114583333333334in" height="3.46875in"}





**Major steps are described here** :

![1 git pull the repository ot the sector 2 test the new metanode using the 'quickscan' in the knime2pytnon script folder. It there is errors, ask the sector leader to adapt his knime workflow 3 open data-processing in Knime 4. open data-preprocessing In Knme 5. update the metanodes related to the sector (if there is no updates, there is a lot of chances that the sector leader did not save the metanodes as template) ](../../../media/Technical-Documentation-New-module-image14.png){width="6.0625in" height="1.25in"}

![6. run the data-preprocessing followed by data-processing witn the different google_sneet_pretix (check that all the google_sheet_prefix are running, it not ask the sector leader to adapt) 7. save the workflows & test the converter by running the tests in Pycnarm ](../../../media/Technical-Documentation-New-module-image15.png){width="6.0625in" height="0.65625in"}

![you can ask the sector leader about the metanodes that were changed to avoid opening data-processing or data-preprocessing it it is not necessary ](../../../media/Technical-Documentation-New-module-image16.png){width="6.5in" height="0.5208333333333334in"}



**Do not forget to push your modifications !**





| Deployment : web application and PowerBI |
|------------------------------------------|

**In configuration files of each project (cfr [Configuration Files](onenote:#Configuration%20Files&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={74BD6C1F-8EF9-4961-B3AC-B17C62138312}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)) : add an output node linked to your module (cfr variable output_nodes).**

**Before testing the converter, you should add your module node name into (cfr** [Test : Knime to Python converter](onenote:#Test%20%20Knime%20to%20Python%20converter&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={860C7EBA-031A-43B9-839B-A87C205CEA00}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)**) :**
-   **Test_data_processing_output_comparison**
-   **Test_data_processing_out_multirun**



If any change to the knime2python script :

![Push knime2python changes to bitbucket git@bitbucketorg:cIimact/knime2pythomgit add --- ---aZI ---am "message" push (Jit tag vX.X) push ------Cags) ](../../../media/Technical-Documentation-New-module-image17.png){width="3.625in" height="2.28125in"}



![Update server update knime2python folder update api db docker (pon 5001) docker---czmpzse up ---d Lags -build-arg ssy KEY= ---'docker Isa) " update api model docker (port 5000) ubdate the versionvkxwiththe correct version and the docker-compose yml file accordingly ---C cL±macC/ZecaLc: ---C climact/bacalc: docker build docker---czmpzse up ---d Lags reset mysql - tind the mysql_id using mysgl docker---compzse up ---d --Zu±Ld-arg MCDEL SSE EEY="; (cat " mode L command => to do only when data or model changes (no need to reset mysql if it is only 'cosmetical' changes) ](../../../media/Technical-Documentation-New-module-image18.png){width="12.625in" height="4.208333333333333in"}
























