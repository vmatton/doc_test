<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [New branch](#new-branch)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# New branch

Created: 2020-04-15 08:48:59 +0200

Modified: 2020-09-03 16:12:18 +0200

---

|-----------------------------|



If new project, cfr [Create new project](onenote:#Create%20new%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={2BBA0D6C-CB72-46C2-B669-136224CC3529}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) as first steps to be done.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>When a modification is done on one project, make sure it is also updated in each other project linked to XCalc (eg. ELIA, BE2050, EU2050)</strong></p><p></p></th></tr></thead><tbody></tbody></table>





| Google Sheets : data collection |
|---------------------------------|



**Clean it according to what you need (more information on** [Create new project](onenote:#Create%20new%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={2BBA0D6C-CB72-46C2-B669-136224CC3529}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)**).**



**Note** : if your project use data from other projects and you do not want to copy/paste all of them, cfr [Create new project](onenote:#Create%20new%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={2BBA0D6C-CB72-46C2-B669-136224CC3529}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) "Note about data collection".



Clean the google sheet to keep only pages related to modules you keep in your project (OTS, LL and CP). Eg. Vlaio : keeps only transport.

If you need to add new module, cfr [New module](onenote:#New%20module&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={4344E2DF-514A-4233-AE84-0DA44AA58C72}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for additional information about it at this step.

**More information about this step on** *User Manual > Google Sheet > Data Collection*



*As you are working on a branch of XCalc, **no need** to make sure your data could be used by any of the existing projects (BE, EU, ELIA, ...) and workflows still run for all these projects !*





| Knime : isolated module and _common |
|--------------------------------------|

**Remember to pull a module before working on it !**



Clean up modules and change them according to your project.

**Do not forget to share your metanodes and push your modifications (_common, your module, other modules your relied on).**

**Be sure all the modules you relied on (if new metrics come from them) and your own module have been run on local to update the Interface Google Sheet.**



| Google Sheet : interface and scenario builder |
|-----------------------------------------------|



Interface

Make sure change you made are reflected in the Interface Google Sheet ; output and input columns should be corrected according to which modules you deleted or added...

Make sure accuracy is set to 100%.

All information are available at : [Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one).



Once your modifications are made :

![1 download interlace google sheet into configuration/XX folder using excel format 2 rename the file: intenace_xlsx XX is the name of the project (BE, EL', ELIA VLAIO) ](../../../media/Technical-Documentation-New-branch-image1.png){width="3.71875in" height="1.53125in"}





Scenario Builder

Each time the interface google sheet has been modified, update the variable_list page with new metrics and check in the variable page if metrics are still available in both PowerBi and Pathway Explorer.

Cfr [Scenario Builder](onenote:#Scenario%20Builder&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E0C26574-D2CA-4B89-B165-ADED2D317096}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



Once they are modified, export them and add them to appropriate file (eg. _interaction / configuration / *project_name* / interface.xlsx for the interface google sheet). More information are available on the links mentioned above.



Do not forget to update the lever_position.json file (cfr [Lever_position.json](onenote:#Lever_position.json&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E8945928-71D4-43AA-A17B-E6EC07067912}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))





| Knime : _interactions |
|------------------------|



All the information is here : [Knime structure](onenote:#Knime%20structure&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5CF3B63A-43BB-44BA-83D6-0ACD4D5E0A7A}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)





| Deployment : web application and PowerBI |
|------------------------------------------|



If any change to the knime2python script :

![Push knime2python changes to bitbucket git@bitbucketorg:cIimact/knime2pythomgit add --- ---aZI ---am "message" push (Jit tag vX.X) push ------Cags) ](../../../media/Technical-Documentation-New-branch-image2.png){width="3.625in" height="2.28125in"}



![Update server update knime2python folder update api db docker (pon 5001) docker---czmpzse up ---d Lags -build-arg ssy KEY= ---'docker Isa) " update api model docker (port 5000) ubdate the versionvkxwiththe correct version and the docker-compose yml file accordingly ---C cL±macC/ZecaLc: ---C climact/bacalc: docker build docker---czmpzse up ---d Lags reset mysql - tind the mysql_id using mysgl docker---compzse up ---d --Zu±Ld-arg MCDEL SSE EEY="; (cat " mode L command => to do only when data or model changes (no need to reset mysql if it is only 'cosmetical' changes) ](../../../media/Technical-Documentation-New-branch-image3.png){width="12.625in" height="4.208333333333333in"}







