<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [New lever (module modification)](#new-lever-module-modification)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# New lever (module modification)

Created: 2020-04-15 08:44:27 +0200

Modified: 2020-04-27 10:18:48 +0200

---


If new project, cfr [Create new project](onenote:#Create new%20project&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={34d1de93-e3f6-4baa-a8d4-5c2da89ac546}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FCreate%C2%A0new%20project%7C34d1de93-e3f6-4baa-a8d4-5c2da89ac546%2F%29&wdorigin=703&wdpreservelink=1)) as first steps to be done.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>When a modification is done on one project, make sure it is also updated in each other project linked to XCalc (eg. ELIA, BE2050, EU2050)</strong></p><p></p></th></tr></thead><tbody></tbody></table>





| Bitbucket and local desktop |
|-----------------------------|



Make sure you get the latest version of the module with a pull request.



| Google Sheets : data collection |
|---------------------------------|



In the ll_page corresponding to the module you are working on, add new values for your levers. For more details, cfr [Data Collection](onenote:User%20manual.one#Data%20Collection&section-id={e0f60df4-e151-48b5-804a-44e2e564b6c9}&page-id={49621430-cf95-41ad-9bbf-4513a94f6da5}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28User%20manual.one%7Ce0f60df4-e151-48b5-804a-44e2e564b6c9%2FData%20Collection%7C49621430-cf95-41ad-9bbf-4513a94f6da5%2F%29&wdorigin=703&wdpreservelink=1)) (section Level of levers).



Add your levers to the metrics_to_names page (in data collection) (cfr [Data Collection](onenote:User%20manual.one#Data%20Collection&section-id={e0f60df4-e151-48b5-804a-44e2e564b6c9}&page-id={49621430-cf95-41ad-9bbf-4513a94f6da5}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28User%20manual.one%7Ce0f60df4-e151-48b5-804a-44e2e564b6c9%2FData%20Collection%7C49621430-cf95-41ad-9bbf-4513a94f6da5%2F%29&wdorigin=703&wdpreservelink=1)))



Ideally, you should be sure your data could be used by any of the existing projects (BE, EU, ELIA, ...) and workflows still run for all these projects !





| Knime : isolated module and _common |
|--------------------------------------|



**Remember to pull a module before working on it !**

In your module workflow, add a *Projections generator* metanode (coming from the Data Import OTS-FTS). Do not forget to link it to a *DataTypeSelection* metanode.



![calc - Pr generat identiel Levers Soli bio techn y in buil gs heating Ote We assume histori data xist(t of po ere, they are fictivæ Change them OR change the mo acc rding availibili f data or the moment t/ TWh che k unit are the sa Column Filter BECalc - Data Impo OTS-FTS (GoogleSh Keep only solid-bio String Mang»ulation (Variable) Da Yte Row Filter Remove 2015 (already in levers) ](../../../media/Technical-Documentation-New-lever-(module-modification)-image1.png){width="5.0in" height="2.6875in"}



Apply a level selection to it (cfr Single Lever interpolation) and join it to the other levers (if any).



![Slider Input Eucalc - Single Lever Interpolation Joiner EUCalc - Single ever Interpolatio ](../../../media/Technical-Documentation-New-lever-(module-modification)-image2.png){width="3.9375in" height="2.7708333333333335in"}



Then do the modifications in your workflow in order to integrate this lever into your calculations.



**Do not forget to share your metanodes and push you workflow !**





| Google Sheet : interface and scenario builder |
|-----------------------------------------------|



Interface

Add your lever to the *Levers* page of the Interface. For more information, cfr [Interface](onenote:#Interface&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={ab4ab288-41d0-4c4f-877c-9460146a67f9}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FInterface%7Cab4ab288-41d0-4c4f-877c-9460146a67f9%2F%29&wdorigin=703&wdpreservelink=1)).

Make sure the accuracy is at 100% for the levers.



Once your modifications are made :

![1 download interlace google sheet into configuration/XX folder using excel format 2 rename the file: intenace_xlsx XX is the name of the project (BE, EL', ELIA VLAIO) ](../../../media/Technical-Documentation-New-lever-(module-modification)-image3.png){width="3.71875in" height="1.53125in"}





Scenario builder

Add you lever to the *Levers* page of the Scenario builder. For more information, cfr [Scenario Builder](onenote:#Scenario%20Builder&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={99ef299c-b9a2-45a9-8fbb-f1e6f5b00f56}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FScenario%20Builder%7C99ef299c-b9a2-45a9-8fbb-f1e6f5b00f56%2F%29&wdorigin=703&wdpreservelink=1))



Once they are modified, export them and add them to appropriate file (eg. _interaction / configuration / *project_name* / interface.xlsx for the interface google sheet). More information are available on the links mentioned above.



Do not forget to update the lever_position.json file (cfr [Lever_position.json](onenote:#Lever_position.json&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={5bd89d5b-670a-4116-81c7-84c1fa755586}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FLever_position.json%7C5bd89d5b-670a-4116-81c7-84c1fa755586%2F%29&wdorigin=703&wdpreservelink=1)))



| Knime : _interactions |
|------------------------|



![1 git pull the repository ot the sector 2 test the new metanode using the 'quickscan' in the knime2pytnon script folder. It there is errors, ask the sector leader to adapt his knime workflow 3 open data-processing in Knime 4. open data-preprocessing In Knme 5. update the metanodes related to the sector (if there is no updates, there is a lot of chances that the sector leader did not save the metanodes as template) 6. run the data-preprocessing followed by data-processing witn the different google sneet_pretix (check that all the google_sheet_prefix are running, it not ask the sector leader to adapt) 7. save the workflows & test the converter by running the tests in Pycnarm you can ask the sector leader about the metanodes that were changed to avoid opening data-processing or data-preprocessing it it is not necessary ](../../../media/Technical-Documentation-New-lever-(module-modification)-image4.png){width="7.15625in" height="2.6875in"}



More informations here : [Knime structure](onenote:#Knime%20structure&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5CF3B63A-43BB-44BA-83D6-0ACD4D5E0A7A}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one).



**Note** : in data-preprocessing, do not forget to link your lever to the level data to Excel sheet.

![](../../../media/Technical-Documentation-New-lever-(module-modification)-image5.png){width="2.4895833333333335in" height="5.0in"}





| Deployment : web application and PowerBI |
|------------------------------------------|



**In configuration files of each project (cfr [Configuration Files](onenote:#Configuration%20Files&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={74BD6C1F-8EF9-4961-B3AC-B17C62138312}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)) : add lever value in the appropriate place. This should be done in arrays named scenario and scenario_for_initialization.**



If any change to the knime2python script :

![Push knime2python changes to bitbucket git@bitbucketorg:cIimact/knime2pythomgit add --- ---aZI ---am "message" push (Jit tag vX.X) push ------Cags) ](../../../media/Technical-Documentation-New-lever-(module-modification)-image6.png){width="3.625in" height="2.28125in"}



![Update server update knime2python folder update api db docker (pon 5001) docker---czmpzse up ---d Lags -build-arg ssy KEY= ---'docker Isa) " update api model docker (port 5000) ubdate the versionvkxwiththe correct version and the docker-compose yml file accordingly ---C cL±macC/ZecaLc: ---C climact/bacalc: docker build docker---czmpzse up ---d Lags reset mysql - tind the mysql_id using mysgl docker---compzse up ---d --Zu±Ld-arg MCDEL SSE EEY="; (cat " mode L command => to do only when data or model changes (no need to reset mysql if it is only 'cosmetical' changes) ](../../../media/Technical-Documentation-New-lever-(module-modification)-image7.png){width="12.625in" height="4.208333333333333in"}











