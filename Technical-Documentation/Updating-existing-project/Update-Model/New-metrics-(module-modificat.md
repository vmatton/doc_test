<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [New metrics (module modification)](#new-metrics-module-modification)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# New metrics (module modification)

Created: 2020-04-15 08:46:04 +0200

Modified: 2020-04-27 10:19:02 +0200

---


If new project, cfr [Create new project](onenote:#Create new%20project&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={34d1de93-e3f6-4baa-a8d4-5c2da89ac546}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FCreate%C2%A0new%20project%7C34d1de93-e3f6-4baa-a8d4-5c2da89ac546%2F%29&wdorigin=703&wdpreservelink=1)) as first steps to be done.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>When a modification is done on one project, make sure it is also updated in each other project linked to XCalc (eg. ELIA, BE2050, EU2050)</strong></p><p></p></th></tr></thead><tbody></tbody></table>





| Bitbucket and local desktop |
|-----------------------------|



Make sure you get the latest version of the module with a pull request.



| Google Sheets : data collection |
|---------------------------------|



***If input metrics are added or deleted and they do not come from other modules*** :

Change your metrics (ots data or constant data). For more details, cfr [Data Collection](onenote:User%20manual.one#Data%20Collection&section-id={e0f60df4-e151-48b5-804a-44e2e564b6c9}&page-id={49621430-cf95-41ad-9bbf-4513a94f6da5}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28User%20manual.one%7Ce0f60df4-e151-48b5-804a-44e2e564b6c9%2FData%20Collection%7C49621430-cf95-41ad-9bbf-4513a94f6da5%2F%29&wdorigin=703&wdpreservelink=1)) (section Level of levers).



Ideally, you should be sure your data could be used by any of the existing projects (BE, EU, ELIA, ...) and workflows still run for all these projects !





| Knime : isolated module and _common |
|--------------------------------------|



**Remember to pull a module before working on it !**

**More details are available here :** [Knime structure](onenote:#Knime%20structure&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={dce242e0-5fee-4030-802a-20abce0cef86}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FKnime%20structure%7Cdce242e0-5fee-4030-802a-20abce0cef86%2F%29&wdorigin=703&wdpreservelink=1)).



***If input metrics are added or deleted and they do not come from other modules***

Import metrics with appropriate metanodes (according to their type ; OTS, CP) ; this stepshould be encapsulated into a shared metanodes.



In the preprocessing part, do not forget to use a DataTYpe Selection metanode



![uonoa1asadQmeo ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image1.png){width="3.71875in" height="2.21875in"}





In the processing part : integrate the metrics into your calculation process.



***If input metrics are added or deleted and they come from another modules***

In each module the data comes from, be sure there is an output port that could be used as input for your module. If no, create this.

Run workflows whose you want to use data as input. Copy/paste output data (set in the module / output /*project_name* folder) into your module / data /*project_name* folder.

Add a *Data from other module* metanode in your module workflow process and link it to your shared metanode where all calculations are done. Then change your process to integrate these new metrics.



![XCaIc - Data from other module Buildings ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image2.png){width="1.25in" height="1.2083333333333333in"}



Do not forget to link the *Data from other module* metanode to the *Update Interface L2* metanode.



![](../../../media/Technical-Documentation-New-metrics-(module-modification)-image3.png){width="5.0in" height="1.09375in"}





**Note** : if you need to import data from a module whose you already import some data from, try to keep them grouped in a unique table, instead of using two (or more) *Data from other module* metanodes. At the end, you should have only one *Data from other module* metanode by module whose data are imported from.



***If new metrics are send as output***

Be sure they are integrated in the table send to Pathway to Explorer.



![6.2 Air pollution Pivot to long format Unpivoting Pivot to long format Regex Split Split metric Regex Split Split metric Column Rename Metric and Dimensions Column Rename Metric and Dimensions Update interface L2 - GoogleSheet - AIR ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image4.png){width="5.0in" height="3.375in"}





***If new calculation process***

Change your process inside the shared metanode where all calculations are done.



**If there is any calibration inside your metanode, check if everyything is still alright after changes have been done.**



**Do not forget to share your metanodes and push your modifications.**



**Be sure all the modules you relied on (if new metrics come from them) and your own module have been run on local to update the Interface Google Sheet.**





| Google Sheet : interface and scenario builder |
|-----------------------------------------------|



Interface

**More details are available here :** [Interface](onenote:#Interface&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={ab4ab288-41d0-4c4f-877c-9460146a67f9}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FInterface%7Cab4ab288-41d0-4c4f-877c-9460146a67f9%2F%29&wdorigin=703&wdpreservelink=1)).



***If input metrics are added or deleted and they come from another modules***

Check the new metrics appeared on the page of these module page and accuracy is set to 100%.



***If new metrics are send as output***

Check the new metrics appeared on the column "Pathway to Explorer" of the page related to your module.

Check accuracy is set to 100% and add the metrics to the "Pathway to Explorer" page.



Once your modifications are made :



![1 download interlace google sheet into configuration/XX folder using excel format 2 rename the file: intenace_xlsx XX is the name of the project (BE, EL', ELIA VLAIO) ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image5.png){width="3.71875in" height="1.53125in"}





Scenario builder

Each time the interface google sheet has been modified, update the variable_list page with new metrics and check in the variable page if metrics are still available in both PowerBi and Pathway Explorer.

**More details are available here :** [Scenario Builder](onenote:#Scenario%20Builder&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={99ef299c-b9a2-45a9-8fbb-f1e6f5b00f56}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FScenario%20Builder%7C99ef299c-b9a2-45a9-8fbb-f1e6f5b00f56%2F%29&wdorigin=703&wdpreservelink=1)).



Once they are modified, export them and add them to appropriate file (eg. _interaction / configuration / *project_name* / interface.xlsx for the interface google sheet). More information are available on the links mentioned above.



*As there was no change in the levers, no need to update the lever_position.json file (cfr [Lever_position.json](onenote:#Lever_position.json&section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&page-id={5bd89d5b-670a-4116-81c7-84c1fa755586}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FLever_position.json%7C5bd89d5b-670a-4116-81c7-84c1fa755586%2F%29&wdorigin=703&wdpreservelink=1)))*





| Knime : _interactions |
|------------------------|

![1 git pull the repository ot the sector 2 test the new metanode using the 'quickscan' in the knime2pytnon script folder. It there is errors, ask the sector leader to adapt his knime workflow 3 open data-processing in Knime 4. open data-preprocessing In Knme 5. update the metanodes related to the sector (if there is no updates, there is a lot of chances that the sector leader did not save the metanodes as template) ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image6.png){width="6.0625in" height="1.25in"}



If modifications are done in new shared metanodes, add these nodes to the workflows.

More information here : [Knime structure](onenote:#Knime%20structure&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5CF3B63A-43BB-44BA-83D6-0ACD4D5E0A7A}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



![6. run the data-preprocessing followed by data-processing witn the different google_sneet_pretix (check that all the google_sheet_prefix are running, it not ask the sector leader to adapt) 7. save the workflows & test the converter by running the tests in Pycnarm ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image7.png){width="6.0625in" height="0.65625in"}

![you can ask the sector leader about the metanodes that were changed to avoid opening data-processing or data-preprocessing it it is not necessary ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image8.png){width="6.5in" height="0.5208333333333334in"}





| Deployment : web application and PowerBI |
|------------------------------------------|



If any change to the knime2python script :

![Push knime2python changes to bitbucket git@bitbucketorg:cIimact/knime2pythomgit add --- ---aZI ---am "message" push (Jit tag vX.X) push ------Cags) ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image9.png){width="3.625in" height="2.28125in"}



![Update server update knime2python folder update api db docker (pon 5001) docker---czmpzse up ---d Lags -build-arg ssy KEY= ---'docker Isa) " update api model docker (port 5000) ubdate the versionvkxwiththe correct version and the docker-compose yml file accordingly ---C cL±macC/ZecaLc: ---C climact/bacalc: docker build docker---czmpzse up ---d Lags reset mysql - tind the mysql_id using mysgl docker---compzse up ---d --Zu±Ld-arg MCDEL SSE EEY="; (cat " mode L command => to do only when data or model changes (no need to reset mysql if it is only 'cosmetical' changes) ](../../../media/Technical-Documentation-New-metrics-(module-modification)-image10.png){width="12.625in" height="4.208333333333333in"}














