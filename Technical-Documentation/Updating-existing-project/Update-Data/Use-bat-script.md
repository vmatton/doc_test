<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Use bat script](#use-bat-script)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Use bat script

Created: 2021-07-14 17:07:04 +0200

Modified: 2021-07-14 17:09:02 +0200

---


Salut à tous,



Comme promis, voilà la marche à suivre quand vous avez modifié vos données XCalc**èà faire à chaque fois que vous avez modifié vos données!**



1.  Modifier vos données dans les google sheets

2.  Avant d'ouvrir Knime:**utiliser le code .bat pour puller tous les modules**(y compris le vôtre) + mettre à jour les données dans le folder data/EU de votre module (cfr ci-dessous = mon ancien email)

3.  Ouvrir Knime

    a.  Ouvrir votre module

    b.  S'assurer que la flow variable 'google_sheet_id' = EU (et non BE)

    c.  Faire tourner votre module**jusqu'au bout**(les nœuds de calibration et d'écriture dans les fichiers Excel doivent avoir tourné!)

    d.  Vérifier que le module a bien tourné complètement > si pas debugger (avec support Thomas / moi si nécessaire)

    ```{=html}
    <!-- -->
    ```
    e.  **Vérifier que vos résultats sont cohérents!**=> Utilisez les nœuds de visualisation graphiques pour vous faciliter la vie. => Ca permet d'avoir un premier aperçu avant la release et éviter les gros «pics» inhabituels!

    ```{=html}
    <!-- -->
    ```
    f.  Si tout est ok > faire un reset de tous les nœuds + sauver votre module

```{=html}
<!-- -->
```
4.  **Commiter + pusher votre module**(pour que les autres aient accès à vos «nouveaux» outputs



Merci d'avance!😊





**Rappel: explication utilisation du code .bat**



**Note: ce code ne fonctionne que pour les données EU (pas BE)!**

**Note 2: Comme on pull tous les modules, si vous avez modifié un module avant et que vous voulez sauvegarder vos modificationsècommit + push de votre/vos module(s) AVANT de lancer le code!!!**



Concrètement, comment ça fonctionne:



1.  Téléchargez lebatchen pièce jointe

2.  Mettez le dans le folder de chaque module dont vous être le propriétaire

Exemple:

3.  ![](../../../media/Technical-Documentation-Use-bat-script-image1.png){width="2.875in" height="2.6666666666666665in"}

```{=html}
<!-- -->
```
4.  Une fois que vous avez modifié vos données et que vous voulez mettre à jour vos données outputs:

    a.  Assurez vous que Knime est bien fermé (ou en tout cas, qu'aucun module n'est ouvert dedans)

    b.  Lancer le cadebatch. Pour ça, rien de plus simple:

i.Ouvrir une ligne de command: Aller dans le menu: Ligne de commandes / command line (selon la langue de votre ordi😉)

!['Osultat Irvvite "e cornmandes ](../../../media/Technical-Documentation-Use-bat-script-image2.png){width="3.6041666666666665in" height="1.40625in"}

ii.Faire un drag and drop de votre codebatchdans cette ligne de commandèça va écrire un truc du style dans la ligne de commande





![partage CePC 2e2e Microsoft Corporation. droits XCaI c t_UPDATE. bat DRAG and DROP Out" d'appliution XCa1.c dev CHANGE L ](../../../media/Technical-Documentation-Use-bat-script-image3.png){width="5.0in" height="2.7291666666666665in"}

iii.Faire enterèça écrit plein de jolies choses (vous êtes presqu'un vrai geek mntnt😉)



iv.Quand c'est fini, fermer la ligne de commande et**relancer les étapes i à iii, vous devez arriver à un résultat du style**



Fin de code: indique qu'il a bien copié les fichiers suivants

![Copie des suivants dans transport/data/Eu C  ELIAIFs_tra. x1sx 1 fi hie (5) c r copiéC5)  XCalcdev c er s copié(s)• ](../../../media/Technical-Documentation-Use-bat-script-image4.png){width="4.65625in" height="1.1458333333333333in"}



P**our chaque module dans votre folder dev, vous devez avoir une ligne qui dit «Already up to date». Si pas le casèfaites moi signe!**

![up to date. up to date. ](../../../media/Technical-Documentation-Use-bat-script-image5.png){width="2.4166666666666665in" height="1.71875in"}



c.  Vous pouvez ouvrir Knime**, lancer votre flux jusqu'au bout**èon veut que les données et la calibration soient à jour!

```{=html}
<!-- -->
```
d.  **Vous vérifiez que les données en sortie + la calibration donnent de bons résultats -- si c'est le cas, vous faite un reset de tous vos nœuds + save + pushez votre module (commit = «mise à jour des données EU -- calibration = ok»)**





