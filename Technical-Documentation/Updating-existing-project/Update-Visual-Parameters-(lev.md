<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Update Visual Parameters (lever definition, graph detail, …)](#update-visual-parameters-lever-definition-graph-detail-)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Update Visual Parameters (lever definition, graph detail, …)

Created: 2020-04-15 08:42:19 +0200

Modified: 2020-04-27 10:18:24 +0200

---


**This section is only for project that already exist and without other modifications than listed above. If you want to create a new project, change the way workflows are build, and so on, cfr** [Update Model](onenote:#Update%20Model&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5C26C6B6-E6F3-4B3C-9ECD-36C5B72217A5}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one).



| Bitbucket and local desktop |
|-----------------------------|



Nothing special here.



| Google Sheets : data collection |
|---------------------------------|



Nothing special here.



| Knime : isolated module and _common |
|--------------------------------------|



Nothing special here.



| Google Sheet : interface and scenario builder |
|-----------------------------------------------|



Interface

Change the interface :

On the graph page ([Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) - section "Graphs")
-   If you want to add /delete some graphs
-   If you want to change the way graph are displayed (position of them, title, ...)
-   ...

On the lever page ([Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) - section "Levers")
-   If you want to change the definition of a lever
-   If you want to change the category in which the lever is set
-   ...



Once your modifications are made :



![1 download interlace google sheet into configuration/XX folder using excel format 2 rename the file: intenace_xlsx XX is the name of the project (BE, EL', ELIA VLAIO) ](../../media/Technical-Documentation-Update-Visual-Parameters-(lever-definition,-graph-detail,-…)-image1.png){width="3.71875in" height="1.53125in"}





Scenario builder

Nothing special here, except if you want to change the way scenario are build for this given data collection. In this case, cfr [Scenario Builder](onenote:#Scenario%20Builder&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E0C26574-D2CA-4B89-B165-ADED2D317096}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more information about it.





| Knime : _interactions |
|------------------------|



Nothing special here.



**Note** : usually you should download this file (interface) and put it at the place of the previous corresponding file on the remote. But, a new way was crated (cfr red frame below) ; with docker (container) you can directly access this google sheet.

In the yml file (defines all variables needed for the docker ; how to connect to the DB, which scenario should be run, ...), there is a variable named "**update_at_start**" ; value can be true or false. **If true, the docker will download the last version of the interface.**

Pay attention you set the proper google sheet id !



![new way to update the google sheet was implemented. itthe nag is true in the yml file. just stop and remove the container in the EC2 instance and run docker-compose_ It will download the last version of the interface file automatically ](../../media/Technical-Documentation-Update-Visual-Parameters-(lever-definition,-graph-detail,-…)-image2.png){width="6.5in" height="0.875in"}







| Deployment : web application and PowerBI |
|------------------------------------------|



![Update server update knime2python folder update api db docker (pon 5001) docker---czmpzse up ---d Lags -build-arg ssy KEY= ---'docker Isa) " update api model docker (port 5000) ubdate the versionvkxwiththe correct version and the docker-compose yml file accordingly ---C cL±macC/ZecaLc: ---C climact/bacalc: docker build docker---czmpzse up ---d Lags reset mysql - tind the mysql_id using mysgl docker---compzse up ---d --Zu±Ld-arg MCDEL SSE EEY="; (cat " mode L command => to do only when data or model changes (no need to reset mysql if it is only 'cosmetical' changes) ](../../media/Technical-Documentation-Update-Visual-Parameters-(lever-definition,-graph-detail,-…)-image3.png){width="12.625in" height="4.208333333333333in"}









