<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Endpoints documentation](#endpoints-documentation)
- [List of the existing endpoints](#list-of-the-existing-endpoints)
- [Version (GET)](#version-get)
  - [Output](#output)
- [Countries (GET)](#countries-get)
  - [Output](#output-1)
- [Model Graphs Details (GET)](#model-graphs-details-get)
  - [Output](#output-2)
- [Observatory Graphs Details (GET)](#observatory-graphs-details-get)
  - [Output](#output-3)
- [Official Pathways List](#official-pathways-list)
  - [GET method](#get-method)
    - [Output](#output-4)
  - [POST method](#post-method)
    - [Input](#input)
    - [Output](#output-5)
- [Pathways List](#pathways-list)
  - [GET method](#get-method-1)
    - [Output](#output-6)
    - [Output (old version)](#output-old-version)
  - [POST method](#post-method-1)
    - [Input](#input-1)
    - [Output](#output-7)
- [Levers](#levers)
  - [GET method](#get-method-2)
    - [Output](#output-8)
  - [POST method](#post-method-2)
    - [Input](#input-2)
    - [Output](#output-9)
- [Levers Details](#levers-details)
  - [GET method](#get-method-3)
    - [Output](#output-10)
  - [POST method](#post-method-3)
    - [Input](#input-3)
    - [Output](#output-11)
- [Metrics](#metrics)
  - [GET method](#get-method-4)
    - [Output](#output-12)
  - [POST method](#post-method-4)
    - [Input](#input-4)
    - [Output](#output-13)
- [Official Results](#official-results)
  - [GET method](#get-method-5)
    - [Output](#output-14)
  - [POST method](#post-method-5)
    - [Input](#input-5)
    - [Output](#output-15)
- [Results (POST)](#results-post)
  - [Input](#input-6)
  - [Output](#output-16)
- [Displayed Names (GET)](#displayed-names-get)
  - [Output](#output-17)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Endpoints documentation

Created: 2021-07-16 11:15:27 +0200

Modified: 2021-09-03 11:41:02 +0200

---


# List of the existing endpoints


-   GET /api/v1.0/version
-   GET /api/v1.0/countries
-   GET /api/v1.0/model_graphs_details
-   GET /api/v1.0/observatory_graphs_details
-   GET /api/v1.0/official_pathways_list
-   POST /api/v1.0/official_pathways_list
-   GET /api/v1.0/pathways_list
-   POST /api/v1.0/pathways_list
-   GET /api/v1.0/levers
-   POST /api/v1.0/levers
-   GET /api/v1.0/lever_details
-   POST /api/v1.0/lever_details
-   GET /api/v1.0/metrics
-   POST /api/v1.0/metrics with metric and country
-   POST /api/v1.0/metrics with metric
-   POST /api/v1.0/metrics with country
-   GET /api/v1.0/official_results
-   POST /api/v1.0/official_results with scenario and sector
-   POST /api/v1.0/official_results with scenario
-   POST /api/v1.0/results
-   GET /api/v1.0/displayed_names



# Version (GET)

GET /api/v1.0/version

Provide the version of the last update in _interactions



## Output

[

{"Year": "", "Major":"" , "Minor":"" , "Docker build":"", "Date":"" }

]



# Countries (GET)

GET /api/v1.0/countries

Provide the list of countries and country codes



## Output

{

COUNTRY: {

ISO-3166-2: CODE,

Enabled in Webtool: BOOL,

Description: DESCRIPTION

},

...

}



# Model Graphs Details (GET)

GET /api/v1.0/model_graphs_details

Provide information about the graphs in model mode.



## Output

{

MENU: {

SUB MENU: [

{

"Position": "",

"Name": "",

"Description": "",

"Unit": "",

"graphType": "",

"graphStacked": "",

"metrics": [],

}

],

...

}



# Observatory Graphs Details (GET)

GET /api/v1.0/observatory_graphs_details

Provide information about the graphs in observatory mode.



## Output

{

MENU: {

SUB MENU: [

{

"Position": "",

"Name": "",

"Description": "",

"Unit": "",

"graphType": "",

"graphStacked": "",

"metrics": []

"sectors": [],

"vectors": []



}

],

...

}





# Official Pathways List

GET /api/v1.0/official_pathways_list

Provide the list of official scenarii called official pathways.



## GET method

GET /api/v1.0/official_pathways_list

Give all the available data



### Output

{

COUNTRY:[

PATHWAY:{

"sector_or_vector": SECTOR/VECTOR,

"Definition": DEFINITION

},

...

],

...

}



## POST method

POST /api/v1.0/pathways_list

Give data about specific country



### Input

{

"country": ""

}



### Output

[

PATHWAY:{

"sector_or_vector": SECTOR/VECTOR,

"Definition": DEFINITION

},

...

]





# Pathways List

Provide the list of scenarii called pathways.



## GET method

GET /api/v1.0/pathways_list

Give all the available data



### Output

{

COUNTRY:

{

PATHWAY 1 : {

Levers: [...],

Definition: ...

},

...

},

...

}



### Output (old version)

{

PATHWAY 1 : [

...

],

...

}



## POST method

POST /api/v1.0/pathways_list

Give data about specific country



### Input

{

"country": ""

}



### Output

{

SCENARIO 1 : {

Levers: [...],

Definition: ...

},

...

}





# Levers

Provide basic information on the levers.



## GET method

GET /api/v1.0/levers

Give all data.



### Output

[

{"lever_name": "",

"title": "",

"group_1": "",

"group_2": "",

"headline": ""

}

...

]





## POST method

POST /api/v1.0/levers

Give data for a specific lever.



### Input

{

"lever": ""

}



### Output

[

{"lever_name": "",

"title": "",

"group_1": "",

"group_2": "",

"headline": ""

}

...

]





# Levers Details

Provide detailed information on the levers.



## GET method

GET /api/v1.0/lever_details

Give data for all countries.



### Output

[

{

"headline": "",

"group_1": "",

"group_2": "",

"title": "",

"module": "",

"lever_name": "",

"display": "",

"display_range": "",

"lever definition": "",

"Sources": "",

"Assumptions": "",

"Country": "",

"level 1": "",

"level 2": "",

"level 3": "",

"level 4": "",

"figure_caption": "",

"unit": "",

"metrics": "",

"L1": [],

"L2": [],

"L3": [],

"L4": [],

"Default_metrics": ""

},

]





## POST method

POST /api/v1.0/lever_details

Give data for a specific country.



### Input

{

"country": ""

}



### Output

[

{

"headline": "",

"group_1": "",

"group_2": "",

"title": "",

"module": "",

"lever_name": "",

"display": "",

"display_range": "",

"lever definition": "",

"Sources": "",

"Assumptions": "",

"Country": "",

"level 1": "",

"level 2": "",

"level 3": "",

"level 4": "",

"figure_caption": "",

"unit": "",

"metrics": "",

"L1": [],

"L2": [],

"L3": [],

"L4": [],

"Default_metrics": ""

},

]



# Metrics

Provide specific data on the level of variables.



## GET method

GET /api/v1.0/metrics

Give data for all countries and metrics.



### Output

[

{

"lever_name": LEVER,

"Country": COUNTRY,

"metrics": METRIC,

"figure_caption": CAPTION,

"unit": UNIT,

"L1": [...],

"L2": [...],

"L3": [...],

"L4": [...]

},

...

]





## POST method

POST /api/v1.0/metrics with metric and country

POST /api/v1.0/metrics with metric

POST /api/v1.0/metrics with country



Give data for a specific country and/or metric.



### Input

{

"metric":"",

"country":""

}



### Output

[

{

"lever_name": LEVER,

"Country": COUNTRY,

"metrics": METRIC,

"figure_caption": CAPTION,

"unit": UNIT,

"L1": [...],

"L2": [...],

"L3": [...],

"L4": [...]

},

...

]





# Official Results

Provide official data used by observatory.



## GET method

GET /api/v1.0/official_results

Give data for all scenarios and sectors.



### Output

{

SCENARIO (DIMENSION):[

{

"sector_or_vector": sector

"name": FIELD NAME

"timeAxis": [

...

],

"data": {

COUNTRY CODE: [

...

],

...

},

},

...

],

SCENARIO (DIMENSION):{

SECTOR:[

{

"sector_or_vector": vector

"name": FIELD NAME

"timeAxis": [

...

],

"data": {

COUNTRY CODE: [

...

],

...

},

},

...

],

...

}

}





## POST method

POST /api/v1.0/official_results with scenario and sector

POST /api/v1.0/official_results with scenario



Give data for a specific scenario and/or sector.



### Input

[{

"scenario": SCENARIO (DIMENSION),

"sector": SECTOR (if apply)

}]



### Output

{

SCENARIO (DIMENSION):[

{

"sector_or_vector": sector

"name": FIELD NAME

"timeAxis": [

...

],

"data": {

COUNTRY CODE: [

...

],

...

},

},

...

],

SCENARIO (DIMENSION):{

SECTOR:[

{

"sector_or_vector": vector

"name": FIELD NAME

"timeAxis": [

...

],

"data": {

COUNTRY CODE: [

...

],

...

},

},

...

],

...

}

}





# Results (POST)

POST /api/v1.0/results

Provide results using a backend model provided at initialization time.



## Input

{

"levers": {

"default": [1,1,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],

"exceptions": {

"DE": [4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]

}

},

"outputs": [

{

"id": "bld_residential-energy-demand_oil[TWh]"

},

{

"id": "ref_demand_fuel-oil_total[TWh]",

"allCountries": true

},

]

}



## Output

{

"outputs": [

{

"id": VARIABLE,

"title": DISPLAYED NAME,

"sector": SECTOR NAME,

"vector": VECTOR NAME,

"timeAxis": [

...

],

"data": {

COUNTRY: [

...

],

...

}

},

...

],

"warnings": [],

"kpi": [

{

"id": VARIABLE,

"title": DISPLAYED NAME,

"sector": VECTOR NAME,

"vector": SECTOR NAME,

"data": {

COUNTRY: ...,

...

}

},

...

],

"sankey": []

}





# Displayed Names (GET)

GET /api/v1.0/displayed_names

Provide the list of output variables available from the model or as calculated variables.



## Output

{

VARIABLE: {

"display_name": DISPLAYED NAME,

"sector_mapping": SECTOR NAME,

"vector_mapping": VECTOR NAME

},

...

}

![](../../../media/Technical-Documentation-Endpoints-documentation-image1.png){width="0.11458333333333333in" height="0.11458333333333333in"}

