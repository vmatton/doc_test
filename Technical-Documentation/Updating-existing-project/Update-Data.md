<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Update Data](#update-data)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Update Data

Created: 2020-04-15 08:39:22 +0200

Modified: 2021-02-15 11:56:02 +0100

---


**Here, change is only made on value and not on the variable names !**

**If name changes or other data are added and that implies change in the Knime workflow, cfr to the section** [Update Model](onenote:#Update%20Model&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={5C26C6B6-E6F3-4B3C-9ECD-36C5B72217A5}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one).



| Bitbucket and local desktop |
|-----------------------------|



As this section is about updating data only, nothing special here. If you start a new project or module, cfr the appropriate section.



| Google Sheets : data collection |
|---------------------------------|



Open the "Data collection" Google page linked to your project. If you are creating a new project, refer to for more information about it : *User Manual > Google Sheets > Data Collection*.



| Knime : isolated module and _common |
|--------------------------------------|



Nothing to do here.

Just check is there any problem with the calibration (range below 50% or above 150%, ...). Cfr *User Manual > Google Sheets > Data Calibration* for more details.





| Google Sheet : interface and scenario builder |
|-----------------------------------------------|

Interface

Nothing special here.



Scenario builder

Nothing special here, except if you want to change the way scenario are built for this given data collection. In this case, cfr [Scenario Builder](onenote:#Scenario%20Builder&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E0C26574-D2CA-4B89-B165-ADED2D317096}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more information about it.





| Knime : _interactions |
|------------------------|



Data-preprocessing

![1. open data-preprocessing in Knime 2 run the workflow witn the related to the dataset that was updated 3 new output are saved in the 'data' folder only run the metanodes related to • the updated data ](../../media/Technical-Documentation-Update-Data-image1.png){width="3.625in" height="1.71875in"}



Data-processing

Nothing to do here.



![Push _interactions changes to bitbucket add --- ---a ZL ---am "message" push tag vX.X) push ------Cags) ](../../media/Technical-Documentation-Update-Data-image2.png){width="3.84375in" height="2.8333333333333335in"}





| Deployment : web application and PowerBI |
|------------------------------------------|





![Update server update knime2python folder update api db docker (pon 5001) docker---czmpzse up ---d Lags -build-arg ssy KEY= ---'docker Isa) " update api model docker (port 5000) ubdate the versionvkxwiththe correct version and the docker-compose yml file accordingly ---C cL±macC/ZecaLc: ---C climact/bacalc: docker build docker---czmpzse up ---d Lags reset mysql - tind the mysql_id using mysgl docker---compzse up ---d --Zu±Ld-arg MCDEL SSE EEY="; (cat " mode L command => to do only when data or model changes (no need to reset mysql if it is only 'cosmetical' changes) ](../../media/Technical-Documentation-Update-Data-image3.png){width="12.625in" height="4.208333333333333in"}







