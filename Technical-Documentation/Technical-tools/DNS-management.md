<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [DNS management](#dns-management)
- [General](#general)
- [Connexion to server](#connexion-to-server)
- [AWS](#aws)
- [](#)
- [](#-1)
- [Gandi](#gandi)
- [](#-2)
- [](#-3)
- [NGINX - Reverse proxy](#nginx---reverse-proxy)
  - [HTTPS set up](#https-set-up)
- [SSL](#ssl)
- [security](#security)
- [logging](#logging)
- [reverse proxy](#reverse-proxy)
- [additional config](#additional-config)
- [HTTP redirect](#http-redirect)
- [logging](#logging-1)
- [Czech Republic specific redirect](#czech-republic-specific-redirect)
- [SSL](#ssl-1)
- [security](#security-1)
- [Bulgaria specific redirect](#bulgaria-specific-redirect)
- [SSL](#ssl-2)
- [security](#security-2)
- [Hungary specific redirect](#hungary-specific-redirect)
- [SSL](#ssl-3)
- [security](#security-3)
- [Romania specific redirect](#romania-specific-redirect)
- [SSL](#ssl-4)
- [security](#security-4)
- [Italia specific redirect](#italia-specific-redirect)
- [SSL](#ssl-5)
- [security](#security-5)
- [Greece specific redirect](#greece-specific-redirect)
- [SSL](#ssl-6)
- [security](#security-6)
- [Croatia specific redirect](#croatia-specific-redirect)
- [SSL](#ssl-7)
- [security](#security-7)
- [Spain specific redirect](#spain-specific-redirect)
- [SSL](#ssl-8)
- [security](#security-8)
- [Poland specific redirect](#poland-specific-redirect)
- [SSL](#ssl-9)
- [security](#security-9)
- [Slovakia specific redirect](#slovakia-specific-redirect)
- [SSL](#ssl-10)
- [security](#security-10)
- [Catch everything else](#catch-everything-else)
- [SSL](#ssl-11)
- [security](#security-11)
- [SSL](#ssl-12)
- [security](#security-12)
- [logging](#logging-2)
- [reverse proxy](#reverse-proxy-1)
- [additional config](#additional-config-1)
- [HTTP redirect](#http-redirect-1)
  - [HTTP set up](#http-set-up)
- [security](#security-13)
- [logging](#logging-3)
- [reverse proxy](#reverse-proxy-2)
- [additional config](#additional-config-2)
  - [Commands](#commands)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# DNS management

Created: 2021-06-09 16:46:46 +0200

Modified: 2021-09-23 13:17:17 +0200

---


# General



Climact is using two providers to host his domains : AWS and Gandi. AWS is used to host BeCalc domain (netzero2050.be) and Gandhi for XCalc one (climact.com).



An internet user will receive through DNS the reverse proxy IP. The user will access the proxy through standard ports (80 for HTTP or 443 for HTTPS). This reverse proxy will then forward data from the webtool server on a specific port (3000 by default).





# Connexion to server



To get access to the NGINX server you have to connect in SSH to it. The user is "ubuntu". You have to use the ssh key and this user to connect to the server. The server is protected by IP restriction in AWS. Please add your [public]{.ul} IP before trying to connect (EC2 > Security Groups > nginx).





![aWS Se EC2 EQ Global I nstances Typø Spot Req ngs pin Rmd ted Hosts Ca pacify Key Load fo r Cts, sg-074ae8c7d491017be - nginx rules 10 Tags @ You can ch«k network with Re*hability Inbound rules (8) 629" sr0567d7e7163etgaa9 (Opt ] O emp - NGINX nAø 1 Pemüssion Port Run Reaeiubiuey Manage Edit inbound ](../../media/Technical-Documentation-DNS-management-image1.png){width="6.510416666666667in" height="3.1770833333333335in"}



# AWS



In AWS, **Route53** is the tool to configure DNS. One sub domain is configured : becalc.netzero2050.be. This subdomain is refeering to the reverse proxy.



![Rane 53 > > netzer02050.be netzer02050.be Hosted zone details Configure query logging Edit hosted zone Create record Record details Record type Value 0 18.157.74.26 policy Edit record DNSSEC Hosted (O) Records (1/3) Q Filter records by proWty value Record name r02050.be becalc.netzer02050.be Import zone ns- 1 S 20 .org_ 2030 net. awsdns-hostmaster.amazon.corn. 1 noo %.012096m 18157.7426 ](../../media/Technical-Documentation-DNS-management-image2.png){width="6.541666666666667in" height="3.09375in"}

# 

# 

# Gandi



On the Gandi website, go to : Domain > Active > climact.com. In the tab "DNS records", one sub domain is configured : pathwaysexplorer.climact.com. This subdomain is refeering to the reverse proxy.



![0 33SSNO 59 」 00m 一 0 5 003d SNO 」 一P0」q黌$ 9- S -99 94 45L3L 19 • 【 ZCL6 6up-•e.NOOJqOM 3NVNJ 3NVN3 3RVN3 '0101dxosKem-aed•6q 」 0- d , d•nq 」 o.J01dxosKem•oed KOMu!euop¯Vf 」 03 凹 POIne 2P0 ageL du S 山 1 0- 」 一 1S5 且 一 SNOu 7- z40 0 3 03 」 ♂ SdA ](../../media/Technical-Documentation-DNS-management-image3.png){width="6.520833333333333in" height="3.25in"}

# 

# 

# NGINX - Reverse proxy



NGINX server has been installed by Vox Teneo.



To see the configuration of the reverse proxy :

/etc/nginx/sites-available





## HTTPS set up

XCalc has been configured in HTTPS and has a HTTP redirect in the 80 port. There are country specific redirect.



===============================================================================

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name pathwaysexplorer.climact.com wwww.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



# logging

access_log /var/log/nginx/pathwaysexplorer.climact.com.access.log;

error_log /var/log/nginx/pathwaysexplorer.climact.com.error.log warn;



# reverse proxy

location / {

proxy_pass <http://172.31.35.79:3000>;

include nginxconfig.io/proxy.conf;

}



location /api {

proxy_pass <http://172.31.35.79:5001>;

include nginxconfig.io/proxy.conf;

}



location /model {

proxy_pass <http://172.31.35.79:5000>;

include nginxconfig.io/proxy.conf;

}



# additional config

include nginxconfig.io/general.conf;

}



# HTTP redirect

server {

listen 80;

listen [::]:80;

server_name pathwaysexplorer.climact.com;

include nginxconfig.io/letsencrypt.conf;



# logging

access_log /var/log/nginx/http.pathwaysexplorer.climact.com.access.log;

error_log /var/log/nginx/http.pathwaysexplorer.climact.com.error.log warn;



location / {

return 301 <https://pathwaysexplorer.climact.com$request_uri>;

}

}



# Czech Republic specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name cz.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=CZ>;

}



# Bulgaria specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name bg.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=BG>;

}



# Hungary specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name hu.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=HU>;

}



# Romania specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name ro.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=RO>;

}



# Italia specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name it.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=IT>;

}



# Greece specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name el.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=GR>;

}



# Croatia specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name hr.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=HR>;

}



# Spain specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name es.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=es>;

}



# Poland specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name pl.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=pl>;

}



# Slovakia specific redirect

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name sk.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com/?country=sk>;

}



# Catch everything else

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name *.pathwaysexplorer.climact.com;



# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.climact.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.climact.com/chain.pem;



# security

include nginxconfig.io/security.conf;



return 301 <https://pathwaysexplorer.climact.com>;

}



===============================================================================









BeCalc has been configured in HTTPS and has a HTTP redirect in the 80 port.



Here after a typical exemple of HTTP configuration for Xcalc :
-   Configure the port and url to listen to
-   Configure proxy pass (link between IP:Port and a URL)
-   Set up credentials



===============================================================================

server {

listen 443 ssl http2;

listen [::]:443 ssl http2;

server_name becalc.netzero2050.be;



# SSL

ssl_certificate /etc/letsencrypt/live/becalc.netzero2050.be/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/becalc.netzero2050.be/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/becalc.netzero2050.be/chain.pem;



# security

include nginxconfig.io/security.conf;



# logging

access_log /var/log/nginx/becalc.netzero2050.be.access.log;

error_log /var/log/nginx/becalc.netzero2050.be.error.log warn;



# reverse proxy

location / {

proxy_pass <http://172.31.13.150:3000>;

include nginxconfig.io/proxy.conf;

}



location /api {

proxy_pass <http://172.31.13.150:5001>;

include nginxconfig.io/proxy.conf;

}



location /model {

proxy_pass <http://172.31.13.150:5000>;

include nginxconfig.io/proxy.conf;

}



# additional config

include nginxconfig.io/general.conf;

}



# HTTP redirect

server {

listen 80;

listen [::]:80;

server_name becalc.netzero2050.be;

include nginxconfig.io/letsencrypt.conf;



location / {

return 301 <https://becalc.netzero2050.be$request_uri>;

}

}

===============================================================================







## HTTP set up

[HTTPS SHOULD BE USED]

Here after a typical exemple of HTTP configuration for Xcalc :
-   Configure the port and url to listen to
-   Configure proxy pass (link between IP:Port and a URL)



===============================================================================

server {

listen 80;

listen [::]:80;

server_name pathwaysexplorer.climact.com;



# security

include nginxconfig.io/security.conf;



# logging

access_log /var/log/nginx/pathwaysexplorer.climact.com.access.log;

error_log /var/log/nginx/pathwaysexplorer.climact.com.error.log warn;



# reverse proxy

location / {

proxy_pass <http://172.31.35.79:3000>;

include nginxconfig.io/proxy.conf;

}



location /api {

proxy_pass <http://172.31.35.79:5001>;

include nginxconfig.io/proxy.conf;

}



location /model {

proxy_pass <http://172.31.35.79:5000>;

include nginxconfig.io/proxy.conf;

}



# additional config

include nginxconfig.io/general.conf;

}

===============================================================================





## Commands



To add a configuration file, you should :

1.  Add il to

    -   /etc/nginx/sites-available

    -   Filename : domain managed (e.g.: pathwaysexplorer.climact.com) + ".conf"

2.  Create symbolic link in

    -   cd /etc/nginx/sites-enabled

    -   sudo ln -s ../sites-available/[DOMAIN].conf [DOMAIN].conf

    -   (e.g.: sudo ln -s ../sites-available/pathwaysexplorer.climact.com.conf pathwaysexplorer.climact.com.conf)

3.  Reload service with :

    -   sudo service nginx reload



See NGINX status :

service nginx status







