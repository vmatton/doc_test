<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Start (or restart) a screen / webtool](#start-or-restart-a-screen--webtool)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Start (or restart) a screen / webtool

Created: 2021-02-03 10:02:45 +0100

Modified: 2021-02-03 10:03:32 +0100

---


![Texte de remplacement généré par une machine: Website Pour voir les ports allumés et les processus liés zuci2 ---ZuLpn LISTEN Redémarrer le port du webserver_ Afin de pouvoir quitter la session SSH sans terminer le process exposant le port du web serveur, il faut recréer un screen : 1. ---s (va créer un screen "run") 3. -E -L 3000 permet de déployer le build 4. CTRL + A suivi de D (detached head mode) Si on veut démarrer en mode développementftest Liste de commandes utiles Pour se reconnecter à un screen : ---1 run lance le screen "run") Pour killer un screen : CTRL + A suivi de K (kill) Pour lister les screens en cours : -LE Pour voir les process liés aux screen : -3: ](../../../media/Technical-Documentation-Start-(or-restart)-a-screen---webtool-image1.png){width="8.760416666666666in" height="5.40625in"}

