<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Dockers](#dockers)
- [Docker-compose.yml](#docker-composeyml)
- [Dockerfile_api_db](#dockerfile_api_db)
  - [First part](#first-part)
  - [Second part](#second-part)
- [Dockerfile_api_model](#dockerfile_api_model)
  - [](#)
  - [First part](#first-part-1)
  - [](#-1)
  - [Second part](#second-part-1)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Dockers

Created: 2020-04-15 09:10:25 +0200

Modified: 2021-03-12 11:53:18 +0100

---




Docker is used when we do not want to reinstall all required software while creating a new server. It also allows us to use different version of a software in a server. While creating a docker, we determine what we need and docker installs everything for us.



Docker is like a boat with multiple containers on it. Each container has its own "life" (doing things on its side). We can connect them together thanks to a configuration file that set up the links between them. Each time you want to update your web application, you have to rebuild the image (because their content changes as model or data have been changed) and then construct a container around them. These actions are done while using docker build and docker-compose command lines (cfr [MobaXTerm](onenote:#MobaXTerm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={D305507A-BF69-46A1-AB14-BDD34B4CF290}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more details).



In out process, we have :
-   3 containers :

    -   **Dockerfile_api_db** : set on port 5001. It is connected to the db. When a request is done on the website, it looks into the db to see if the scenario asked already exists. If it is the case, it returns the values for the website. If not, it will send the request to the model (cfr dockerfile_api_model)

    -   **Dockerfile_api_model** : set on port 5000. It contains the model ; each time a user ask for a scenario that is not in the DB, the model is run here to furnish new output values.

    -   **Db (called also mysql)** : it is constructed in the configuration file. There is no docker file for it. For more information : [Configuration Files](onenote:#Configuration%20Files&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={74BD6C1F-8EF9-4961-B3AC-B17C62138312}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) (database section).
-   1 docker-compose file : **docker-compose.ylm**



These files are described in the following sections, except for db/mysql which is described in the Configuration File section as mentioned above.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="docker-compose.yml">Docker-compose.yml</h1></th></tr></thead><tbody></tbody></table>



This file precises what are the connexion between each containers. Here, the api_model and the api_db depends on the db (cfr yellow frame) which is set on the third red frame.

In blue frame : the port of each container is precised.



Each container is linked to an image (cfr "**image**:"). Usually, we use the latest version of an image that exist. But, for some reason, we could want to create a new one. For this purpose : change the name of the version while using command lines (cfr [MobaXTerm](onenote:#MobaXTerm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={D305507A-BF69-46A1-AB14-BDD34B4CF290}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)) and here in **image** value. Just make sure you use a new image name ; to know which name already exist cfr MobaXTerm page.



In green : we initialize the environment for mysql. We use the same initialization and environment that are described in the configuration file (ex. Conf_docker_be.yml) ; cfr Configuration Files page for more details.



![REAOME,md docker-compose,yml WOEL VERSION: master " søøe:saøa• a pi_db : image: AP i to the Nortd (needs to natch API config) cc r, text: . # Expse API to wrLd (needs image: restart: always "3306:33az" to API . /db/init : /docker-entrypoint-initdb. d/ : rc MYSQL RCOT PASS'ÆORD: 7vupwgcxBQvz Needs to match API config MYSQL_OATASAS?: eucalc Needs to narch API confi9 ](../../../media/Technical-Documentation-Dockers-image1.png){width="7.552083333333333in" height="9.6875in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="dockerfile_api_db">Dockerfile_api_db</h1></th></tr></thead><tbody></tbody></table>



Docker_api_db and docker_api_model are quite similar.



## First part

First part of their code is completely identical ;

![python 3.6- ARG '.ODEL ARG ssH KEY * directory in the BORKDIR /eucalc contai * Install python dependencies RIN install - -upgrade setuptOOIS COPY . RUN pip install -r requirements. txt * Copy all source files for production COPY ./scripts . 'scripts copy ./src ./src COPY . 'config . 'config Get model sources Install git RUN apt-get update apt-get install -y Bit Authorize SSH Host RUN mkdir -p 'root/ . ssh  chmod /root/. ssh 88 55h-ey>sgo > /nx•t/ . ssh/known_hosts Copies key RUN echo > /root/.ssh/id_rsa RUN chmod /root/.ssh/id_rsa Get code RUN mkdir interactions RUN 55h-agent bash -c •ssh-add / root/.ssh/id_rsa; Remove SSH key RUN -rf / git clone --branch $00DEL_VERS10N} --depth I • ](../../../media/Technical-Documentation-Dockers-image2.png){width="8.760416666666666in" height="7.177083333333333in"}



First (blue boxes), we initialize two arguments named MODEL_VERSION and SSH_KEY. Their values are both specified in command lines while updating the web application (cfr [MobaXTerm](onenote:#MobaXTerm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={D305507A-BF69-46A1-AB14-BDD34B4CF290}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



![ARG VERSION-master ARG ssH KEY ](../../../media/Technical-Documentation-Dockers-image3.png){width="2.1458333333333335in" height="0.65625in"}



Then (yellow boxes 1 to 3), we indicate the directory of the containers and we install required software as well as needed input folders (eg. Scripts).



Finally (fourth yellow box), we clone the last version of _interactions. To do so, we need to :

1.  Install GIT

2.  Set the values for the SSH key to access bitbucket (key is set in the .ssh folder, file name is known_hosts)

![/homeJec2-user/ssh/ Name au thorized _ke vs dim act_knime2python config known hosts Size (KB) bitbuck... Last modified 2019-09-13... 2019-02-27 2019-02-27 2019-02-27 ](../../../media/Technical-Documentation-Dockers-image4.png){width="3.9583333333333335in" height="1.5833333333333333in"}



3.  Copy the key to a folder named id_rsa (into the container). Here, the SSH_KEY argument is used.

4.  Change the directory to _interactions and use git bash to clone this module. Here is used the MODEL_VERSION argument to define which branch we want to clone.

![Rtm „kdir Rtm ssh-agent bash -c 'ssh-•dd /root/.ssh/id_rsa; git clone --branch -depth 1' ](../../../media/Technical-Documentation-Dockers-image5.png){width="8.53125in" height="0.7291666666666666in"}



5.  Remove the SSH key when _interactions has been cloned.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p>How to use a SSH key in a dockerFile ?</p><p>Code should look like :</p><p><img src="../../../media/Technical-Documentation-Dockers-image6.png" style="width:4.70833in;height:1.57292in" alt="ARG finished ssH PRIVATE KEY mkdir s [ 00 Where is the SSH key u need with remote Oel.te SSH " /></p><p></p><p><img src="../../../media/Technical-Documentation-Dockers-image7.png" style="width:4.89583in;height:1.78125in" alt="The SSH PRIVATE KEY is passed when issuing the build command with build-arg or in the build block of your docker-compose.yml file. That ARG variable is not used in the final image, the value will not be available using the history command. Using multi-stage builds also has the great side effect of significantly reducing the size of your final Docker images, as they don&#39;t need to contain traces of Git and other build tools if used correctly. " /></p><p></p></th></tr></thead><tbody></tbody></table>





## Second part

Second part of the code slightly differs between both docker files (model and db). What the code does is :

1.  Indicate the environment for python

2.  Indicate the port where the container will be set ; **for docker_api_db : 5001**

3.  Use an entrypoint to run specific executable in the container :

    a.  Define the "executable" : Gunicorn (Python Web Server Gateway Interface)

    b.  API is build by using the application config_docker_be.ylm ; **bypass_model = True**

    c.  Gives Gunicorn setting : timeout = 500 (cfr "---timeout", "500"). Workers silent for more than the defined time (in second) are killed and restarted (here set to 500 s)

    d.  Link the container to a port using the Gunicorn setting called bind (cfr "bind","0.0.0.0:5001) ; **for docker_api_db, port is 5001**.

```{=html}
<!-- -->
```
1.  Define the number of worker with the Gunicorn setting "workers" (here set to 1).

2.  Run the app python code (here : eucalc-app.py) in Python environment and specify what is the configuration file (setting "-c") to use (here : confi_docker.yml)

For more details about Gunicorn command settings : <https://docs.gunicorn.org/en/stable/settings.html>



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>Note</strong> :</p><p>bypass-model = True ; this argument is used in the converter code. It indicates that we do not need to run the model (model is run only in api_model)</p></th></tr></thead><tbody></tbody></table>





![Set variable for python code to find sc»urces API interface to match Eith API config EXPOSE seal Run the API z for Green Unicorn, sets ti•eout to 5øø to al Iou the model tie to build. These oar•meters are not This sets the default to z To override, run 'docker r testing run meant to be chaneed •t run time. True) I worker, mrks with 46B run -p -w 2' the app with Flask • "---timeout • "e.e.e.e:seel"] CBO ["python-. " c", ](../../../media/Technical-Documentation-Dockers-image8.png){width="8.916666666666666in" height="3.4479166666666665in"}







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="dockerfile_api_model">Dockerfile_api_model</h1></th></tr></thead><tbody></tbody></table>

## 

## First part

Cfr Docker-api-db above.

## 

## Second part

For details about it, cfr Docker-api-db above. Here we just highlight differences with the Docker-api-db file :
-   In blue box (2) : port is 5000 (<> 5001 in api-db)
-   In the entrypoint :

    -   No bypass_model setting

    -   Bind setting is set to 0.0.0.0 :5000 (<> 0.0.0.0 :5000 in api-db)

    -   Backlog setting is set to 0 (cfr « --backlog », « 0 »). This set the maximum number of clients that can be waiting to be served.



![Set environment variable for python code to find sources API interfaæ to ntch with API Run the API for Green unicorn, sets timeout tc 5W to allow the B'del tine to build. These parameters are not rant to be changed at time. •--tieout% "see", •--bind", "---backlog". "e" I This sets the default to I "orker, wrks Kith 46B To override, run docker run -p seee:seee -N 2' For 1-1 testing purposes: run the app with ["python", Flask app py' c- config/config_docker " ] ](../../../media/Technical-Documentation-Dockers-image9.png){width="8.885416666666666in" height="3.15625in"}













