<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Configuration Files](#configuration-files)
- [Database](#database)
- [Model](#model)
- [Log](#log)
- [Api](#api)
  - [Name of the file](#name-of-the-file)
  - [What are the settings ?](#what-are-the-settings-)
  - [What are the differences between configuration files ?](#what-are-the-differences-between-configuration-files-)
  - [What are the settings ?](#what-are-the-settings--1)
  - [What are the differences between configuration files ?](#what-are-the-differences-between-configuration-files--1)
  - [What are the settings ?](#what-are-the-settings--2)
  - [What are the differences between configuration files ?](#what-are-the-differences-between-configuration-files--2)
  - [What are the settings ?](#what-are-the-settings--3)
  - [What are the differences between configuration files ?](#what-are-the-differences-between-configuration-files--3)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Configuration Files

Created: 2020-04-15 09:11:06 +0200

Modified: 2021-03-12 11:59:28 +0100

---




The configuration file is called inside each dockerfile (dockerfile_api_db and dockerfile_api_model - cfr Entrypoint in code). It can be easily modified thanks to the vmi command in MobaXTerm (cfr [How to open a file with command line and change](onenote:#MobaXTerm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={D305507A-BF69-46A1-AB14-BDD34B4CF290}&object-id={441235DF-1F27-0288-0EC0-E1A2C2E99D65}&BC&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



![( "src . api . ](../../../media/Technical-Documentation-Configuration-Files-image1.png){width="7.125in" height="0.6979166666666666in"}



This file gives some additional information about what should be loaded, what to do with the Knime model and so on. This code is divided into four main variables :
-   Database
-   Model
-   Log
-   Api



![database: host: localhost port: 33ø6 user: root password : admin schema : levers output table: output Imodel: caching: true reset db: true False level: INFO e: api.log port: 5øøø country_codes : ](../../../media/Technical-Documentation-Configuration-Files-image2.png){width="2.5in" height="3.875in"}



Each of these "variables" are described in the following sections.





## Name of the file

**For each project, we have two configuration files** :
-   Config_docker_*project_abbr* : this file is used by the docker while building containers. So it is set for the web application.
-   Config_*project_abbr* : this file is used to test the web application in local.



*Project_abbr* is the abbreviation used for the project (eg. Be, eu2050, elia)







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="database">Database</h1></th></tr></thead><tbody></tbody></table>



## What are the settings ?

Here, we define how we can connect to the database. **Should not be changed !**



## What are the differences between configuration files ?

No matters the project, database is the same for all of them. Nevertheless, differences are found between config_docker and config files ;



<table><colgroup><col style="width: 49%" /><col style="width: 50%" /></colgroup><thead><tr class="header"><th>Config_docker_<em>project_abbr</em></th><th>Config_<em>project_abbr</em></th></tr></thead><tbody><tr class="odd"><td><p><img src="../../../media/Technical-Documentation-Configuration-Files-image3.png" style="width:2.21875in;height:1.77083in" alt="Fldatabase: host: db port: user: password : 7VupwgCxaQVZ schema: eucalc output table: output baseyear: 2ß15 " /></p><p></p><p>Here, we work on remote (cr "db"). The password that is used should be the same that is set in docker-compose file (cfr "volumes")</p></td><td><p><img src="../../../media/Technical-Documentation-Configuration-Files-image4.png" style="width:2.0625in;height:1.75in" alt="- database: host: localhost port: user: password: admin schema: levers output table: output baseyear: 2ø15 " /></p><p></p><p>Here, we work on local and password is "admin"</p></td></tr></tbody></table>





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="model">Model</h1></th></tr></thead><tbody></tbody></table>



## What are the settings ?

Defines how the model is used, ... Here are the different settings :


-   **Db_caching** : set to True to set mysql in cache (we use False when we work on our computer and do not want to install mysql on it)
-   **Reset_db** : reset the db each time the server is restarted. On local, we set to true as we want to use the latest version, but for the web application, we set False as we do not want to loose all the previous requests that have been made and saved into the db.
-   **Cache_after_cube** : set to false. We use true only for PowerBI as the scenario are pre-defined and there is no need to run the model again.
-   **Interpolation_start** and **interpolation_end** : allow us to make an interpolation between years to get the results for each year (eg. ELIA project). Take care ; this is an interpolation and not the real value for each year.
-   **Workspace** : where is our workspace (on local (cfr c://) or for the web application (eucalc)
-   **Knime_preferences_file** and **init_workflow** : where Knime is and where are the initial database
-   **Google_sheet_prefix** : depends on the project (cfr workflow variable in Knime)



![db_caching: true reset db: false cache after cube: false # Powerbi . interpolation_start: 2ø15 interpolation_end: 2ø15 workspace: /eucalc true - too : false /knime/knime_preferences . epf init_workflow: BE #foLder of data to be use the model ](../../../media/Technical-Documentation-Configuration-Files-image5.png){width="5.0in" height="1.53125in"}


-   **Dev** : set were to find the levers_file (cfr lever_position.json), the Knime model (cfr data-processing) and what is the scenario that appears when we open the web application

![levers_file: . json model workflow: /eucalc/dev/ scenario for initialisation: INIT: [1.1, 2, 3, 4, 1, 1, interactions / workflows / data # Scenarios for development purpose (and for Powerbi use = > onLy inititisation) ](../../../media/Technical-Documentation-Configuration-Files-image6.png){width="7.1875in" height="0.71875in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>WHAT TO DO ?</strong></p><p><strong>Should be done in each project configuration file !</strong></p><p></p><p><strong>If new lever</strong> : you should add the appropriate value here.</p><p>Usually, we use value = 1 for each lever, but just make sure the number of "1" is the same than the number of lever in the model !</p><p></p><p>&gt;&gt; The script "configure_config_docker.py" is designed to do it for you (see <a href="onenote:#Release%20Support&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Release Support</a>)</p><p></p></th></tr></thead><tbody></tbody></table>




-   **Prod** : defines where the Knime model is (should be the same as it is set in "dev"), which modules nodes should be run, what are the scenario that should at least appears on the website (eg. REF, Core - 80%) and if we have to run these scenario.

As the run of each predefined scenario takes a lot of time, if we are testing our model, no need to run all of them (**enable_webtool_scenarios is set to false**), for the web application, we need these scenario (**enable_webtool_scenarios is set to true**).



![prod : model workflow: /eucalc/ interactions/workflows/data-processing output_nodes : 3789 cut 3836 cut 3836 cut 3855 cut node node node node node node node node node node node ' node node node a cut I ' 3877 3882 384a 3847 3874 3875 37ß4 37ß4 388a cut cut cut cut cut cut cut cut cut # Buildings ' # Lifestyle Transpo r t Indus try Ammon a Power Agricul ture # Land use # Climate Emissions # C Limate Emissions PIs *energy demand by se tor compared to RES *energy dia r mb sector compared to RES *Bioenergy balance *Air pollution enable webtool scenarios: true * ont if cache after cube is set to false ! Powerbi : scenarios: # Scenarios for production urpose (for Webtoot menu) REF: [1.1, 1.1, CORE -8%: [2.5, CORE -95%: [3.3, Behavior: 4, Technology: C 3, 1.1, 1.1, 2, 2, 2, 2.5, 2.5, 2, 2, 2.5, 2. 3.3, 2, 3.5, 2.5, 1, 1.5, 2.5, 5, 3.5, 3.5, 3.5, 2.5, 3, 3, 2, 3.5, 3.5, 3.5, 1.1, 1.1, 1.1, 1.1, 1.5, 2.5, 2.5, 2.5, 3.3, 3.3, 3.3, 3.3 1.1, 2.5, 3.3, 1.1, 2.5, 3.5, AFOLIJ intensification 4, 2.5, 4, 4, and diet. AFOLIJ agro-forestD' 3.5, 3.5, 3.5, 3, 4, 1.5, 3.5, 3.5, 3.5, 3.5, 2.5, 3.5, 2, 4, 4, 4, 3.5, 4, 3.5, 1, All All All All 4: 4 2, 4 4 no 2, 4 diet. • [3.5, 3 , 2, 4 2, 4 4 2, 4 4, 4, 2, 4, 2, 4, 4, 2, 4, 4, 2, 4, 4, 2, 4, 2, 4, 2, 4, 4, 2, 4, 4, ](../../../media/Technical-Documentation-Configuration-Files-image7.png){width="6.208333333333333in" height="5.947916666666667in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>WHAT TO DO ?</strong></p><p><strong>Should be done in each project configuration file !</strong></p><p><strong>If new lever</strong> : you should add the appropriate value here in each scenario array.</p><p></p><p><strong>If new module</strong> : you should add the node of your module in the output_nodes setting</p><p><em>Example for output_nodes</em> :</p><p>Air pollution : metanode number in data-processing is <strong>3880</strong> and number of the port containing values for Pathway to Explorer is <strong>1</strong> ==&gt; node = 'node_<strong>3880</strong>_out_<strong>1</strong>'</p><p></p><p><img src="../../../media/Technical-Documentation-Configuration-Files-image8.png" style="width:2.84375in;height:2.14583in" alt="Air pollution #3886 i 0.2 Pathway Ex plorer " /></p><p></p><p>&gt;&gt; The script "configure_config_docker.py" is designed to do it for you (see <a href="onenote:#Release%20Support&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Release Support</a>)</p><p></p></th></tr></thead><tbody></tbody></table>




-   **Interfaces** : defines where the interface.xlsx could be found, which sheets are to be used, ... ; spreadsheet_id is the id of the interface google_sheet.



![interfaces: file: /eucaIc/_interactions/configuration/3E/interface.xIsx levers sheet: Levers level sheet: "Level data" cube_sheet: "a .2 Pathway Explorer" graphs_sheet: Graphs true 'Al : 125øa' spreadsheet_id: credentials: credentials . json level data: file: .xlsx max cache size: ](../../../media/Technical-Documentation-Configuration-Files-image9.png){width="5.0in" height="3.0625in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If new project</strong> : make sure you set the appropriate settings above.</p><p></p></th></tr></thead><tbody></tbody></table>





## What are the differences between configuration files ?

There are a lot of differences between configuration files in the "model" settings. According you are working on local or the confi file is used by the web application, settings are differents. They are also differents depending on the project you are working on. Therefore, refer to the description of each settings above to see what to set there.





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="log">Log</h1></th></tr></thead><tbody></tbody></table>



## What are the settings ?

Here, we set the level of details we have in the log. If we are testing our web application a higher level of data is required. But, while running the web application, it is better to avoid to have logs as it slows down the running time.

Level of details is given in the *level* setting. Here, value is INFO. For other possible values <https://reflectoring.io/logging-levels/>



## What are the differences between configuration files ?

No matters the project, database is the same for all of them. Nevertheless, differences are found between config_docker and config files ;



<table><colgroup><col style="width: 49%" /><col style="width: 50%" /></colgroup><thead><tr class="header"><th>Config_docker_<em>project_abbr</em></th><th>Config_<em>project_abbr</em></th></tr></thead><tbody><tr class="odd"><td><p><img src="../../../media/Technical-Documentation-Configuration-Files-image10.png" style="width:2.23958in;height:0.89583in" alt="- log: suppress run_logs: True level: INFO *fit e: api.Log " /></p><p></p><p></p><p>Here, we do not allow the logs.</p></td><td><p><img src="../../../media/Technical-Documentation-Configuration-Files-image11.png" style="width:2.21875in;height:0.95833in" alt=") log: suppress run_logs: False level: INFO file: api.log " /></p><p></p><p></p><p>Here, we allow the logs</p></td></tr></tbody></table>







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="api">Api</h1></th></tr></thead><tbody></tbody></table>



## What are the settings ?

Here, we define the port of the api and a country list with abbreviation correspondance.



## What are the differences between configuration files ?

No differences are found between config_docker and config files. But we have differences between project as the country list should be changed according to our project.



<table><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><thead><tr class="header"><th>Project 1 (eg. BE)</th><th>Project 2 (eg. EU)</th></tr></thead><tbody><tr class="odd"><td><p><img src="../../../media/Technical-Documentation-Configuration-Files-image12.png" style="width:2.23958in;height:2.0625in" alt="&#39;apl: port : 5øøø country codes: brussels: SR belgium: BE flanders: VL wallonia: WL rest of the world : " /></p><p></p></td><td><p><img src="../../../media/Technical-Documentation-Configuration-Files-image13.png" style="width:2in;height:4.83333in" alt="flapl: country codes: Austria: AT Belgium: BE Bulgaria: Croatia: HR Cyprus: CY Czech Republic: Denmark. Estonia: Finland. France. Germany. Greece: Hungary. Ireland: Italy: Latvia . • LV Lithuania: LT Luxembourg. malta: MT Netherlands. cz " /></p><p></p></td></tr></tbody></table>













