<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [_common](#_common)
- [Introduction](#introduction)
- [Excel File : google_sheets_ids ](#excel-file--google_sheets_ids)
- [Excel File : short_long_names](#excel-file--short_long_names)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# _common

Created: 2020-04-27 10:34:38 +0200

Modified: 2020-04-27 13:50:56 +0200

---


<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="introduction">Introduction</h1></th></tr></thead><tbody></tbody></table>



The _common folder in dev was created to add all the metanodes used by all sectors/modules (cfr *User Manual > Knime > Metanodes Library*).

It also contains some excel file that allows us to be more flexible while creating a new project or a new module as it summarize some information accessible by each metanodes. These files are set ine the configuuration folder and are described in the following section.



![C:) XCaIc Nom conf.xlsx dev configuration common x calc-knime-api-3a4fc023d8f3.p12 Modifié le 02-03-2009:18 18-03-20 14:46 02-03-2009:18 ](../../../media/Technical-Documentation-_common-image1.png){width="4.270833333333333in" height="2.1770833333333335in"}





Some metanodes use a "Single Selection" or a "Rule Engine" node to allow the user to choose which module he wants to work on in a selection box.

*Example of Selection box* :

![Texte de remplacement généré par une machine: Dialog - D•4D6 Update interface L2 - Googl... Optons Flon Variables Memory P olicy Curren t Module 6.5 Macro Econom y [Z] Change OK - Execute Apply Cancel ](../../../media/Technical-Documentation-_common-image2.png){width="4.5in" height="5.34375in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If new module</strong></p><p>When you create a new module, you should change the available list of choices. This should be done for all the following metanodes that are set in _common (lib &gt; metanodes). <strong>Before modifying them, be sure you get the latest vesion of _common module with a pull request</strong>.</p><p></p><p>Metanodes that should be changed :</p><p></p><table><colgroup><col style="width: 19%" /><col style="width: 80%" /></colgroup><thead><tr class="header"><th><strong>Metanode</strong></th><th><strong>Where to change</strong></th></tr></thead><tbody><tr class="odd"><td><p>BeCalc - Data Import - Calibration</p><p>BeCalc - Data Import - CP</p><p>BeCalc - Data Import - Full Time Series</p></td><td><p><img src="../../../media/Technical-Documentation-_common-image3.png" style="width:3.07292in;height:4.40625in" alt="Texte de remplacement généré par une machine: Inputs BEC* - mstrlnç to ID " /></p><p></p></td></tr><tr class="even"><td>BeCalc - Data Import - OTS/FTS</td><td><p><img src="../../../media/Technical-Documentation-_common-image4.png" style="width:1.625in;height:5.28125in" alt="Texte de remplacement généré par une machine: Data import and constants " /></p><p></p></td></tr><tr class="odd"><td><p>Xcalc - Data from other modules</p><p>Xcalc - Write data for other module</p></td><td><p><img src="../../../media/Technical-Documentation-_common-image5.png" style="width:4.82292in;height:2.88542in" alt="Texte de remplacement généré par une machine: Create flow variables Single Selection Merge Variables Source module selection Single Selection Ru le Engine Variable Ru le Engine Variable Ru le Engine Variable Folder to use String Manipulation (Variable) File path So rte trigram Destination trigram tination module sele Component In " /></p><p></p></td></tr><tr class="even"><td>Update Interface L2 - Google Sheets</td><td><p><img src="../../../media/Technical-Documentation-_common-image6.png" style="width:4.44792in;height:2.69792in" /></p><p></p></td></tr></tbody></table><p></p></th></tr></thead><tbody></tbody></table>





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="excel-file-google_sheets_ids">Excel File : google_sheets_ids </h1></th></tr></thead><tbody></tbody></table>



This file specifies what is the google sheet id of each module for each project.



![google_sheet_prefix ELIA ELIA ELIA ELIA ELIA ELIA ELIA ELIA ELIA ELIA ELIA ELIA module agriculture buildings climate electricity_supply industry interface lifestyle parameters technology transport air_pollution air_mortality agriculture buildings climate electricity_supply industry interface lifestyle parameters technology transport air_pollution air_mortality agriculture buildings climate v google_sheet_id IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO 14dDa2aiEjL8JMPdh1xqudhxcpWM7vxDccg1SilpxxN4 IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IJ486DecMM FOD7Lq138aUl WEw4nKebmpJOFfwbaeVl IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IPEiTK7VMC217qrf3vj1Xtg73txkWCWC1hRzAGA2VLaY IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO IUEpeo_9RQhOq5EmCYatDWGtDQNRfXJaS-HtrUOvTEJO lyAA4HqaMYcuw5sPleFmZ8Rq791_5esRFFxDwcfPzWl IrKV4Q6J5VRhMjHF481E8M1v-fDtF5esFUzdxRDFrpJO 1sWzbigFg-tNW7480HSlxhLOFD Qw8x66Hnq212Hh3A ](../../../media/Technical-Documentation-_common-image7.png){width="6.03125in" height="4.322916666666667in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If new project</strong></p><p>For each existing module, add the google_sheet prefix of your project in new lines and refer the google sheet containing all the OTS and FTS data in the google_sheet_id column.</p><p><strong>If new module</strong></p><p>Add your module for each existing project prefix and refer the google sheet containing all the OTS and FTS data in the google_sheet_id column</p><p></p><p>More details here : <a href="onenote:#Create%20new%20project&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={2BBA0D6C-CB72-46C2-B669-136224CC3529}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Create new project</a> (cfr "Note about data collection")</p><p></p></th></tr></thead><tbody></tbody></table>





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="excel-file-short_long_names">Excel File : short_long_names</h1></th></tr></thead><tbody></tbody></table>



Indicates for each module what is the "short" name and whet is the "long" name. Indeed, some metanodesin Knime use the long name version and other the short name. This allow to make the link between them.



**Note** : long name should be the same that the one use in the interface google sheet (cfr [Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



![Texte de remplacement généré par une machine: module short lifestyle climate technology na mes technology costs climate emissions buildings transport district heating industry ammonia power land use minerais agriculture water biodiversity bioenergy balance electricity_supply oil refinery air_pollution employment macroeco v module_long_names 1.1 Lifestyle 1.2 Climate 1.3 Technology 1.3 Technology costs 1.4 Climate Emissions 2.1 Buildings 2.2 Transport 2.3 District Heating 3.1a Industry 3.1b Industry Ammonia 3.1c Industry Power 4.1 Land-use 4.2 Minerals 4.3 Agriculture 4.4 Water 4.5 Biodiversity 4.6 Bioenergy Balance 5.1 Electricity 5.2 Oil Refinery 6.2 Air Pollution 6.4 Employment 6.5 Macro Economy ](../../../media/Technical-Documentation-_common-image8.png){width="6.302083333333333in" height="5.15625in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p><strong>If new module</strong></p><p>Add the name (short and long) in this file</p><p></p></th></tr></thead><tbody></tbody></table>








