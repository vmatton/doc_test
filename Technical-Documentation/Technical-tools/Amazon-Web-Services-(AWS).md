<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Amazon Web Services (AWS)](#amazon-web-services-aws)
  - [How to be connected to AWS ?](#how-to-be-connected-to-aws-)
  - [How to add someone to a project/server ?](#how-to-add-someone-to-a-projectserver-)
- [How to create a server for a project ?](#how-to-create-a-server-for-a-project-)
  - [How to create an image of a server ?](#how-to-create-an-image-of-a-server-)
  - [How to create a server with an AMI ?](#how-to-create-a-server-with-an-ami-)
  - [Next steps while creating a new server](#next-steps-while-creating-a-new-server)
    - [Start and stop Rules](#start-and-stop-rules)
    - [IP address](#ip-address)
    - [Domain name](#domain-name)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Amazon Web Services (AWS)

Created: 2020-04-15 08:58:35 +0200

Modified: 2021-09-28 09:49:54 +0200

---

|--------------|



AWS offers you several types of server (instances). Up to now, for Climact, only two types of instances are used :
-   Linux (no "win" suffix in name)
-   Windows (set "win" suffix in name)



Example :

![Name becalc-prod (powerbi) linux becalc-dev (webtool) linux eucal -win- nime-3T2 windows eucalc-dev (docker builder) temp VLAIO linux elia (powerbi) linux xcalc in nime-4.1 Windows vlaio-dev (powerbi) linux eu2060-prod (powerbi) linux greenpea -win- mme Windows ID d"instance i-08312f17fabd966b6 i-OcOff33aOf430183e i-0096cbb8fc08012e3 i-OOa4661S39b6f61S9 i-0213b51c3a8a34fc8 i-02c1c03867f9d8ca4 i-090ab7e16e9137abf i-09S9a40fdS63c3be7 i-Oa08920S8820a93__ ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image1.png){width="4.625in" height="2.8125in"}



Each instance is paied by hour of use (the price depends on the type and size of the instance), so it is better to stopped them when there are not used (automatic start and stop can be configured). For prices, cfr : <https://aws.amazon.com/fr/ec2/pricing/on-demand/>



Differences between Linux and windows



<table><colgroup><col style="width: 17%" /><col style="width: 54%" /><col style="width: 28%" /></colgroup><thead><tr class="header"><th></th><th><strong>Windows</strong></th><th><strong>Linux</strong></th></tr></thead><tbody><tr class="odd"><td><strong>Purpose</strong></td><td><p>For Knime</p><p>For Python when we need a more powerful machine than our laptop</p></td><td>For remote (website)</td></tr><tr class="even"><td><strong>How to open ?</strong></td><td>Remote desktop connexion (only one person at the same time)</td><td>MobaXterm</td></tr><tr class="odd"><td><strong>Saving modification</strong></td><td>Take care ! If modifications are done and not saved when the server is turn down, everything is lost !</td><td>No need to save anything.</td></tr><tr class="even"><td><strong>Shut down</strong></td><td>Do it manually (to be sure, any modifications will be saved)</td><td>Turn on and down automatically according to a time schedule</td></tr><tr class="odd"><td><strong>What if new project ?</strong></td><td><p>It does not depend on the project but on the computer capacity of each one. Usually, everyone uses his own computer to run the _interactions module. But for some reason, a computer could be not "powerfull" enough to run thiw workflow. In this case, it is recommended to work on a remote server (one server by worker).</p><p></p><p><strong>Note</strong> : Exception for EUCalc. As it still uses a previous version of Knime, we have a server for this specific project.</p></td><td>For each new project, create a new linux instance (you should have two servers by projects ; one for the PowerBi and one for the webtool)</td></tr></tbody></table>





| Connection to AWS |
|-------------------|



URL : <https://aws.amazon.com/>



## How to be connected to AWS ?

Go to My Account > AWS Management Console and sign in. If not already registered, go to "Create an AWS Account" first.



*Figure 1 : Go to the management console*

![IT Maison Matton Orthographe Contact Sales Support English My Account • Other an AWS Account AWS Management Console Account Settings Billing & Cost Management Security Credentials AWS Personal Health Dashboard )istro for Elasticsearch Elyze your logs with the only 100% open source Elasticsearch ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image2.png){width="4.15625in" height="3.3958333333333335in"}



*Figure 2 : Sign it*

![aws Sign in O Email address ot your AWS account Or to sign in as an IAM user, enter your account ID or account alias instead. Vincent Next New to AWS? Create a new AWS account ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image3.png){width="3.46875in" height="3.8125in"}

![aws Account ID or alias 910931614638 IAM user name Vincent Password Sign In Sign-in using root account credentials Forgot password? ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image4.png){width="3.09375in" height="3.96875in"}





## How to add someone to a project/server ?

**Note : each user is linked to a project. No need to create a personnal account. Just add a user to the project and his account will be created at the same time !**



In the AWS Management Console, look for IAM and select it.

![Texte de remplacement généré par une machine: AWS Management Console AWS services Find Services You can enter names, kep.vords or acronyms_ Q IAM IAM Manage access to AWS resources ecen y e services ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image5.png){width="5.135416666666667in" height="2.8333333333333335in"}





In the menu on the left, go to Access management > Users

![Texte de remplacement généré par une machine: Identity and Access Management (IAM) Dashboard Access management Groups Users Policies Identity providers Account settings ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image6.png){width="2.8854166666666665in" height="3.53125in"}



You can add a user by clicking on the appropriate button.

![Texte de remplacement généré par une machine: Add user Q Find use Delete user username or access key user name Amaury Benoit Maite Marion L Michel C Quentin_s Vincent ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image7.png){width="4.5in" height="3.7604166666666665in"}



![Texte de remplacement généré par une machine: Set user details You can add multiple users at once With the same access type and permissions. Learn more user name* Quentin J O Add another user Select AWS access type Select how these users Will access AWS Access keys and autogenerated passwords are provided in the last step_ Learn more Access type* Console password* Require password reset Programmatic access Enables an access key ID and secret access key for the AWS API, CLI, SDK, and other development tools_ AWS Management Console access Enables a password that allows users to sign-in to the AWS Management Console. Autogenerated password Custom password User must create a new password at next sign-in Users automatically get the IAMUsercnangePassword policy to allow tnem to change their own password. ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image8.png){width="8.510416666666666in" height="5.1875in"}



![Texte de remplacement généré par une machine: Set permissions Add user to group Select an existing user trom whicn Copy permissions trom existing user Copy permissions from existing user Q Search user name Amaury Attach existing policies directly Attached policies Showing 7 results Benoit Maite Marion Michel c Quentin S Vincent Groups None None None None None None None AmazonEC2ContainerRegistr_. AmazonEC2ContainerRegistr_. AmazonEC2ContainerRegistr_. AmazonEC2ContainerRegistr_. AmazonEC2ContainerRegistr_. AmazonEC2ContainerRegistr_._ AmazonEC2ContainerRegistr_. and 2 more and 2 more and 2 more and 2 more and 2 more and 2 more and 2 more ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image9.png){width="8.291666666666666in" height="4.8125in"}



| Starting specific server |
|--------------------------|



1.  Go to EC2 service (Elastic Clouds 2 services)



![aws O IAM Services v Resource Groups v Vincent @ 9109-3161-4638 Access resources on the go Access the Management Console using the AWS Console Mobile App. Learn more Explore AWS Amazon DynamoDB Frankfurt Support AWS Management Console AWS services Find Services You can enter names, ke»vords or acronyms. Q Example: Relational Database Service, database, ROS Recently visited services Billing @ EC2 All services Compute EC2 Lightsail ECR ECS EKS Lambda Batch Elastic Beanstalk Serverless Application Repository AWS Outposts EC2 Image Builder Storage s; EFS FSx SS Glacier Lambda Developer Tools Cod eStar CodeCommit CodeBuild Cod eDep loy CodePipeline Clouds X-Ray Customer Enablement AWS IQ Support Managed Services Robotics AWS RoboMaker @ Support Machine Learning Amazon SageMaker Amazon CodeGuru Amazon Comprehend Amazon Forecast Amazon Fraud Detector Amazon Kendra Amazon Lex Amazon Machine Learning Amazon Personalize Amazon Polly Amazon Rekognition Amazon Textract Amazon Transcribe Amazon Translate AWS DeepLens AWS DeepRacer Amazon Augmented Al Mobile AWS Amplify Mobile Hub AWS AppSync Device Farm AR & VR Amazon Sumerian Application Integration Step Functions Amazon EventBridge Amazon MQ Simple Notification Service Simple Queue Service SWF Want more scale? Try a serverless NoSQL database service for your modern application. Get started AWS IQ Connect with AWS Certified third-party experts for on-demand consultations and project help. Get started Free Digital Training Get access to 350+ self-paced online courses covering AWS products and services. Learn more Amazon SageMaker Studio The first visual integrated development environment for machine learning. Learn more Have feedback? ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image10.png){width="10.427083333333334in" height="5.395833333333333in"}



2.  Select 'Frankfurt' (eu-central-1) on the localisation menu (upper right)

![Maite @ 9109-3161-4638 Ohio A USA Est (Virginie du N us-east-l USA Est(O us-east-2 uest (Californie du Nord) us-west-l USA Ouest (Oregon) us-west-2 Asie Pacifique (Hong Kong) ap-east-l Asie Pacifique (Mumbai) ap-south- 1 Asie Pacifique (Séoul) ap-northeast-2 Asie Pacifique (Singapour) ap-southeast-l Asie Pacifique (Sydney) ap-southeast-2 Asie Pacifique (Tokyo) ap-northeast- 1 Canada (Central) ca-central-l Europe (Francfort) eu-central-l Europe (Irlande) eu-west- 1 Europe (Londres) eu-west-2 Europe (Paris) eu-west-3 Europe (Stockholm) eu-north-l Moyen-Orient (Bahreïn) me-south-l Amérique du Sud (Sao Paulo) sa-east- 1 Nous contacter Suppo c ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image11.png){width="2.40625in" height="5.0in"}



3.  Select 'Running instances'



![aws Services Resource Groups v @ Welcome to the new EC2 console! Vincent @ 9109-3161-4638 Frankfurt Support New EC2 Experience Tell us you think EC2 Dashboard Events Tags Reports Limits INSTANCES Instances Instance Types Launch Templates Spot Requests Savings Plans Reserved Instances Dedicated Hosts Capacity Reservations IMAGES AMIS Bundle Tasks ELASTIC BLOCK STORE Volumes Snapshots Lifecycle Manager NETWORK & SECURITY Security Groups Elastic IPS New We're redesigning the EC2 console to make it easier to use and improve performance. We'll release new screens periodically. We encourage you to try them and let us know where we can make improvements. TO switch between the Old console and the new console, use the New EC2 Experience toggle. EC2 Resources Y are using the following Amazon EC2 resources in the Europe (Frankfurt) Region: 7 8 9 Dedicated Hosts Load balancers Placement groups x Account attributes Supported platforms vpc Default VPC vpc-4796902c Console experiments Settings Additional information Getting started guide Documentation All EC2 resources Forums Pricing Contact us c Runni g instances Snapshots Key pairs 2 2 2 Elastic IPS Volumes Security groups @ Easily size, configure, and deploy Microsoft SQL Server Always On availability groups on AWS using the AWS Launch Wizard for SQL Server. Learn more Launch instance To get started, launch an Amazon EC2 instance, which is a virtual server in the cloud. Service health Region Europe (Frankfurt) Availability Zone status Zone eu-central- 1 a (eucl-az2) Service Health Dashboard Launch instance Note: Your instances will launch in the Europe (Frankfurt) Region Scheduled events Europe (Frankfurt) c Status @This service is operating normally Status @Availability Zone is operating normally ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image12.png){width="10.489583333333334in" height="4.84375in"}





4.  Select the server you want to start

5.  Go to 'Action'>'Instance State'>'Start'

==> The linux server (for PowerBi or webtool) will automatically start and build the model so **you need to wait 5 min before making your first request**.

==> NOTE: the linux server are programmed to be stopped at 7pm. To change this behavior go to the Lambda functions or adjust the timing in the CloudWatch service (cfr further on "*How to schedule a turn off/on automatically ?*").



![aws Services New EC2 Experience Tell us you think EC2 Dashboard Events Tags Reports Limits INSTANCES Instances Instance Types Launch Templates Spot Requests Savings Plans Reserved Instances Dedicated Hosts Capacity Reservations IMAGES AMIS Bundle Tasks ELASTIC BLOCK STORE Volumes Resource Groups Launch Instance Filter by tags and attributes or searc Name becalc-dev (webtool) becalc-prod (powerbi) elia (powerbi) eu2060-prod (powerbi) eucalc-dev (docker builder) ucalc-win-knime-372 reenpeace-win-knime xcalc-win-knime-4.1 Actions Connect Get Windows Password Create Template From Instance Launch More Like This Instance State Instance Setti7gs Image Network ng CloudWatch Monitoring A ilability Zone Start Stop Stop - Hibernate Reboot Term friate eu-central- 1b eu-central- 1b eu-central- lc Instance State running running stopped stopped stopped stopped stopped stopped Status Checks 2/2 checks 2/2 checks i-0096cbb8fc08012e3 i-Oa08920S8820a93_ i-02c1c03867f9d8ca4 t32xlarge t32xlarge t32xlarge Alarm S None None None None None None No C No C Instance: i-0213b51c3a8a34tc8 (elia (powerbi)) Elastic 'P: 3.125.242.214 ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image13.png){width="10.53125in" height="6.3125in"}







| How to open each server ? |
|---------------------------|



For windows servers

First, make sure your instance is running. If not, start it (cfr previous steps)

Open the remote desktop window :
-   Desktop : set the DNS public of the server you want to reach
-   User name : administrator
-   Password : kUpG)v%.ZOgxt@L&HWDZU)mrs7y!hIt.



![YouTube Maps eucalc Vince 'Ices v Groupes de ressources Lancer une instance 2050 Se connecter 8itbucket Action 8ECALC Connexion Bureau distance Connexion Bureau A distance Général chage Ressoumes locales Expénence -Paramétres d 'ou re de session Entrez le no ordinateur distant Maite @ 9109-3161-4638 Francfort Q Filtrer par balises et attributs ou rechercher par mot clé Name becalc-dev (webtool) becalc-prod (powerbi) elia (powerbi) eu2060-prod (powerbi) eucalc-dev (docker builder) temp VLAIO eucalc-win-knime-372 greenpeace-win-knime vlaio-dev (powerbi) xcalc-win-knime-4.1 Av ancé Ordinateur 16 eucentral-l compute amazonawscom Vos Homations d identification semnt demandées 10B de la connexlom Me pemettre d enregistrer les informations d identification - P aramétres de connexion Enregistrez les paramétres de connexion actuals dans un fichier RD? ou ouvrez une connexion enregistrée Masquer les options Etat de "instancy running running stopped running stopped stopped stopped stopped running Contröles des s • 2/2 contröle 2/2 contröle 2/2 contröle 2/2 contröle Statut des alam Aucun(e) Aucun(e) Aucun(e) Aucun(e) Aucun(e) Aucun(e) Aucun(e) ALARME DNS public (IPv4) ec2-3-120-222-173. eu-central- 1. compute.amazonaws.com ec2-54-93-129-246. eu-central- 1. compute.amazonaws.com ec2-3-125-242-214. eu-central- 1. compute.amazonaws.com ec2-3-123-139-87 _eu-central-l _computeamazonawscom ec2-3-126-21-163_eu-central-1 _computeamazonawscom ec2-3-125-16-86.eu-central-1.compute.amazonaws.com ec2-52-68-126-167. eu-central- 1. compute.amazonaws.com ec2-3-126-220-20_eu-central-1 _compute amazonawscom c2-3-123-171-86_eu-central-1 _compute.amazonaws. com Suppc 1 Sur! IP publi 54831: 3.125 a 3.123.1: 3.1262' 3.1251k 52581: 3.125 z 3.123.17 ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image14.png){width="13.520833333333334in" height="5.03125in"}





For Linux servers

1.  Make sure you already have the SSH keys for AWS. If not, follow these steps :

    a.  Create a folder named "ssh keys" in local (should look like : C: Users  Your_User_Name  ssh keys). This is not the same as .ssh folder created for other purposes

    b.  Go to Microsoft Teams > XCalc PROPECTIVE > Files : copy/paste the eucalc-aws.pem in your ssh keys folders



![XCalc Publications EIJ2050 - TODO Fichiers Cha Meeting Notes EU2050 Wiki + Nouveau v XCalc > Keys Nom v Synchroniser Q) Copier le lien Modifié I v 17 février 16 janvier 16 janvier Télécharger Modifié par v Vincent Matton Amauty Anciaux Amauty Anciaux ab Ouvrir dans SharePoint eucalc-aws.pem docker-build rsa docker-build_rsa.pub ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image15.png){width="7.65625in" height="3.9375in"}





2.  Open MobaXterm. If not already install, go to <https://mobaxterm.mobatek.net/download.html> to do so.



3.  Click on Session > Select SSH Keys :


-   Remote host : the IP Public of the server you want to reach
-   Specify username : ec2-user



*In Advanced SSH Settings*
-   Use private key : Choose direction for your SSH keys linked to AWS (cfr step 1.)



![McbaXterm Terminal Sessions Servers View Tools X server Tools Games Settings Macros Help Games Sessions Vie n Split Mul bExec Tunneling Packages Se tbngs Help x X server Exit Q •ck connect... Session settings SSH Telnet Rsh Xdmcp VNC FTP SFTP Serial ec2-user File Shell Browser Mosh Aws S3 WSL Basic SSH settings Remote host * Advanced SSH settings Z] Xl 1 -Forwarding Execute command: SSH-browser typ Z] Specify' username Terminal settings Z] Compression rotocol Network settings Bookmark settings Z] Use private key éJonasssh Execute macro at session start: <none> e 0K Remote environment: Interactive shell D Do not exit after command ends D Follow SSH path (experimental) Adapt locales on remote server Cancel ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image16.png){width="13.84375in" height="7.46875in"}





4.  Change the default name by a name corresponding to AWS (eg. AWS : vlaio-dev (powerbi) ==> MobaXterm : vlaio-AWS (powerbi))

![user sessions 3.126.220.20 Secalc Rename session Edit session Delete session Duplicate session Save session to file Create a desktop shortcut Connect as... Ex e cute Ping host ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image17.png){width="3.375in" height="3.2395833333333335in"}







How to schedule a turn off/on automatically ?

In the AWS Management Console, look for Cloud Watch and select it.

![Texte de remplacement généré par une machine: AWS Management Console AWS services Find Services You can enter names, kep.vords or acronyms_ Q Cloud watchl CloudWatch Monitor Resources and A lications ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image18.png){width="5.125in" height="2.6770833333333335in"}



In the menu on the left side, go to Events > Rules

![Texte de remplacement généré par une machine: I CloudWatch Dashboards Alarms ALARM INSUFFICIENT OK Billing Logs Log groups Insights Metrics Events Event Buses ServiceLens Service Map Traces Canaries Contributor Insights Settings o o ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image19.png){width="2.1979166666666665in" height="5.989583333333333in"}





Here, you will defined the rules that can be used to start and stop your instances (servers).

In the example below, the Start rule is not activated (in grey) as we only want to start an instance when needed (therefore, it is done manually). On the other side, the Stop rule is activated (in green) as we want to be sure, each time an instance has been started, it will be stopped at the end of the day.

![Texte de remplacement généré par une machine: Rules Rules route events from your AWS resources for processing by selected targets. You can create, edit, and delete rules_ Create rule Actions • Name Status C) C) Ali Status Na me StartEC21nstances StopEC21nstances Description Start EC2 instances every morning at BAM (GMT+I) Stop EC2 instances every evening at 7PM (GMT+I) ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image20.png){width="9.260416666666666in" height="2.28125in"}



If you click on a rule (here : StopEC2Instances), the following window appears :
-   **Schedule** : enter a cron expression to define when the lambda function will be run. You can visualize to what your cron expression correspond in the "trigger" frame below.
-   **Description** : define here what the rule does
-   **Targets** : select here which lambda functions are run at the "trigger time".

![Texte de remplacement généré par une machine: Rules > StopEC21nstances Summary ARN O Schedule Next 10 Trigger Date(s) Status Description Monitoring Targets Filter: Type Lambda function am:avvseventseu-central- Cron expression e 18 ? * x 1. Mon, 04 May 2020 GMT 2 Tue, 05 May 2020 18 00:00 GMT 3. wed, 06 May 2020 18:oono GMT 4. Thu, 07 May 2020 GMT 5. Fri, 08 May 2020 GMT 6. Mon, 11 May 2020 GMT 7. Tue, 12 May 2020 18 00:00 GMT & wed, 13 May 2020 GMT 9. Thu, 14 May 2020 18:oom GMT 10. Fri, 15 May 2020 GMT Enabled Stop EC2 instances every evening at TPM (GMT+I) Show metrics for the rule Na me StopEC21nstances Input Matched event ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image21.png){width="5.052083333333333in" height="4.760416666666667in"}

Modification of rules are made by selecting Actions > Edit (right top side of the window).

![Texte de remplacement généré par une machine: Actions • Edit Delete ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image22.png){width="1.5625in" height="1.8333333333333333in"}

In order to modify the lambda function, click on it (cfr Name in blue, previous Figure) or go to the AWS Management Console, and look for lambda :

![Texte de remplacement généré par une machine: AWS Management Console AWS services Find Services You can enter names, kep.vords or acronyms_ Q lambda Lambda Run Code without Thinking about Servers Amazon Lex ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image23.png){width="5.770833333333333in" height="2.8229166666666665in"}



You will find all the lambda functions you already created.

![Texte de remplacement généré par une machine: Lambda Functions Functions (2) Q Filter by tags and attributes or search by keyword Description o o Function name StopEC21nstances StartEC21nstances Runtime 3.7 3.7 ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image24.png){width="6.625in" height="2.28125in"}



To modify one of them, select it (here : StopEC2Instances) :

You have the code for the lambda function. In red : all the instance ids concerned by this function :

![Texte de remplacement généré par une machine: Tools Window lambda function import bot03 Save Test instances def context): et 2. stop_ins tan celds -instances) print( • stopped your instances: ' + str(instances)) ' i-e213b51c3a8a34fc8'] ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image25.png){width="7.947916666666667in" height="2.9375in"}



If you create a new instance and you want this function to apply on it, you should add its id in the instances list (in red). You can find the id of an instance in the EC2 > Instances :

![Texte de remplacement généré par une machine: Name xcalc-win-knime-4.1 becalc-dev (webtool) eucalc-win-knime-3.7 Q eucalc-dev (docker builder) temp VLAIO elia (powerbi) elia-dev (webtool) becalc-prod (powerbi) vlaio-dev (powerbi) eu2050-prod (powerbi) greenpeace-win-knime Instance ID i-02c1c03867f9d8ca4 i-OcOff33aOf430183e i-0096cbb8fc08012e3 i-OOa4661539b6f6159 i-0213b51c3a8a34fc8 i-07dad94aa2677e4___ i-08312f17fabd966b6 i-090ab7e16e9137abf i-0959a40fd563c3be7 i-Oa0892058820a93__ Instance Type t3Qxlarge c5xlarge t3Qxlarge c52xlarge c5xlarge c5xlarge c5xlarge c51arge c5xlarge t3Qxlarge Availability Zon eu-central-lc eu-central- Ic eu-central- lb eu-central- Ic eu-central- Ic eu-central- lb eu-central- lb eu-central- lb eu-central- Ic eu-central- lb ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image26.png){width="6.708333333333333in" height="3.09375in"}









How to run codes automatically when server are started ?

1.  Select an instance you want to automatically turn off/on.

2.  Go to Actions > Instance Settings > View / Change User data

![Launch Instance Connect Filter by tags and attributes ors rc Name becalc-prod (powerbi) becalc-dev (webtool) eucalc-win-knime-372 eucalc-dev (docker builder) tem elia (powerbi) xcalc-win-knime-4.1 vlaio-dev (powerbi) eu2060-prod (powerbi) greenpeace-win-knime Actions Connect Get Windows Password Create Template From Instance Launch More Like This Instance State Instance Settings Image Networkng CloudWatch Monitoring i-02c1c03867f9d i-090ab7e16e91 i-09S9a40fdS63c i-Oa08920S8820a 6b6 Instance Type c5 xlarge Availability Zone eu-central- 1b Add/Edit Tags Attach to Auto Scaing Group Attach/Replace IAM Role Change Instance Type Change Termination Protection View/Change User Data Change Shutdown Behavior Change T2JT3 Unlimited Get System Log Get Instance Screenshot Modify Instance Placement Modify Capacity Reservation Settings ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image27.png){width="7.020833333333333in" height="5.0in"}



3.  Copy / Paste :

<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p>Content-Type: multipart/mixed; boundary="//"</p><p>MIME-Version: 1.0</p><p>--//</p><p>Content-Type: text/cloud-config; charset="us-ascii"</p><p>MIME-Version: 1.0</p><p>Content-Transfer-Encoding: 7bit</p><p>Content-Disposition: attachment; filename="cloud-config.txt"</p><p>#cloud-config</p><p>cloud_final_modules:</p><p>- [scripts-user, always]</p><p>--//</p><p>Content-Type: text/x-shellscript; charset="us-ascii"</p><p>MIME-Version: 1.0</p><p>Content-Transfer-Encoding: 7bit</p><p>Content-Disposition: attachment; filename="userdata.txt"</p><p><strong>#!/bin/bash</strong></p><p><strong>cd /home/ec2-user/PatEx/knime2python/</strong></p><p><strong>docker-compose up –d</strong></p><p><strong>cd /home/ec2-user/PatEx/webtool/</strong></p><p><strong>screen -dmS run sh -c 'serve -S build -l 3000; exec bash'</strong></p><p><em><strong>-- Other command lines to run other codes here --</strong></em></p><p><strong>--//</strong></p><p></p></th></tr></thead><tbody></tbody></table>





==> Note : the first part in bold indicates that each time the instance is turn on, the docker should be run as well (docker is set in the folder : /home/ec2-user/eucalc/knime2python/) (more information about dockers : [MobaXTerm](onenote:#MobaXTerm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={D305507A-BF69-46A1-AB14-BDD34B4CF290}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))

==> Note: the second part in bold indicates that each time the instance is turn on, the webtool will be served on port 3000 on a detached screen.

==> **Note : when command lines : always be in knimetopython folder (eg. /home/ec2-user/eucalc/knime2python/)**





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="how-to-create-a-server-for-a-project">How to create a server for a project ?</h1></th></tr></thead><tbody></tbody></table>



Servers are created based on an already existing image of a "standard" server (AMI). This is done to ease the creation of a server as it avoids to clone and install what was already done in an existing "standard" server.



## How to create an image of a server ?

This step should be done time to time to be sure the image correspond to the latest version of our "standard" server.



In order to create an AMI of a server, go to EC2 > Instances



![Texte de remplacement généré par une machine: EC') Dashboard Events Tags Reports Limits v INSTANCES Instances Instance Types Launch Templates ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image28.png){width="1.8645833333333333in" height="2.4166666666666665in"}





Select the server you want to use as AMI and then select Actions > Image > Create Imagea

![Texte de remplacement généré par une machine: Launch Instance Connect u Filter by tags and attributes or searc Name xcalc-win-knime-4.1 becalc-dev (webtool) eucalc-win-knime-3.7 Q eucalc-dev (docker builder) tem elia (powerbi) elia-dev (webtool) becalc-prod (powerbi) Actions Connect Get Windows Password Create Template From Instance Launch More Like This Instance State Instance Settülgs Image Networking CloudWatch Monitoring ca4 3e Instance Type t32xlarge c5xlarge Availability Zone eu-central-lc eu-central-lc Create Image Bundle Instance (instance store AMI) fc8 i-07dad94aa2677e4___ i-08312f17fabd966b6 cs xlarge c5xlarge c5xlarge eu-central-lc eu-central- lb eu-central- lb ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image29.png){width="6.0in" height="2.5833333333333335in"}





Enter the image name, the size and check for no reboot (as below). Then click on "Create Image".

![Texte de remplacement généré par une machine: Create Image Type CU) SSD Add an ESS an ESS of ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image30.png){width="6.84375in" height="3.1979166666666665in"}





## How to create a server with an AMI ?



Go to EC2 > Images > AMIs

![Texte de remplacement généré par une machine: EC2 Dashboard New Events Tags Reports Limits v INSTANCES Instances Instance Types Launch Templates Spot Requests Savings Plans Reserved Instances Dedicated Hosts Capacity Reservations v IMAGES AMIS Bundle Tasks ELASTIC BLOCK STORE ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image31.png){width="1.9791666666666667in" height="4.9375in"}





It gives you all the AMIs you created with their name, the creation date and the AMI ID.

![Texte de remplacement généré par une machine: Name AMI Name becalc-prod-build climact-knime-__ xcalc-linux AMI ID ami-05c481b94997dd4c7 ami-OcbabOb1394144561 ami-0623be33d3d779e1f Source 910931614638".. 910931614638/c_.. 910931614638/x_.. Owner 910931614638 910931614638 910931614638 Visibility Private Private Private Status available available available Creation Date September 4, 2019 at August 20, 2019 at April 30, 2020 at PM_ ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image32.png){width="11.6875in" height="1.6770833333333333in"}

Select the AMI you want to use for creating a new server and then click on Launch

![Texte de remplacement généré par une machine: Launch nedby me v Filter by tags and attributes or search by keyword AMI Name becalc-prod-budd chmact-knime- _ AMI ID ami-05c481b94S97dd4c7 ami-OcbabOb1394144561 9109316146381. 910931614638/c ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image33.png){width="5.15625in" height="2.09375in"}



**Note : when a server is created in this way, there is no need to clone or "xcalc" folder as it is already included in the image. We just have to pull it to be sure to get the latest version.**





## Next steps while creating a new server



### Start and stop Rules

Make sure you add you instance ids in the Start and stop lambda function used in rules if required. Cfr "How to schedule a turn off/on automatically".



### IP address

Each time a server is created, it is assigned to a random IP address. This IP change each time the server is turn off/on. Therefore, it is not easily used while creating a web site.

In order to have a fixed address (also called elastic IP), go to EC2 > Network & Security > Elastic IPs

![Texte de remplacement généré par une machine: NETWORK & SECURITY Security Groups Elastic IPS New Placement Groups Key Pairs New Network Interfaces v LOAD BALANCING ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image34.png){width="1.7083333333333333in" height="2.0729166666666665in"}



Choose "Allocate Elastic IP address"

![Texte de remplacement généré par une machine: Actions v Allocate Elastic IP address ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image35.png){width="4.90625in" height="1.125in"}



![Texte de remplacement généré par une machine: Allocate Elastic IP address Allocate an Elastic IP address by selecting the public IA'4 address pool from which the public IP address is to be allocated. Elastic IP addresses incur charges if they are not associated With a running instance or a network interface that is attached to a running instance. Learn more Elastic IP address settings Public IA'4 address pool Public IP addresses are allocated from Amazon's pool of public IP addresses, from a pool that you own and bring to your account, or from a pool that you own and continue to advertise„ O Amazon's pool of IPv4 addresses Public IPv4 address that you bring to your AWS account(option disabled because no pools found) Learn more Customer owned pool of IPv4 addresses(option disabled because no customer owned pools found) Learn more Cancel Allocate ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image36.png){width="6.84375in" height="3.9166666666666665in"}





At the end, you will have a public IP address associated to your instance ID



![Texte de remplacement généré par une machine: Na me becalc-dev eucalc-dev becalc -Win- knime Public IPW' address 3.120.222.173 3.123.139.87 3.123.171.86 3.125.16.86 3.125.242.214 3.126.21.153 Allocation ID eipalloc-04edaa57577ffdfdc eipalloc-07d46abd4c5978f7b eipalloc-Oe3ddeff5e4608b9d eipalloc-06SeB0489cc40a03 eipalloc-02d2ced791 Sdaa949 eipalloc-Oce40dccSa718S5ef Associated instance ID i-oc0fEsa0f430183e i-095%40fd563c3be7 i-02c1c03867f9d8ca4 i-0096cbb8fc08012e3 i-021 i-ooa4661539b6f6159 ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image37.png){width="7.041666666666667in" height="2.0833333333333335in"}





If you go to EC2 > Instances, you will also find the IP address associated to each instances. If Public IP is in blue, that means it is an Elastic IP. You can click on it to reach the web site".

![Texte de remplacement généré par une machine: Name xcalc-win-knime-4.1 becalc-dev (webtool) eucalc-win-knime-3.7 Q eucalc-dev (docker builder) temp VLAIO elia (powerbi) elia-dev (webtool) Instance ID i-02c1c03867f9d8ca4 i-OcOff33aOf430183e i-0096cbb8fc08012e3 i-OOa4661539b6f6159 i-0213b51c3a8a34fc8 Instance Type t3Qxlarge c5xlarge t3Qxlarge c52xlarge c5xlarge c5xlarge Availability Zone eu-central-lc eu-central- Ic eu-central- lb eu-central- Ic eu-central- Ic eu-central- lb Instance State running running stopped stopped stopped stopped Status Checks 0 2/2 checks 0 2/2 checks Alarm Status ALARM None None None None None Public DNS (IPv4) ec2-3-123-171-86_eu-central-1 _ compute amazonawscom ec2-3-120-222-173. eu-central- 1. compute.amazonaws.com ec2-3-125-16-86.eu-central-1.compute.amazonaws.com ec2-3-126-21-153_eu-central-1 _computeamazonawscom ec2-3-125-242-214. eu-central- 1. compute.amazonaws.com ec2-3-126-41-3.eu-central-1 IPv4 Public IP 3.123171.86 3.120222.173 3.1251686 312621453 3.125242214 3.126 41 3 ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image38.png){width="11.020833333333334in" height="1.53125in"}





**What to do if limitation is reached ?**





### Domain name

By default, the IP address is used to reach a web site but it is cleaner to have a domain name instead of a IP address.

To get a domain name, in the AWS Management Console, look for Route 53.

![Texte de remplacement généré par une machine: AWS Management Console AWS services Find Services You can enter names, kep.vords or acronyms_ Q route 53 Route 53 Scalable DNS and Domain Name Registration ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image39.png){width="4.947916666666667in" height="2.375in"}





Go to Hosted Zones

![Texte de remplacement généré par une machine: Dashboard Hosted zones Health checks Traffic flow Tramc policies Policy records Domains Registered domains Create Hosted Zone Q Search all fields Domain Name netzer02050.ôe. Go to Record Sets All Types Delete Hosted Zone Type • Public Record Set Count 3 Comment HostedZone created by Route53 Registrar ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image40.png){width="9.697916666666666in" height="2.3645833333333335in"}



Then "Create Hosted Zone" to add a domain name to one of your server.

If you click on an already existing hosted zone (here : netzero2050.be), you will find this :
-   Testnetzero2050.be = web link
-   3.120.222.173 = is the IP address of the becalc-dev(webtool) server.

![Texte de remplacement généré par une machine: Back to Hosted Zones Q Record Set Name Create Record Set Import Zone File Aliases Only Œil Delete Record Set Weighted Only Evaluate Target Health Test Record Set Health Check ID Name netzer02050_be_ netzer02050_be_ testnetzer02050 be. Type NS SOA A X Any Type Value ns-1520.awsdns-62.org ns-261 awsdns-32com. ns-2030.avvsdns-61.co ns-802 awsdns-36_net. TTL 172800 900 300 Regior ns-1520.avvsdns-62.org av.'sdns-hostmaster.amazol 3.120.222173 ](../../media/Technical-Documentation-Amazon-Web-Services-(AWS)-image41.png){width="7.083333333333333in" height="2.625in"}





| How to change disk size ? |
|---------------------------|



<https://aws.amazon.com/premiumsupport/knowledge-center/expand-ebs-root-volume-windows/>









































