<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Knime structure](#knime-structure)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Knime structure

Created: 2020-04-15 08:57:42 +0200

Modified: 2020-04-27 10:57:58 +0200

---

|--------------|



The installation of Knime and how to use it have been described in the *User Manual* section. As said in this section, each module is created on local and is not linked to the others. Indeed, data sharing is made by creating csv file in a module and then copy/paste them to another module which need these data as input.

On the web application and on PowerBI, modules need to be interconnected, as the choice of a lever for a specific module influences the output data send to another module. Therefore, using fix csv file between module is not recommended.



In order to make the link between all the modules, an additional module was created in Knime, called _interaction. This module is described in this page.



| Structure |
|-----------|

The module _interactions contains two workflows :
-   **Preprocessing** : takes inputs from Google Sheets, makes some modifications on data and put them in csv files. The preprocessing process was created in order to do as much Knime process as possible outside the web application/Power BI in order to decrease time of calculation when requests are made. All the contents in Knime yellow rocess pboxes (except contents coming from others modules) are preprocessed here.

The csv files are saved in *knime.workspace / input_folder / filename* to be directly available for the processing workflow.

*Knime.workspace, input_folder* and *filename* are workflow variables.

Example :
-   Knime.workspace = C://BECalc/dev (always in dev !)
-   Output_folder = data
-   Filename = ELIA


-   **Processing** : is run when we use the website. It uses the preprocessed csv files to calculate all the process.



| **Note** : when updating data some problems may rise ; the most common is that the module owner did not share some of his metanodes. Therefore, they cannot be updated in the preprocessing (or processing) module. If preprocessing has been run but data are still not updated, check first if metanodes in each modules are properly shared ! |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|





| Create Interface |
|------------------|



How to convert a local module in _interactions

A local module is composed of a series of steps :
-   Import of time series and levers data
-   Lever selections (to work only with a restriction of lever level and avoid huge time of data processing and table size)
-   Import of constants parameters
-   Import of data coming from other module
-   Calculation metanodes (with output for the Pathway Explorer and other modules)



All these steps should be encapsulated separately into shared metanodes to ease the creation of the _interactions module.

For more information about the best practices while createinf a local module, cfr User Manual > Tools > Knime ([Knime](onenote:User%20manual.one#Knime&section-id={e0f60df4-e151-48b5-804a-44e2e564b6c9}&page-id={bace8536-4efd-417d-8a5c-ff019b455b54}&end) ([Affichage web](https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&action=edit&wd=target%28User%20manual.one%7Ce0f60df4-e151-48b5-804a-44e2e564b6c9%2FKnime%7Cbace8536-4efd-417d-8a5c-ff019b455b54%2F%29&wdorigin=703&wdpreservelink=1)))



Once, a local module is created and its metanodes are shared, it can be added to the _interactions module as followed :



1.  Process flow in your module workflow

![In data-preprocessing BE-GIC OTS BFcalc OTS FTS BEcalc Lever selection Use .json instead (in data-processing) Direct link with other module (in data-processing) Input from other modules Merge FTS-OTS BEcalc cp (constant values) In data-processing Calculation metanode ](../../media/Technical-Documentation-Knime-structure-image1.png){width="7.625in" height="3.2083333333333335in"}





2.  Process flow in _interactions workflow

![In data-preprocessing BEcalc OTS BECalC OTS FTS BEcalc FTS BEcalc cp (constant values) csv file csv file file Import lever position (json file) Read csv Merge FTS-OTS Read csv Metanode « DB --- Module Name (not shared but make it more readable) In data-processing Calculation metanode (other modules) Calculation metanode ](../../media/Technical-Documentation-Knime-structure-image2.png){width="7.65625in" height="3.78125in"}





What should be set in data-preprocessing and what should be set in data-processing



Data-preprocessing

Reads Google Sheet, does some modifications to input data tables and saves them in csv files that will be used in data-processing workflow.

In this step, we only run steps that occurs before "lever selection" node and that are not connected to other module.

![In data-preprocessing BECalc OTS BECalc OTS FTS BECalc BECalc CP (constant values) csv file csv file csv file ](../../media/Technical-Documentation-Knime-structure-image3.png){width="3.65625in" height="4.0in"}





Data-processing

Creates link between modules and calculates output. All the steps after lever selections are run here.

Some modifications are done to make the workflow more readable ; metanode created for steps before calculation node (read csv, merge ots and fts, im*port lever position **based on json file***).

![csv file csv file BECa1c interface csv file Import lever position (json file) Read csv Merge FTS-OTS Read csv Metanode « DB --- Module Name » (not shared but make it more readable) In data-processing Calculation metanode (other modules) Calculation metanode ](../../media/Technical-Documentation-Knime-structure-image4.png){width="5.0in" height="4.0625in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p>If not already done, clone _interactions on local.</p><p><strong>Before modify _interactions, pull it to be sure you get the latest version and be sure nobody will work on them while you do your modifications !</strong></p><p><strong>Once a module has been updated/created, make sure this module shared its metanodes and was pushed on remote. Then, pull this module on your local desktop to get the latest version of each metanode.</strong></p><p></p><p>If new lever was created (or old lever was deleted)</p><p>In data-preprocessing</p><p>Levers are imported from Google sheet thanks to this workflow. Usually, a new lever is always added in a shared metanode. Therefore, at this step, you just have to update the metanode linked to the import of levers. A new output port should appear ; link it to a <em>DataWriter (CSV)</em> metanode.</p><p>If a lever is deleted, delete the corresponding DataWriter (CSV)metanode as well.</p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image5.png" style="width:3.53125in;height:5in" alt="Example : new lever AIR OTS FTS Import OTS and FTS for Air QualiW Datawriter (CSV) Export data to specific Excel file Datawriter (CSV) Export data to specific Excel file Datawriter (CSV) Export data to specific Excel file " /></p><p>You shoud also link all your levers to a <em>level data to Excel sheet</em> metanode. This metanode save all the lever in an Excel file that will be read with the API. It specifies which lever should be displayed for the lever decription (more information in data collection Google sheet &gt; metrics_to_names page &gt; tooltip column : <a href="onenote:User%20manual.one#Data%20Collection&amp;section-id={e0f60df4-e151-48b5-804a-44e2e564b6c9}&amp;page-id={49621430-cf95-41ad-9bbf-4513a94f6da5}&amp;end">Data Collection</a> (<a href="https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&amp;action=edit&amp;wd=target%28User%20manual.one%7Ce0f60df4-e151-48b5-804a-44e2e564b6c9%2FData%20Collection%7C49621430-cf95-41ad-9bbf-4513a94f6da5%2F%29&amp;wdorigin=703&amp;wdpreservelink=1">Affichage web</a>)).</p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image6.png" style="width:2.48958in;height:5in" /></p><p></p><p>Inside this metanode, you have a series of level data to Google sheets metanode (one by lever).</p><p><img src="../../media/Technical-Documentation-Knime-structure-image7.png" style="width:7in;height:3in" /></p><p>If you open one of them, you will see they contain a <em>Data Import – CP</em> which import values from the above mentionned metrics_to_names page coming from data collection Google sheet (id of this Google sheet is given in the google_sheets_ids.xlsx file in _common ; module used for that is "parameters"). This is where you have the link between all the levers that are available and the choice made to determine which one is used for lever description.</p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image8.png" style="width:6.5in;height:4.58333in" alt="A Dialog - Optons Flovv Variables Memory P olicy parame ters Constant Sheet Name metrics to names - SEC... Cancel Change Change " /></p><p></p><p></p><p></p><p></p><p></p><p>In data-processing</p><p>If a new lever was created, you should import its csv file (written on the data-preprocessing workflow) into the data-processing workflow.</p><p>Open the DB metanode corresponding to the appropriate module (where lever was created) and read the csv file in the "FTS" section (cfr Figure below). Join this lever table to the other ones (if any) and link it to the OTS data with a Lever selection node (cfr Figure below).</p><p>If a lever was deleted, open the appropriate DB metanode, remove the DataWriter node linked to the deleted lever and change the flow process to make it run (eg. if joiner, remove it).</p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image9.png" style="width:1.65625in;height:1.46875in" alt="- Air Pollution Node 3866 " /></p><p><img src="../../media/Technical-Documentation-Knime-structure-image9.png" style="width:1.65625in;height:1.46875in" alt="- Air Pollution Node 3866 " /></p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image10.png" style="width:5in;height:3.625in" /></p><p></p><p></p><p></p><p></p><p>If an existing model was updated</p><p>In data-preprocessing</p><p>If modifications were linked to the data-preprocessing flow, make sure these modifications are updated on the data-preprocessing workflow (all shared metanodes should be updated).</p><p><em><strong>Note</strong> : for some reasons, modifications are not always done in a metanode. In this case, change it either on the concerned module to set the changes in a metanode (commit/push/pull) or add these modifications manually in the data-preprocessing metanode.</em></p><p></p><p><em>If new table used as input</em> : add the node/metanode that import this data and link it to a "DataWriter (CSV)" metanode.</p><p><em>If old table used as input was deleted</em> : remove both the import metanode (for this table) and the corresponding "DataWriter (CSV)" metanode. <em><strong>Exception</strong> : if the new created table or the old deleted table were created/deleted inside a shared metanode, no need to make any change here (everything is already managed in the shared metanode)</em></p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image11.png" style="width:4.34375in;height:1.55208in" alt="Example : new metanode for new input data AIR_exposu re_other-secto rs Exposure from other s ectors (llASAdata) Datawriter (CSV) Export data to specific Excel file " /></p><p></p><p>In data-processing</p><p>If modifications were linked to the data-processing flow, make sure these modifications are updated on the data-processing workflow (all shared metanodes should be updated).</p><p><em><strong>Note</strong> : for some reasons, modifications are not always done in a shared metanode. In this case, change it either on the concerned module to set the changes in a shared metanode (commit/push/pull) or add these modifications manually in the data-processing metanode.</em></p><p></p><p><strong>Modification in inputs</strong></p><p><em>If new table used as input</em> : open the DB corresponding to the module where modifications were done, read this new table (with <em>DataWriter</em> metanode) and link it to appropriate metanode. These data are then send to the "calculation" metanode as it was already done before (cfr output port of the DB metanode).</p><p><em>If old table used as input was deleted</em> : remove the corresponding DataWriter metanode and do some modificatiosn if needed (eg. If joiner, delete it).</p><p><em><strong>Exception</strong> : if the new created table or the old deleted table were created/deleted inside a shared metanode, no need to make any change here (everything is already managed in the shared metanode and changes impact directly the corresponding csv file)</em></p><p></p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image12.png" style="width:1.4375in;height:1.09375in" alt="DB - Air Pollution " /></p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image13.png" style="width:4.6875in;height:5in" alt="T.&quot; R&quot; to 1231 STS Example : new input table EUcec - EUcec - Link it to appropriate etanode AR OTS " /></p><p></p><p><strong>Modification in outputs</strong></p><p><em>If new output are created (as input for other module)</em> : Make sure your calculation node is properly updated ; new output port should appear.</p><p>Link this output port to appropriate node (other module calculation nodes and/or to a visualisation output)</p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image14.png" style="width:3.09375in;height:3.84375in" alt="isualize out Eucalc - uts 3.10 3 la Industry module Filter alization Eucalc - Graph —alization (WebAp " /></p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image15.png" style="width:5in;height:2.11458in" alt="Visualize outputs Assemble all pathway Explorer output EUCa1c • Graph Visualization (WebApp) " /></p><p></p><p><em>If old output are removed</em> : Make sure your calculation node is properly updated ; an output port should have been deleted.</p><p></p><p></p><p></p><p>If new branch was created</p><p>If not already done, create branch to it (branch should have the same name as the one you give to the other module for your project (eg. VLAIO)).</p><p><strong>In your local folder, you should switch from the master to your new branch (cfr command line "checkout") in each module you have cloned (_interactions included !!!). Once it is done, each time you work with Knime workflow into your project folder, modifications are done on the new branch and not on the master.</strong> If you have a doubt about it, open a command line into the module folder you want to work on and check if you are on the appropriate branch.</p><p></p><p><em>Example : For VLAIO project (in VLAIO folder) ; we work on the vlaio branch (and not the master) for the _interactions module</em></p><p><img src="../../media/Technical-Documentation-Knime-structure-image16.png" style="width:7.77083in;height:2.21875in" alt="Affichage Disque local VLAIO dev Nom interactions Rech azureACA•IaitéJonasa840-5CG9431HGJ MINGWS4 /c/VLAIO/dev/_interaction " /></p><p></p><p></p><p>In data-preprocessing</p><p>Clean it : keep only the modules you need and delete the other ones. If you have to add a new module, cfr following section "If new module was created".</p><p></p><p>In data-processing</p><p>Clean it : remove all the modules that are not needed (should be the same that were deleted in the data-preprocessing).</p><p>Be sure, all the remaining modules are properly connected to each other and there is no input (or output) ports that are not connected.</p><p></p><p><strong>Note</strong> : usually modifications on a module branch should use as much as possible the same shared metanodes than the master branch. Therefore, shared metanodes used in data-preprocessing and data-processing are automatically updated and do not required further modifications at this step.</p><p>Nevertheless, some major changes could be done on a module (eg. adding other shared metanodes). In this case, if new shared metanodes are created/deleted, make sure all these modifications are equally done into the _interactions worflows.</p><p></p><p></p><p></p><p>If new module was created</p><p>In data-preprocessing</p><p>Copy/Paste one of the existing module metanode and set the appropriate name to it (makes the process easier) or create one from scratch.</p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image17.png" style="width:5in;height:3.96875in" alt="Node 398 R POLLUTION Reset (9 OXO Configure... Execute Execute and Open Views Cancel Edit Node Description... New Workflow Annotation Connect selected nodes Disconnect selected nodes Create Metanode... Create Component... Compare Nodes Cut Copy Paste Redo Shift+FIO Alt+F2 Ctrl+L Ctrl+Shift+L Open Expand Reconfigurem Convert to Component Share... Update Link Disconnect Link Ctrl+A1t+U " /></p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image18.png" style="width:4.3125in;height:2.80208in" alt="A Reconfigure Metanode Wizard Specfiy the name of the node and define the number and type of the desired in and out pots. Metanode Name: AIR POLLUTIONI In Ports: Out Ports: Finish Cancel " /></p><p></p><p></p><p>Open this metanode and change the content ; set here all the metanodes that are linked to data importation (constant parameters, OTS/FTS and LL) and write the results in csv files as shown on the Figure below.</p><p></p><p><img src="../../media/Technical-Documentation-Knime-structure-image3.png" style="width:3.42708in;height:3.75in" alt="In data-preprocessing BECalc OTS BECalc OTS FTS BECalc BECalc CP (constant values) csv file csv file csv file " /></p><p></p><p></p><table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>Pay attention there is only one metric by lever that is set to "yes" !</strong></p><p></p><p>Example <em>:</em></p><p></p><p><em>In ll_transport : the lever "freight_modal-share" is composed of several technologies.</em></p><p><img src="../../media/Technical-Documentation-Knime-structure-image19.png" style="width:6.46875in;height:2.96875in" alt="Ion — - lever lever ever lever lever lever lever lever lever lever lever lever lever lever lever lever freight demand reig mo a-s are freight modal-share freight modal-share freight modal-share freight modal-share freight modal-share freight modal-share freight modal-share freight modal-share freight modal-share freight_mcdal-share freight _ technology-share freight technology-share freight technology-share new new new ZEV ZEV ZEV technology share-intraBE-lastmile transitlO HDV transitlO LOV transitlO rail transitlO IWW longdistance_HDV intraBE- 10 ngdistance_LDV intraBE- longdistance_rail intraBE- longdistance_lWW intraBE- intraBE-lastmile HDV intraBE-lastmile LDV intraBE-lastmile bike HOV LOV " /></p><p></p><p><em>In metrics_to_names : Only one technology is set to yes for ""freight_modal-share".</em></p><p><img src="../../media/Technical-Documentation-Knime-structure-image20.png" style="width:8.625in;height:2.65625in" alt="metric fts tra fts tra fts tra fts tra fts tra freight freight fts tra_freight_demand freight frei ht fts freight freight frei ht freight aviation[bn tkm] inland[bn tkm] _maritime[bn_tkml fts share-intraBE-longdistance[%] share-transitlO[%l demand demand demand demand figure_caption Freight aviaton transport demand Freight inland transport demand Freight maritime transport demand Freight transport demand share (last mile) Freight transport demand share (long distance) Freight transport demand share (transit 10) Freight modal share last mile (HDV) Freight modal share last mile (LDV) Freight modal share last mile (bike) Freight modal share long distance (HDV) Freight modal share long distance (IWW) Freight modal share long distance (LDV) Freight modal share long distance (rail) Freight modal share transit 10 (HDV) sector tra tra tra tra tooltip yes intra8E-lastmile intraBE-lastmile fts fts fts fts fts fts moda I-share modal-share tra_freight_modal-share_intraBE-longdistance modal-share tra tra tra tra intraBE-longdistance intra3E•Ion distance " /></p><p></p></th></tr></thead><tbody></tbody></table><p></p><p></p><p>In data-processing</p><p></p><p>Copy/Paste an already existing DB metanodes and change the name (it makes the process easier) or create one from scratch.</p><p>Link it to the API interface (as shown on the Figure 1).</p><p>Inside the DB import the csv files that were written on the data-preprocessing step (Figure 2) and do some other modifications if needed ; OTS and FTS should be merged thanks to a <em>LeverSelection</em> metanode (Figure 2).</p><p>Link the output table coming from the DB metanodes to your module calculation metanode (where all calculations are done) (Figure 1).</p><p>Link the output coming from your module calculation metanode to Graph Visualization metanodes (Figure 3) and/or to other module (if relevant).</p><p>Link your module (input ports) with the other modules you relied one (output ports). Use an <em>Interface Validation</em> metanode before injecting data coming from other modules to your module (Figure 4).This metanode allows us to check if the variable names are the same between input and output variables.</p><p></p><p><em>Figure 1: Interaction betwwen API interface, DB metanode and calculation metanode</em></p><p><img src="../../media/Technical-Documentation-Knime-structure-image21.png" style="width:9.03125in;height:1.75in" alt="Import lever positions A P&#39; interface Import historical and future time series for all levers ROW r DB - Lifestyle Node S03 L fe.tyle Lif&quot;ty• " /></p><p></p><p><em>Figure 2 : Inside a DB metanode</em></p><p><img src="../../media/Technical-Documentation-Knime-structure-image22.png" style="width:8.46875in;height:8.58333in" /></p><p></p><p><em>Figure 3 : Output are linked to Graph Visualization</em></p><p><img src="../../media/Technical-Documentation-Knime-structure-image14.png" style="width:3.625in;height:4.52083in" alt="isualize out Eucalc - uts 3.10 3 la Industry module Filter alization Eucalc - Graph —alization (WebAp " /></p><p></p><p></p><p><em>Figure 4 : Interactions between modules, "Interface Validation" metanodes are used.</em></p><p><img src="../../media/Technical-Documentation-Knime-structure-image23.png" style="width:4.6875in;height:5in" alt="Data co from ot module ng EUCaIc - Interface Validation 21 Buildings EUCaIc - Interface Validation 22 Transport EUCaIc - Interface Validation 6.2 Air pollution 3 la Industry EUCaIc - Interface Validation 31b Industry Ammonia Validation S I Electricity " /></p><p></p></th></tr></thead><tbody></tbody></table>



| Using Interface |
|-----------------|

Before testing _interactions

Always pull _interactions to get the latest version.

Data-processing workflow works with an interface Excel file. If this file is not up to date, data-processing could rise some error and stop running.

To get this file :

1.  Run on local each module in order to update the value in each RAW pages (cfr [Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more details).

**/! This step is particularly important when you have created a new module or when you have modified input or output variable in your module workflow.**

2.  Make sure everything is alright on the interface Google Sheet for your project (eg. BE2050, ELIA, EU2050) ; accuracy should be set at 100% for each module (cfr [Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more details).

3.  Download the Google file

![BE2050 File dit Share New Open Interfaces View Insert Format Data Tools Add-ons Help Last edit was made .00 123. Arial ctrl+0 Import Make a copy Download ally generated from the KNIME code (DON'T UPI Coherence checks Interfaces Data Validation Data Scope Microsoft Excel (.xlsx) OpenDocument format (.ods) Make available offline PDF document (.pdf) Version history Web page (.html, zipped) ](../../media/Technical-Documentation-Knime-structure-image24.png){width="4.15625in" height="2.90625in"}



4.  Save this file to **_interactions / configuration /*project_name*** folder. This file should replace the old one named "interface.xlsx".





Testing _interactions

Data-preprocessing

Run each metanodes.

Note : if some modules were not modified at all in your projects and that they were already runs on local previously, no need to run them again (for saving time purpose).



Data-processing

Run the whole process. Nothing special here.


























