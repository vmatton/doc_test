<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Lever_position.json](#lever_positionjson)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Lever_position.json

Created: 2020-04-15 08:56:46 +0200

Modified: 2020-04-15 08:57:09 +0200

---


![Import lever positions ](../../../media/Technical-Documentation-Lever_position.json-image1.png){width="3.0520833333333335in" height="3.9375in"}

![Component Input Node 500 J SON Reader Node 494 JSON to Table Node 495 Table Row to Variable (deprecated) Component Output Node Node 501 ](../../../media/Technical-Documentation-Lever_position.json-image2.png){width="5.0in" height="1.6770833333333333in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>Modify this file each time modification are done on levers (new lever or delted lever, new module, new branch).</strong></p><p></p><p><strong>The following steps should be done for each project linked to the same calculator (eg. ELIA, BE2050, …)</strong></p><p></p></th></tr></thead><tbody></tbody></table>



**In order to update this file** :

1.  Pull _interaction to be sure you get the latest version

2.  Go to dev > _interactions > configuration

![û xcalc Nom ELIA lever_pasltian.jsan interactions configuration Modifié le 18-03-20 10:18 29-01-20 15:cu 16-03-20 14:52 24-02-20 15:17 29-01-20 15:cu 16-03-20 16:31 25-02-20 16:03 25-02-20 15:55 16-03-20 14:52 Type Dossier Dossier Dossier Dossier Feuille de Fichier JS( Fichier JS( Fichier JS( Fichier JS( ](../../../media/Technical-Documentation-Lever_position.json-image3.png){width="5.0in" height="2.8645833333333335in"}



3.  Open the json file (we recommend to use Sublime Text)

![lever_position.jso Open With Sublime Text Ouvrir avec Code ichier JSO chier JSO chier JSO chier JSO ](../../../media/Technical-Documentation-Lever_position.json-image4.png){width="4.90625in" height="1.03125in"}



4.  Update the file :

***If a lot of modifications were done*** in the levers page of the interface google sheet :

Copy/Paste all levers (lever_name column) from interface and paste them in the [{}] brackets

Apply same format than before :

a.  SHIFT (capital letter) + RIGHT CLICK on the file (allows you to work with columns)

b.  Drawn a line along all the variable (select all the line we want to work on at the same time)

c.  Add **"**

d.  END key (on keyboard) (will go at the end of each line)

e.  Add **":2,** (we set the value 2 to all lever (instead of 3 in the example below) because some lever have only two levels and not four)

f.  At the end on the bracket, the value should not be followed by a ",". Delete it.

!["lever "lever "lever energy energy energy-net -import_efuels" : 3, emission factor non-residential solid-bio" : 3, emission factor residential solid- bio" •3 No dote ! ](../../../media/Technical-Documentation-Lever_position.json-image5.png){width="5.0in" height="1.0in"}



***If just few modifications***, you can do it manually (if easier for you). **Take care to add the lever to the appropriate line (lever order should be the same than in the interface !)**

*Initial format* :

!["lever_pkm" : 3, - share" : 3 "lever_passenger_occupancy" : 3 "lever_passenger_utilization-rate" : 3 "lever floor-intensity" : 3 "lever_heatcool - behaviour_degrees" : 3 "lever _ appliance-use" : 3 "lever_lighting_res" : 3, "lever _ kcal- req" : 3, "lever diet" :3, "lever_paperpack" : 3 , "lever_product- substitution-rate" : 3 "lever fwaste" : 3, "lever _ appliance-own" : 3 , "lever _ freight "lever _ freight "lever _ freight "lever _ freight "lever _ freight "lever _ freight "lever _ freight "lever _ freight "lever _ freight demand " : 3, veh-effici ency_new" : 3 , technology-share technology-share technology -share technology -share modal-share" : 3, load-factor" : 3, utilization- rate" : new new LEV" new ZEV-mix" : new LEV-mix" 3, •3, "lever_passenger_veh-efficiency 3, new" : 3, "lever_passenger_technology-share new ZEV "lever_passenger_technology-share new LEV "lever_passenger_technology-share new ZEV-mix" :3 "lever_passenger_technology-share new LEV-mlx" :3 "lever_passenger_automation" : 3 , "lever "lever "lever "lever "lever "lever "lever "lever "lever "lever "lever electrification" : 3, fuel fuel fuel fuel fuel fuel fuel -mix -mix -mix -mix -mix -mix -mix biofuel- road" : 3, biofuel-marine" : 3 biofuel- aviation" : 3, efuel -road" : 3, efuel -marine" : 3 efuel -1k" : 3, efuel -aviation" : 3, building-renovation-rate residential" 3, building- renovation-mix residential" : 3 building-demolition- rate residential" •3, ](../../../media/Technical-Documentation-Lever_position.json-image6.png){width="5.145833333333333in" height="6.625in"}



5.  Save your file and **commit/push interaction**














