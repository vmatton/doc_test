<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Interface](#interface)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Interface

Created: 2020-04-15 08:55:00 +0200

Modified: 2021-04-01 15:06:25 +0200

---


| Introduction |
|--------------|



This page is used to collects outputs from Knime module. Thanks to the "Update Interface L2" metanode, output data are written in RAW pages (hidden). Based on them, we can check if data table are well constructed and variable/column names are the same between modules.



| Create Interface (or add sheet for new module) |
|------------------------------------------------|



Create a file with a RAW page by module you will have in your calculator (example below)



**File name is** : *project_name*_Interfaces

Where project_name should be the same used across all the google sheets files linked to this project (data collection, data calibration, ...)

*Example* :

![BE2050 Interfaces File Edit View Insert Format ](../../../media/Technical-Documentation-Interface-image1.png){width="3.0208333333333335in" height="0.625in"}





**RAW sheet names are** : *module_number module_name* RAW

Where module_name and module number are defined in the "Update Interface L2" metanode



![A Dialog - Update interface L2 - Googl... Optons Flow Variables Memory P olicy Current Module 6.2 Air Pollution Change OK - Execute Apply Cancel ](../../../media/Technical-Documentation-Interface-image2.png){width="3.03125in" height="2.34375in"}



*Example of RAW pages* :

![1.3 Technology 1.4 Climate Emissions RAW 2.1 Buildings 2.1 Buildings RAW 2.2 Transport RAW 2.2 Transport 2.3 District Heating 2.3 District Heating RAW Dashboard • Interface m ](../../../media/Technical-Documentation-Interface-image3.png){width="3.3645833333333335in" height="3.3125in"}







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>If your module name is not in the <em>Update Interface L2</em> metanode</strong></p><p></p><p></p><p>Currently, available module names are :</p><p></p><p>0.2 Pathway Explorer</p><p>1.1 Lifestyle</p><p>1.2 Technology</p><p>2.1 Buildings</p><p>2.2 Transport</p><p>3.1a Industry</p><p>3.1b Industry Ammonia</p><p>3.1c Industry Power</p><p>4.1 Land-use</p><p>4.2 Minerals</p><p>4.3 Agriculture</p><p>4.4 Water</p><p>5.1 Electricity</p><p>5.2 Oil Refinery</p><p>6.2 Air Pollution</p><p>6.4 Employment</p><p></p><p></p><p>If you are creating a new module or if for some reasons your module name is not in the above list, the Update Interface L2 metanode should be updated.</p><p></p><p>To do so :</p><ol class="incremental" type="a"><li><p>Be sure you pulled the _common library and that nobody is working on it at the same time</p></li><li><p>Open the Update Interface L2 metanode (after disconnecting the link / unsharing it)</p></li><li><p>Double click on the "" node to open its parameters and add your module name in the "Possible Choices" box.</p></li><li><p>Save the metanode and share it again</p></li><li><p>Do not forget to commit and push your changes</p></li></ol><p></p><p></p><p><img src="../../../media/Technical-Documentation-Interface-image4.png" style="width:6.03125in;height:5.64583in" alt="Single Selection Memor y P olicy Curr ent Module Name of Che Single Selection Node 395 String Man Node 1 nipula (Variable) Update interface L2 - column creation Pathway explorer Update interface L2 - column creation A Dialog - Control Variable Name: Parameter Name: Selection Type : Possible Choices: Default Value : cur I enc mcdul module name current single -selecton Dr opdo vvn 0.2 Pathway Explorer 1.1 Lifestyle 1.2 Technology 2.1 Buildings O. 2 Explorer I. I Lifestyle I. 2 Technology 2. I Buildings 2.2 Transport Limit number of visible options: Number of visible options: Value overwritten by dialog, current value: 6.2 Air Pollution Apply Cancel " /></p><p></p><p></p></th></tr></thead><tbody></tbody></table>





In addition to RAW page, create another page by module

The name of this page should be the same than the RAW page except that there is no "RAW" suffix.

*Example* :

![1.3 Technology 1.4 Climate Emissions RAW 2.1 Buildings 2.1 Buildings RAW 2.2 Transport RAW ](../../../media/Technical-Documentation-Interface-image5.png){width="2.34375in" height="1.78125in"}



Complete each of the sheet create on the step (2)

Before doing this step, make sure the RAW sheets contain data. If not, you should run in Knime each of the module for which no data are available in RAW sheet (this module should all have a "Update Interface L2" metanode to fill the RAW sheet with their data !).



**First line : show accuracy (% of metrics that are properly connected to other modules). Should be at 100% !**



Each sheet should have columns for :
-   Levers (if there are some levers in the module)
-   Input (if the module receive data from other modules)
-   Output (if the module gives data to other modules) - with at least a 0.2 Pathway Explorer column (if some results are used in the web application/PowerBI)



*Examples* :

![Accuracy: 100 LEVERS 100% INPUT Naming convention 2.1 Buildings Years bld_emissions-total-C021Mt] bld_em issions-total-CH4 [Mtl 100% 2.2 Transport Years tra_freight_emissions-CH4 tra_freight_emissions-CH4 HDV[Mt1 IWWIMt] 3.1a Industry Years ind_emissions-captured ind_emissions-captured ](../../../media/Technical-Documentation-Interface-image6.png){width="7.5in" height="1.3125in"}



![OUTPUT 1.3 Technology costs Country Yea rs Ots tec price-indices 1.3 Fuel costs Years Country 3.1a Industry ind_energy_specificæxcl-feedstock ind_energy_specificæxcl-feedstock aluminium aluminium aluminium ](../../../media/Technical-Documentation-Interface-image7.png){width="7.541666666666667in" height="1.5625in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>How to fill pages ?</strong></p><p></p><p><strong>Example for Electricity</strong></p><p></p><p><strong><u>In Electricity page</u></strong></p><p></p><p><strong>Column "Levers" :</strong></p><p>On line 3 : Add "Naming Convention"</p><p></p><p><strong>Columns "Inputs"</strong></p><p>On line 3 : Add the name of modules you use as input (pay attention to the name, cfr step (1) for more details)</p><p></p><p><strong>Columns " Ouput"</strong></p><p>On line 3 : Add the name of modules you send data (pay attention to the name, cfr step (1) for more details)</p><p></p><p></p><p><strong>Visible Columns</strong></p><p>=INDEX('5.1 Electricity RAW'!$A:$Z,ROW(A2),MATCH(A$3,'5.1 Electricity RAW'!$1:$1,0))</p><p></p><p>This equation looks into all the cells from the RAW sheet what is the cell corresponding to the lign "ROW(A2)" and the column defined by the match function.</p><p>Match function returns the index number of the column whose the name (set in lign 1 ($1:$1) in RAW) is the same than the column name you are working on (here A$3).</p><p></p><p>==&gt; Change name of the sheet according of the module you are working on (cfr text in orange and red)</p><p>==&gt; Change the cell number (+1 when going to the next lign of your Google Sheet) (cfr value in the ROW function)</p><p>==&gt; Change the cell letter according to the column letter you are working on (cfr text in yellow)</p><p></p><p></p><p><strong>Hidden Columns</strong></p><p>These columns are used to define the text color of visible columns.</p><p></p><p><em>For levers :</em></p><p>=IF(OR(ISBLANK(A4),A4=""),"",IFERROR(MATCH(A4,levers_pathwayexplorer,0)&gt;0,FALSE))</p><p><em>For other modules (input or output) :</em></p><p>=IF(OR(ISBLANK(C4),C4=""),"",IFERROR(MATCH(C4,'1.1 Lifestyle'!$W$4:$W$33,0)&gt;0,FALSE))</p><p><em>For 0.2 Pathway Explorer output :</em></p><p>=IF(OR(ISBLANK(U4),U4=""),"",IFERROR(MATCH(U4,'0.2 Pathway Explorer'!$A:$A,0)&gt;0,FALSE))</p><p></p><p>To be changed :</p><ul class="incremental"><li><p>All the cell number (in orange) : should correspond to the cell we want to color</p></li><li><p>For "others modules" : value in yellow : as we want to test the match between variable names of two modules (are the output of one module the same than the input of the other module ?), you should set here the module page and the corresponding column for the test.</p></li></ul><p><em>Example</em> : we are in Electricity. This module receives data from Lifestyle.</p><ul class="incremental"><li><p>In Lifestyle page, there is a column (W) for OUPUT for Electricity.</p></li><li><p>In Electricity page, there is a column (C) for INPUT coming from Lifestyle</p></li></ul><p>We will check if the variable name in C4 exist in the Output – Electricity list in Lifestyle.</p><p></p><p></p><p></p><p></p><p><em>Levers_pathwayexplorer</em> is a named range defined this way (page Levers : J2 =&gt; J155) :</p><ol class="incremental" type="1"><li><p>Go to the Google Sheet menu &gt; Data &gt; Named Ranges</p></li></ol><blockquote><p><img src="../../../media/Technical-Documentation-Interface-image8.png" style="width:2in;height:5in" alt="ata ools Add-ons Help Last Sort sheet by column B, A Z Sort sheet by column B, Z A Sort the range Y Create a filter Filter views Slicer Data validation Pivot table Randomise range Named ranges New Protected sheets and ranges Split text to columns Remove duplicates Trim whitespace Group Ungroup Alt+Shift+* Alt+Shift+e " /></p><p></p></blockquote><p></p><ol class="incremental" start="2" type="1"><li><p>Named ranges open on the right side of your window. Then you can add a new range or visualize/modify an old one.</p></li></ol><blockquote><p><img src="../../../media/Technical-Documentation-Interface-image9.png" style="width:1.75in;height:5in" alt="Named ranges &#39;23 District Heating&#39;!A4:A51 levers_31a_industry &#39;3 la levers_31b_industryammonia &#39;3.1b Industry levers_31 c_industry &#39;3.1 c Industry levers_41_landuse levers_42_Minerals levers_43_agriculture &#39;4.3 levers_44_water levers_45_biodiversity &#39;4.5 Biodiversity&#39; levers_51 _el ectricity levers_52_oilrafinery Levers_backup!12:1155 levers_pathwayexplorer x " /></p><p></p></blockquote><p></p><p></p><p></p><p></p><p><strong>Accuracy</strong></p><p>It helps us to qhickly determine if there is a mismatch in variable name between to modules.</p><p></p><p><em><strong>General formula</strong></em> :</p><p>="Accuracy: "&amp;ROUND(average(B1:AF1)*100,0)&amp;"%"</p><p>It calculates the average of each module accuracy</p><p></p><p><img src="../../../media/Technical-Documentation-Interface-image10.png" style="width:1.40625in;height:0.95833in" alt="Accuracy: 100% LEVERS Naming convention " /></p><p></p><p></p><p><em><strong>Formula by module</strong></em> :</p><p>=countif(D4:D928,TRUE)/(countif(D4:D928,TRUE)+countif(D4:D928,FALSE))</p><p>It calculates who many variable name are the same between two modules compared to the total number of variables shared between these two modules.</p><p>To be changed : in orange ; cells refer to the "check column" of each module. Therefore, you should have a formula by check column (in input and output sections)</p><p></p><p><img src="../../../media/Technical-Documentation-Interface-image11.png" style="width:5in;height:0.8125in" alt="INPUT 2.1 Buildings Years 100% 2.2 Transport Years 100% 3.1a Inc Years " /></p><p></p><p></p><p></p><p><strong><u>In other modules used as input and output for Electricity</u></strong></p><p></p><p>Add two columns in google page for each module Electricity relied on. One is visible and the other one will be hidden :</p><p>The way this column are calculated is the same that what was described for "Electricity" except that we compare each module to Electricity here (&lt;&gt; compare Electricity to each module). For more details about them, refer to the previous sections.</p><p></p><p><strong>Visible Columns</strong></p><p>=INDEX('1.1 Lifestyle RAW'!$A:$Z,ROW(W2),MATCH(W$3,'1.1 Lifestyle RAW'!$1:$1,0))</p><p></p><p>In the above formula, column W in Lifestyle page refer to the Eletricity (Output) section</p><p><img src="../../../media/Technical-Documentation-Interface-image12.png" style="width:2.90625in;height:2.21875in" alt="100% Ifs el-servers TWhl Country " /></p><p></p><p><strong>Hidden Columns</strong></p><p>=IF(OR(ISBLANK(W4),W4=""),"",IFERROR(MATCH(W4,'5.1 Electricity'!$C$4:$C$133,0)&gt;0,0&gt;0))</p><p></p><p>In the above formula, '5.1 Electricity'!$C$4:$C$133 corresponds to the INPUT – Lifestyle and column W correspond to the OUTPUT - Electricity in Lifestyle</p><p></p><p></p><p><strong>Tips</strong></p><p></p><p>Use the "Paint format" to copy paste a formula from one cell to another one ;</p><p></p><p><img src="../../../media/Technical-Documentation-Interface-image13.png" style="width:1.75in;height:1.17708in" alt="File Edit View Insert 100% " /></p><p></p><p></p></th></tr></thead><tbody></tbody></table>





| Using Interface |
|-----------------|



Dashboard and Interface matrix

These two pages give a summary of the accuracy check : **all values should be at 100% !** If not the case, go to each module pages to see where the problem comes from (which module ? Which variable name ? ...).

These page also represents the execution time of each module (in Knime and with Python) and set if the documentation is available (for Knime and on OneNote).



<table><colgroup><col style="width: 92%" /><col style="width: 7%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>WHAT TO DO ?</strong></p><p>Check if accuracy is set at 100% (if not, fix it).</p><p>Set the documentation of OneNote to 0% to all sector if new project. Change it manually to 100% once it has been done.</p><p></p></th><th></th></tr></thead><tbody></tbody></table>



![How to update update the interfaces in the KNIME (not in this google sheet which is automatically generated from the Knime Before Modifying pur KNIME interfaces, inform the sending/receiving end so he can adapt too (we can only update the general model once both ends are updated) Once a module is pushed (by you) on the GIT, the coherence check for interfaces is run Once a module is consolidated (by Climact) in the general model, the coherence check for values is run How to use First aimn to have 100% next to your module (this means all your interfaces match with how others see them) hen, ensure, you have green elements in the other modules, for what relates to your module ](../../../media/Technical-Documentation-Interface-image14.png){width="9.083333333333334in" height="2.03125in"}



**These pages are automatically updated, no need to change anything here !**





0.2 Pathway to Explorer (called "le Cube")

**This page is crucial to make the API / PowerBi works !** It is a **clean** version of what we want to do on the web application and PowerBI. If a metric is not in the Pathway Explorer page it would not be able to use it in the web app/PowerBI.

To keep it clean, we decided not to automate its update. Therefore, some actions need to be done manually.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If new metrics were added to a module and we want to use them in the web app/PowerBI</strong> :</p><p>Go to the module page in the OUPUT – 0.2 Pathway Explorer column and copy/paste new metrics into the Pathway Explorer page (column A, named "column").</p><p>Normally, new metrics should appear in red in your module page (as they are not in the Pathway Explorer page yet)</p><p></p><p></p><p><strong>If new module was created</strong> :</p><p>Go to the module page in the OUPUT – 0.2 Pathway Explorer column and copy/paste all your metrics into the Pathway Explorer page (column A, named "column").</p><p>Normally, all the metrics should appear in red in your module page (as they are not in the Pathway Explorer page yet). But sometimes, when you used same metrics than other modules, they could be already set in the Pathway Explorer page. In this case, just copy/paste the metrics that are not already in the Pathway Explorer page (metrics in red only).</p><p></p><p>In the "check" column formula, add the Ouput – 0.2 Pathway Explorer row from your new module, as done below :</p><p>=IF(OR(ISBLANK(A6),A6=""),"",IFERROR(MATCH(A6,{'1.1 Lifestyle'!C:C;'2.1 Buildings'!I:I;'2.3 District Heating'!I:I;'2.2 Transport'!E:E;'3.1a Industry'!M:M;'3.1b Industry Ammonia'!I:I;'4.1 Land-use'!G:G;'4.2 Minerals'!G:G;'4.3 Agriculture'!O:O;'4.4 Water'!O:O;'5.1 Electricity'!U:U;'3.2 CCUS'!K:K;'1.4 Climate Emissions'!S:S;'6.2 Air Pollution'!Q:Q;'4.6 Bioenergy Balance'!I:I},0)&gt;0,0&gt;0))</p><p></p><p></p><p><strong>For both new metrics and new module</strong> :</p><p>Create aggregated metrics if required (cfr columns <em>list_of_ids_to_aggregate</em> and <em>aggregation_id</em>). Note : thses cells should be highlighted in grey and text should be in black (to spot them easily and not confuse them with errors).</p><p></p><p>Add the content for other columns (cfr columns <em>table_type</em> and <em>display_name</em>)</p><p></p><p></p><p><strong>Check : is there some metrics that are defined in the Pathway Explorer page but that do not exist in the module outputs</strong> (<strong>metrics in red</strong> in the column A named "column") ? If there is some, fix it (exceptions exist, cfr note below) !</p><p></p><p><strong>Note</strong> :</p><p>Exception is made for two modules that are created in the data-processing (_interactions module) but do not exist as "independent" module (as it is the case for the other modules). As these two modules are only created on data-processing, no data are send from them to a RAW page. The metric names were just copied/pasted from Knime table to the Pathway Explorer google sheet. Therefore, their metrics always appear in red, but it should not be considered as an error !</p><p><em>This is the case for the sector "tor" (metrics are : energy-demand-…) and "rgy" (metric energy-sankey...).</em> These metrics are highlighted in yellow.</p><p></p></th></tr></thead><tbody></tbody></table>





**Description of each columns** :


-   **Column** : contains all the metrics available in Knime. These metrics are copied/pasted from each module sheet (cfr column Output - 0.2 Pathway Explorer in each module)

![Named ranges '23 District Heating'!A4:A51 levers_31a_industry '3 la levers_31b_industryammonia '3.1b Industry levers_31 c_industry '3.1 c Industry levers_41_landuse levers_42_Minerals levers_43_agriculture '4.3 levers_44_water levers_45_biodiversity '4.5 Biodiversity' levers_51 _el ectricity levers_52_oilrafinery Levers_backup!12:1155 levers_pathwayexplorer x ](../../../media/Technical-Documentation-Interface-image9.png){width="1.75in" height="5.0in"}


-   **Check** : check if the name set in column exist in output coming from modules.

Note : as the Output - 0.2 Pathway Explorer column is not in the same letter in each module, check the equation in "check" is properly written if new module are created and added.

When check is TRUE, cell of "column" is written in green, otherwise in red.


-   **List_of_ids_to_aggregate** and **Aggregation_id** : allows us to create new metrics that are a **sum** of others metrics (note : only sum are available to aggregate metrics together).

Example : I want all my metrics relative to meat and fish to be grouped in a unique metric. I set an id (in column aggregation_id) to the metrics I want to group (eg. Id is 1004). And I precise in the metric I created (eg. Lfs_fwaste_meat-fish) that it is an aggregation of other metric (eg. List_of_id_to_aggregate = 1004 : takes all the metrics with aggregation_id set to 1004 and merge them).

If you want to merge different id together use ";" between id to be merged (eg. : 1450;1500;1600).



![](../../../media/Technical-Documentation-Interface-image15.png){width="6.864583333333333in" height="3.5625in"}


-   **Table_type** : defines to which time series the metric is related to. It allows to aggregate tables that have the same time series (eg. From 2000 to 2050, from 1990 to 2050, ...).

**Pay attention** : if your metric do not have the same time series than other metrics (not the same time step, start year and end year), create a new table_type name.

*Example* : sankey_diagram : format is completely different from the timeseries_2000_2050.


-   **Metric and sector** : are autogenerated (cells in orange) : set the metric name and the abbreviation name of the module from which the metric comes from.


-   **Display_name** : name we want to display in the webtool / PowerBI. It makes the the full Knime metric name more readable (eg. Agr_emissions-CH4_crop_rice[MtCO2e] ==> emissions-CH4).





Levers

This sheet allow us to define the levers, how they should appears (on the website) and what to do with them.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If new lever was added to a module or new module was created</strong> :</p><p></p><ol class="incremental" type="1"><li><p>Decide where you want your lever to appear on the web application (in which categories) ? It will determine the values of the columns named headline, group_1 and group_2.</p></li></ol><blockquote><p>Try as much as possible to keep the current structure before adding new headline and group names in the web application.</p><p><em>Cfr description of these columns below.</em></p></blockquote><p></p><ol class="incremental" start="2" type="1"><li><p>Add your lever(s) in the appropriate line (try to keep the same headline/group_1/group_2 in a block and not scattered on the page). Fill the headline, group_1, group_2 and title cell.</p></li></ol><blockquote><p><em>Cfr description of title column below.</em></p></blockquote><p></p><ol class="incremental" start="3" type="1"><li><p>Add the module name from which the lever comes from (cfr column E (first column in "module" section"))</p></li></ol><blockquote><p>If the named ranges (cfr name in the G column) for your module does not already exist, create it.</p><p>Cfr <em>columns in the "module" section</em> for more information about it.</p></blockquote><p></p><ol class="incremental" start="4" type="1"><li><p>Add the lever name (= Knime name of the lever, found in the Lever column of each module page) in the column "code"</p></li></ol><p></p><ol class="incremental" start="5" type="1"><li><p>Add lever parameters and definition in columns range and level_1 to level_4.</p></li></ol><blockquote><p><em>Cfr description of these columns below</em></p></blockquote><p></p><blockquote><p><strong>Do not change the # column before changing your other lever files ! Otherwise, it makes the task more difficult !</strong></p></blockquote><p></p><p><strong>If a lever was deleted from a module (it does not exist anymore) :</strong></p><p></p><ol class="incremental" type="1"><li><p>Just select the line corresponding to the lever that was deleted in your module and delete it</p></li></ol><p></p><blockquote><p><strong>Do not change the # column before changing your other lever files ! Otherwise, it makes the task more difficult !</strong></p></blockquote><p></p><p><strong>For both, deleted and added levers :</strong></p><p></p><ol class="incremental" type="1"><li><p>Go to the Scenario Builder Google Sheet and apply the same change you did in this section. As the # column was not changed yet, it is easier to see where you should add or remove a lever. <strong>The lever position should be the same in each file ! Therefore, be sure you applied the modifications at the same place in each file related to levers !</strong></p></li></ol><blockquote><p>For more information : cfr <a href="onenote:#Scenario%20Builder&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={E0C26574-D2CA-4B89-B165-ADED2D317096}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Scenario Builder</a></p></blockquote><p></p><ol class="incremental" start="2" type="1"><li><p>Go to the lever_position.json file in _interaction / configuration and apply the same change you did in this section. <strong>Same comments than for the Scenario builder are applied here !</strong></p></li></ol><blockquote><p>For more information : cfr <a href="onenote:#Lever_position.json&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={E8945928-71D4-43AA-A17B-E6EC07067912}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Lever_position.json</a></p></blockquote><p></p><ol class="incremental" start="3" type="1"><li><p>Once it is done, change the # column (should start at zero and be incremented by one one each additional line). Apply these modifications (on # column) to all others files linked to it (cfr Scenario Builder).</p></li></ol><p></p><p>Note : this step should be done for the Google Sheet (interface and scenario builder) of each other project related to the same calculator than yours (eg. If you are working on XCalc, you should change it in ELIA, BE2050, EU2050, …), otherwise, your levers will not be displayed on the web application of these projects and could lead to some problems while running !</p><p></p></th></tr></thead><tbody></tbody></table>





**Description of each columns** :
-   **The first four columns**

They define de way the levers are displayed on the web application (headline, group_1, group_2, title) :

!["OUP_ 2 Transport Transport Transport Transport Transport Transport 十 reight ransport reight ransport reight ransport reight ransport reight ransport reight title nergy ffciency echnology hareZEV echnology hare V-m ix hn 引 0 hare EV-mix Freight Explore > Key behaviours Transport Freight Transport Ener Efficien Freight mode Technology evolution 囗 囙 囙 echnology evolution echnology evolution echnology evolution echnology evolution echnology share 囯 囗 囙 囙 EV Technology share 囯 囗 囙 囙 LEV-mlX Vehicles utilization 囗 囙 囙 and load fa 0 「 囗 囙 囙 囙 > passengerTransport 1 囗 囙 囙 囙 > Technology and fuels 1 > Buildings > Manufacturing 囗 囙 囙 囙 > Power production 1 Agriculture, forestß/ >and land use (AFOLU) ](../../../media/Technical-Documentation-Interface-image16.png){width="6.770833333333333in" height="6.114583333333333in"}






-   **Columns in the "module" section**

![module .1 lifestyle 11 lifestyle Automatic levers 11 lifestyle calculation ](../../../media/Technical-Documentation-Interface-image17.png){width="4.21875in" height="1.46875in"}


-   First column : write the name of the module (the same as the module sheet name)
-   Second and third columns : automatic calculation to change the module name ;



***Note*** : The third column correspond to a named range. This name ranged point to the levers column of each module page. If this named range does not already exist for your module, create it.

***Eg. levers_11_lifestyle** named ranges correspond to the column A (Levers) of the sheet called 1.1 Lifestyle*

![Lifestyle RAW' Accuracy: 100% Naming convention 100% % .0 .00 123. -INDEX( 1 .1 Lifestyle RAW' Ifs fwaste_beer[kcal Named ranges '1.1 Lif style'!AA Cancel '1.1 Life '1.4 Climate x Done OUTPUT 0.2 Pathway Explorer ever ever ever ever _ pap urb o kcal-req diet fwaste LEVER NAME LIST Years Ifs fwaste afats[kcall Ifs fwaste_bev-alc al] Ifs fwaste 1.1 Lifestyle . bev- Emissions'!levers_12_Climate '1 _4 Climate levers_12_Climate 0.2 Pathway Explorer r[kcal) 1.2 c Explore ](../../../media/Technical-Documentation-Interface-image18.png){width="6.65625in" height="3.03125in"}




-   **Column "code"**

This column set the code name of the lever. This code name should be the same that is found on the levers column of the corresponding module.

Check are available : it compare the given code name (cfr column code) with all the code name available in the lever list of the corresponding model :
-   Column "lever_name" : automatically calculated (= value set in "code")
-   Column "check" : compare the value in "lever_name" to a list of all the available levers coming from the module defined in the first dolumn into the "module" section. If the code_name is in the list, return TRUE, else FALSE.

**Note** : when check is set to "TRUE", "code" cell are written in green, otherwise in red

**Note** : Formula uses "indirect("Gx")" : that looks into column named like it is set in cell "Gx". Eg. Cell G17 = Levers_11_lifestyle ==> look in the cell range defined in the named ranges, cfr columns in the "module" section for more information about it.



![WIRECT(G2 group_ headline Kev behaviours Kev behaviours group_l Travel Travel 2 title Passenger levers distance Mode of levers transport 11 22 lifestyle transport lever_pkm lekr_pkm lever _ passenger modal-share check TRUE TRUE display_range ](../../../media/Technical-Documentation-Interface-image19.png){width="10.03125in" height="2.1145833333333335in"}


-   **Column "range"**

This column defines what values are set for each lever :
-   [1,2,3,4] : numeric value from 1 to 4
-   [1,2] : numeric values from 1 to 2
-   [A,B,C,D] : categorical value with 4 possibilities : A to D
-   [A,B] : categorical value with 4 possibilities : A and B


-   **The last columns named "level 1" to "level 4"**

They contain text that pop up when you set cursor on a lever number and the column named "lever definition" give us the lever definition when the cursor is on the lever name (cfr column title).



Graphs

This page is crucial to make the API / PowerBi works !

It defines webtool parameters : title of each graph, which metrics are used, ... ?



<table><colgroup><col style="width: 91%" /><col style="width: 8%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If you want to add a graph</strong> :</p><p>Add new columns as described below. Just make sure your metrics exist into the Pathway Explorer sheet.</p><p></p><p><strong>If you want to delete a graph</strong> :</p><p>Delete the column corresponding to this graph</p><p></p><p><strong>If you want to change the metrics, the position of the graph, and so on</strong> :</p><p>Cfr description below</p><p></p></th><th></th></tr></thead><tbody></tbody></table>



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p>For each graph, several parameters can be set :</p><p></p><ul class="incremental"><li><p>I which menu and submenu it appears</p></li><li><p>What is the title of the graph ? (cfr Name)</p></li><li><p>What is the unit of the metrics ?</p></li><li><p>Is the graph set as area/line (graphType) and is it stacked or not (graphStacked)</p></li><li><p>Which metrics should be drawn ?</p></li></ul><p></p><p>To make the link between Knime metrics name and legend metric name, go to the 0.2 Pathway Explorer Sheet :</p><ul class="incremental"><li><p>Knime metrics are set in "column"</p></li><li><p>Legend name are set in "display_name".</p></li></ul><p></p><p>In the "Graphs" sheet : use only Knime metrics. Link with legend name is automatically done (on the webtool) thanks to the 0.2 Pathway Explorer sheet.</p><p></p><p>Metrics are written in green when they fit the output that are set in Pathway Explorer, in red otherwise (cfr hidden column A ==&gt; take all the values of Pathway Explorer ; if in red and in Pathway Explorer, check if equation used in A is properly set. Up to now, takes values from A4 to A3061 ==&gt; If more than 3061 metrics in Pathway Explorer could rise up problems that are not real ones !)</p><p></p><p></p><p><strong>Example</strong> :</p><p><em><strong>Graphs sheet</strong></em> :</p><p><img src="../../../media/Technical-Documentation-Interface-image20.png" style="width:7.3125in;height:4.64583in" alt="item Menu ubmenu Posit on Name Unit raphType raphStacke d 1. Ene and Emissions a. GHG Emissions GHG emissions by sector MtC02e a rea true ra h 2 1. Ene and Emissions a. GHG Emissions bottom GHG emissions captured MtC02e a rea true Metric Metric Metric Metric Metric Metric Metric Metric Metric Metric Metric Metric Metric Metric Metric _domestc[MtC02e] BECCSIMtC02e] BECCS[MtC02e] _domestc[MtC02e] _domestc[MtC02e] _domestc[MtC02e) _domestc[MtC02e) _domestc[MtC02e) _domestc[MtC02e) elc ind ref efu emissions-C02 ccs[MtC02e] emissions-GHG cc[MtC02e] emissions-C02 ccs[MtC02e) emissions-C02-captured[MtC02e) ra h 3 1. Ene and Emissions b. Energy Consumption Final energy consumption by sector TWh a rea true energy-demand-by-sector_agr[TWhl energy-clema nd-by-sector bld[TWh] energy-demand-by-sector_dhg[TWh) energy-demand-by-sector ind[TWh] energy-demand-by-sector Ifs[TWh] energy-demand-by-sector_tra[TWh] 2 3 4 5 6 7 8 s 10 11 12 13 14 15 lus elc ind agr_ elc ind fos tra bld emissions-GHG emissions-GHG emissions-GHG emissions-GHG emissions-GHG emissions-GHG emissions-GHG emissions-GHG emissions-GHG dhg_emissions-GHG_domesticlMtC02e] was_emissions-GHG_domestc[MtC02e] total_emissions-GHG_domestic[MtC02el " /></p><p></p><p></p><p><strong>Future improvements</strong> : after graphStacked, add new lines with properties you want (ex. Colors). These properties are set by graph.</p><p></p><p></p><p><em><strong>Link between Legend name and Knime metric name (cfr 0.2 Pathway Explorer sheet)</strong></em> :</p><p><img src="../../../media/Technical-Documentation-Interface-image21.png" style="width:10.21875in;height:0.84375in" alt="column lus_emissions-GHG domestic[MtC02e] was emissions-CH4CMtC02e) check list TRUE of ids to_aggregate aggregation id 1416 table_type timeseries 2000 2050 metric emissions-GHG emissions-CH4 was " /></p><p></p><p></p><p><em><strong>Results on the website</strong></em> :</p><p><img src="../../../media/Technical-Documentation-Interface-image22.png" style="width:8.28125in;height:5.21875in" alt="1 . Energy and Emissions B. CHG Emissions SUBMENU a. GHG Emissions b. Energy Consumption 1990 emissions 2015 emissions UNIT 80 -80% target -95% target 2. Activity data 2025 3. Buildings 2030 MENU 4. Tra nsport 5. Power POSITION : Top 2000 2005 2010 2015 2020 2035 Belgium 2045 GHG emissions captured NAME -1 3877787807814457e-17 -0.04400000000000001 -0.08800000000000001 total waste and others district heating buildings transport oil refineries industry power agriculture industry power (BECCS) land-use 2050 METRICS direct air capture (efuels) oil refineries (CC) industry electricity (CC) POSITION -0 132 -0 176 : Bottom -0.22 " /></p><p></p><p></p></th></tr></thead><tbody></tbody></table>




























