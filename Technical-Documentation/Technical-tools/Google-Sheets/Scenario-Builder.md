<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Scenario Builder](#scenario-builder)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Scenario Builder

Created: 2020-04-15 08:55:50 +0200

Modified: 2020-06-24 14:53:34 +0200

---


| Introduction |
|--------------|

This Google Excel file is only useful for PowerBI. It defines a set of scenarii (levers are fixed and predefined here) to be displayed on PowerBI.

Indeed, PowerBi is not responsible as the web application is. You cannot select a given lever and change is level to get new results.



![Texte de remplacement généré par une machine: My Drive Name > CLIMACT > BE2050 > scenarios builder BECalc - scenarios builder - federal and regions BECalc - scenarios builder - federal and regions - All_l .json BECalc - scenarios builder - federal and regions - All_2.json ](../../../media/Technical-Documentation-Scenario-Builder-image1.png){width="5.4375in" height="1.9375in"}



| Create Scenario Builder |
|-------------------------|

The easiest way to create a Scenario Builder is to copy/paste one from an existing project and then modify it to display what you want.

Definition of each pages is set in the "Using Scenario Builder" section.



| Using Scenario Builder |
|------------------------|



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>Take care :</strong></p><p><strong>As many of the lines are linked to a formula, we recommend to copy/paste only the value and not the formula ! Make sure, once you have done with your modifications, each formula make sense !</strong></p><p></p></th></tr></thead><tbody></tbody></table>



Scenario Builder is composed of several pages containing :
-   A list of levers and their level for each scenario
-   A description of each lever : **Levers_description : not used anymore !**
-   Which are the metrics we want to display
-   Which are the lever we want to display
-   A series of pages ; one by scenario



All these pages are described below.

![Levers Levers description draft Levers description param Variables variable list Reference ](../../../media/Technical-Documentation-Scenario-Builder-image2.png){width="10.458333333333334in" height="0.4375in"}







Levers

This page contains all the levers that are available in each workflow. To get them, go to the Interface Google sheet (cfr [Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)) and check if the **column "code"** are the same between both google sheet. If no, check were are the differences and apply them from the Interface to the Scenario Builder Google Sheet. **Take care : lever order should be the same between these two files !** Help you with the "#"column to check differences.



Fill the **headline/group_1/group_2/title** columns as it is set in the Interface Google sheet. For more information about it : cfr [Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one).



In scenario0, set the value range for the lever ;
-   [1, 2] and [1,2,3,4] : respectively 2 and 4 levels (continuous)
-   [A,B] and [A,B,C,D] : respectively 2 and 4 levels (categorical)



Then fill **each column corresponding to a scenario**. The level of each levers depends on what was defined on the project.



Note : as there are some formula linked to each cell (eg. Linked to page *param*), it is recommended to add or remove lines rather than changing the initial position of a group of lever using a copy/paste.



![headline Key behaviours Key behaviours Key behaviours Key behaviours Key behaviours Key behaviours Key behaviours Key behaviours p_l Travel Travel Travel Travel Homes Homes Homes D iet of lever O lekr_pkm Passenger distance _l Mode of transport Occupancy Car own or hire Living space per person Appliance use Calories consumed 2 4 5 6 8 lekr_noor-intensitv lekr_appliance-use lekr_kcal req ](../../../media/Technical-Documentation-Scenario-Builder-image3.png){width="11.53125in" height="2.9270833333333335in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p><em><strong>If new lever (existing project or new module/branch)</strong></em> :</p><p>Once the lever has been added on Interface, apply the same modification here.</p><p></p><p><em><strong>If new project</strong></em> :</p><p>Be sure the level of each levers is the same than defined on your project.</p></th></tr></thead><tbody></tbody></table>



Param

Set which are the levers that should be considered/displayed ;
-   Column [B] : scenario list
-   Letter of the column corresponding to the given scenario on *Lever* page
-   Start : from which line we should start (to get the levers)
-   End : until which line we should end (to get the levers)
-   Indirect : formula to look into a column of a given page (depends on what you set into the columns C/D/E) : ="Levers!"&C2&D2&":"&C2&E2



![column start All 1 All 2 All 3 All a Reference CORE -80% CORE -95% Behavior Technical Industry exo REF Industry exo CORE -80% Industry exo CORE -95% Industry exo behavior Industry exo tech Industry endo REF Industry endo CORE -80% Industry endo CORE -95% Industry endo behavior Industry endo tech AFOLU intensification and diet AFOLU_agroforestry_no-diet AFOLU 3 Transport Transport Transport 2 3 o s w x AC AD AE Al AK AL AM AN AO Power Power Power 2 3 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 end indirect 158 Levers'J2:JIS8 158 Levers'K2KIS8 158 LeversU:L158 158 Levers'M2:M158 158 Levers'02:0158 158 Levers'P2PIS8 158 Levers'Q2:QIS8 158 Levers'R2:R158 158 Levers'S2SIS8 158 Levers'U2:U158 158 Levers'V2VIS8 158 Levers'W2:W158 158 Levers'X2XIS8 158 Levers'Y2YIS8 158 Levers'AA2:AA158 158 LeversA82:AB158 158 LeversAC2ACIS8 158 LeversAD2ADIS8 158 LeversAE2:AE158 158 LeversA12:A1158 158 LeversAJ2:AJIS8 158 LeversAK2:AK158 158 LeversAL2AL158 158 LeversAM2:AMIS8 158 LeversAN2ANIS8 158 LeversA02A0158 158 LeversAP2:AP158 158 LeversAQ2AQ158 ](../../../media/Technical-Documentation-Scenario-Builder-image4.png){width="4.0in" height="6.0in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p>If new scenario added to the Lever page, add it in the column B too, and fill following columns.</p><p></p><p>Be sure all the levers you want to display are in the list set in the column "<em>indirect</em>"</p><p></p></th></tr></thead><tbody></tbody></table>



Variables

Set all the metrics (variables) that are used in PowerBi to create graphs. If a variable is set into the Pathway Explorer, it will not be necessarily added to the Variables list, as only metrics that are usefull for the PowerBi are listed here. This step is done manually.



**Columns** :
-   **Id** : Incremental number ; from 1 to the total number of metrics in the list
-   **Output_name** : gives an output name based on the number set in the id column.

Formula used : IF(ISBLANK(C2),"","[outputs]{#"&A2&"}id")
-   **Column** : copy/paste here the variable from Pathway to Explorer you want to use in PowerBI.

Text is in green when the check column is set to TRUE.
-   **Check** : check if metrics in "column" exist in Pathway to Explorer.

Formula used : if(countif(variable_list!B:B,C2)>0,"TRUE","FALSE")



In yellow, the cell number has to be change from one line to another one.

![output_name ¯ column dhg_emissions-GHG_domestic[MtC02e] 3 4 7 8 10 ind elc fos lus tra bld dac _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG agr_energy-demand_domestic-total[TWh] che ¯ TRUE TRUE TRUE TRUE TRUE TRUE TRUE TRUE TRUE TRUE ](../../../media/Technical-Documentation-Scenario-Builder-image5.png){width="6.71875in" height="2.7083333333333335in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p>If you want to use new metrics in PowerBi (or delete some) : make the changes here.</p><p>Be sure this metrics appears in green (you should update the variable_list page first)</p><p></p></th></tr></thead><tbody></tbody></table>



Variable_list

Takes all the metrics from the page ***0.2 Pathway Explorer*** to be sure metrics set in PowerBi are present on the interface -- Pathway Explorer.



**Columns** :
-   **Id** : Incremental number ; from 1 to the total number of metrics in the list
-   **Column** : copy/paste the column named "*column*" coming from the 0.2 Pathway Explorer page (interface Google sheet)
-   **Metric** : determines what is the metric (based on the column name).

Formula used : REGEXEXTRACT(B4,"(?:..._)?([^_]+)(?:_.*)*[.+")
-   **Sector** : determines what is the sector of the metric (based on the column name).

Formula used : REGEXEXTRACT(B5,"(...)_")
-   **Accessible ?** : check if the variable exist in the Variable page (makes it easier to find which variable are not set in the Variable page yet)

Formula used : SI(NB.SI(Variables!C:C,B5)>0,"TRUE","FALSE")



In yellow, the cell number has to be change from one line to another one.

![id column 1 Years Country metric emissions-p b sector emissions-per-subsector emissions-per-end-use emissions-per-end-use emission nd-use emission emissions-per-end-use sector 3 bld 4 bid 5 bld 6 bld 7 bld bld O bid emissions-per-subsector C02e non-residential[Mt] emissions-per-subsector C02e residennallMtl emissions-per-end-use emissions-per-end-use emissions-per-end-use 8 bld_emissions-per-end-use em use emissions-per-end-use coze C02e C02e C02e coze coze _ appliances-all(MtJ _ cooking[Mt] _ cooling[Mtl _ heating [Mt) hotwater[Mt] lighting[Mt] Accessible in PowerBl ? TRUE TRUE TRUE TRUE TRUE TRUE TRUE TRUE ](../../../media/Technical-Documentation-Scenario-Builder-image6.png){width="9.09375in" height="2.21875in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p>Each time the interface google sheet has been modified, update this page with new metrics and check in the variable page if metrics are still available in both PowerBi and Pathway Explorer.</p><p></p></th></tr></thead><tbody></tbody></table>





Scenario pages

Each columns referring to a scenario has its own page :

*Example : in Filter pages you find the same scenario than on pages available (in red).*

![](../../../media/Technical-Documentation-Scenario-Builder-image7.png){width="4.9375in" height="2.0520833333333335in"}



![Reference . CORE 80 CORE 95 behavior technol ](../../../media/Technical-Documentation-Scenario-Builder-image8.png){width="5.0in" height="0.5208333333333334in"}



In each scneario page, data coming from others pages are summarize here :
-   **Column [A]** :

    -   line 1 : {levers}default

    -   Line 2 : textjoin(", ", 1, INDIRECT(param!F6))



This column set the value of each lever for the given scenario. Value in blue should be changed according to your scenario.

*Example* : Scenario named "Reference" is set on the line 6 (*param* page). Column "F" is used to set where youwill find the value of the levers. Param!F6 is then the value to use.

![column start All 1 All 2 All 3 Reference CORE -80% CORE -95% Behavior Technical o s 2 2 2 2 2 2 2 2 end indirect 158 Levers'J2:JIS8 158 Levers'K2KIS8 158 LeversU:L158 158 Levers'02:0158 158 Levers'P2:P158 158 Levers'Q2:QIS8 158 LeversR2:R158 158 Levers'S2SIS8 ](../../../media/Technical-Documentation-Scenario-Builder-image9.png){width="5.0in" height="1.9583333333333333in"}




-   **Column [B] - line 1** : TRANSPOSE(Variables!B2:C3005)

This column transpose all the metrics present in the *Variable* page

Example :

*Figure 1 : metrics 1 to 9 in the Variable page*

![utput_name [outputs] ¯ column agr_emissions-GHG ind elc fos lus tra bld dac agr tra _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _domestc[MtC02e] emissions-GHG _ domestc(VtC02e) emissions-GHG energy-demand_domestic-total[TWh) energy-demand_domestic-total[TWhl elc_prod_RES_other_geothermal[TWhl ](../../../media/Technical-Documentation-Scenario-Builder-image10.png){width="5.0in" height="2.9583333333333335in"}



*Figure 2 : transposition of the metrics (scenario page)*

![agr_emissions-GHG domestic(MtC02e] [outputs]{#2)id [outputs]{#3)id [outputs]{#4)id [outputs]{#5)id [outputs]{#6)id [outputs]{#7)id [outputs]{#8)id dhg_emissions-( ind_emissions-C elc emissions-G fos_emissions-C lus_emissions-G tra_emissions-G bld emissions-C dac_emissions-GHG _domestic[MtC02e] [outputs]{#10)id agr_energy-dem. ](../../../media/Technical-Documentation-Scenario-Builder-image11.png){width="11.895833333333334in" height="1.0625in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p>If you added a new scenario on the Lever page, create a page for it like it is done above.</p><p></p></th></tr></thead><tbody></tbody></table>





| Update scenario builder |
|-------------------------|

Once you have done your modifications, scenario builder should be updated.

If you only add a new scenario, update inly the corresponding page.

If you added new metrics or new levers, all the scenario pages should be updated.



First, be sure you have the add-ons "Export Sheet data".

![Add-ons Help Last edit was Document add-ons Export Sheet Data Sheetgo Get add-ons Manage add-ons ](../../../media/Technical-Documentation-Scenario-Builder-image12.png){width="1.8958333333333333in" height="2.0625in"}



If no, add it like this ;

<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><ol class="incremental" type="1"><li><p>Go to Add-ons &gt; Get add-ons</p></li></ol><blockquote><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image13.png" style="width:1.8125in;height:2.125in" alt="Add-ons Help Last edit was 59 Document add-ons Export Sheet Data Sheetgo Get add-ons Manage add-ons " /></p><p></p></blockquote><ol class="incremental" start="2" type="1"><li><p>Look for "Export Sheets Data"</p></li></ol><blockquote><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image14.png" style="width:6.4375in;height:0.90625in" alt="G Suite Marketplace Works with Sheets O Q Q Q export Export Sheet Data StratosMedia Export Tool o " /></p><p></p><p></p></blockquote><ol class="incremental" start="3" type="1"><li><p>Double click on the appropriate add-ons</p></li></ol><blockquote><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image15.png" style="width:2.42708in;height:3in" alt="0 Gbort: Sheet Data Export Sheet Data 4.0 (179) 25,158 " /></p><p></p><p></p></blockquote><ol class="incremental" start="4" type="1"><li><p>Install it</p></li></ol><blockquote><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image16.png" style="width:5in;height:1.5in" alt="G Suite Marketplace Power Tools Ablebits Sheets Add-on Q Search apps x (1741) • Install " /></p><p></p></blockquote></th></tr></thead><tbody></tbody></table>





In order to update scenario page, go to each page needed to be updated and do the following steps :

<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><ol class="incremental" type="1"><li><p>Go to Add-ons &gt; Export Sheet Data &gt; Open sidebar</p></li></ol><blockquote><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image17.png" style="width:3.28125in;height:2.59375in" alt="Add-ons Help Last edit was 1 hour ago Document add-ons Export Sheet Data Sheetgo Get add-ons Manage add-ons Open Sidebar Release Notes (v5g) Documentation Nested Elements Help hid ns- " /></p><p></p><p></p></blockquote><ol class="incremental" start="2" type="1"><li><p>Sidebar appears on the right side. Parameters are :</p></li></ol><table><colgroup><col style="width: 25%" /><col style="width: 25%" /><col style="width: 25%" /><col style="width: 23%" /></colgroup><thead><tr class="header"><th><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image18.png" style="width:2.40625in;height:4.53125in" alt="— Format Export Format JSON Export Folder Export Sheet(s) Current sheet only — General Replace existing file(s) Unwrap single row sheets Collapse single row sheets Ignore empty cells " /></p><p></p></th><th><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image19.png" style="width:2.33333in;height:3.92708in" alt="— Advanced Nested Elements Minify data Include frst column Ignore prefix NOEX Unwrap sheet prefix US Collapse sheet prefix " /></p><p></p></th><th><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image20.png" style="width:2.33333in;height:2.15625in" alt="— JSON Force string values Export cell arrays Export sheet arrays Export value arrays " /></p><p></p></th><th><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image21.png" style="width:2.20833in;height:4.21875in" alt="— Advanced JSON Export contents as array Export cell objects Empty value format Null ( null Null value format Null ( null Array separator character Array prefix JA Nested Array prefix " /></p><p></p></th></tr></thead><tbody></tbody></table><blockquote><p></p><p></p></blockquote><ol class="incremental" start="3" type="1"><li><p>Click on "Export" ; data will be exported as a json file. This file is set in the Drive where your Scenario Builder file is (Drive / <em>Project_Name</em> / scenarios builder / ). <strong>Wait for the result pop-up page (should say "Export Complete". If no, fix it).</strong></p></li></ol><blockquote><p></p><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image22.png" style="width:2.98958in;height:1.14583in" alt="Include first column Visualize " /></p><p></p><p><img src="../../../media/Technical-Documentation-Scenario-Builder-image23.png" style="width:7.9375in;height:6.4375in" alt="My Drive Name &gt; CLIMACT &gt; EU2050 &gt; scenarios builder EUCalc - scenarios builder - external EUCalc - scenarios builder - interne EUCalc - scenarios builder - interne - AFOLU_3.json EUCalc - scenarios builder - interne - AFOLU_agroforestry.json EUCalc - scenarios builder - interne - AFOLU_intensification_diet.json EUCalc - scenarios builder - interne - behavior.json EUCalc - scenarios builder - interne - CORE_80.json EUCalc - scenarios builder - interne - CORE_95.json EUCalc - scenarios builder - interne - Reference.json EUCalc - scenarios builder - interne - scenario_l .json EUCalc - scenarios builder - interne - scenario_2.json EUCalc - scenarios builder - interne - scenario_3.json EUCalc - scenarios builder - interne - scenario_4.json EUCalc - scenarios builder - interne - technology.json EUCalc - scenarios builder - interne.json Folder where the Scenario Builder File is All the json files are exported here " /></p><p></p></blockquote></th></tr></thead><tbody></tbody></table>

























