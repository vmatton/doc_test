<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [PyCharm](#pycharm)
- [How to install](#how-to-install)
- [How to use](#how-to-use)
- [How to run a code](#how-to-run-a-code)
- [How to push knime2python folder when tests are alright ?](#how-to-push-knime2python-folder-when-tests-are-alright-)
- [Tips](#tips)
  - [Go to the definition of an element used in python (eg a function, a class, ...) :](#go-to-the-definition-of-an-element-used-in-python-eg-a-function-a-class--)
- [LOGS in TESTS](#logs-in-tests)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# PyCharm

Created: 2020-04-15 08:59:31 +0200

Modified: 2021-09-16 12:25:52 +0200

---




PyCharm is an integrated development environment used to write and run python codes as well as other file types. As most of the tests concerning the converter are run on remote (to avoid our own computer to slow down), this software must be installed on each windows server we create (cfr [Amazon Web Services (AWS)](onenote:#Amazon%20Web%20Services%20(AWS)&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={0A729CE1-374D-4CC5-B587-D730CC4FDD82}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)) page for more details about servers).



Python codes used in the deployment are described in the following subpages. Other files can also be read by PyCharm (eg. Ylm). These files are described in the section they are linked to (eg. Configuration files and docker-compose (ylm) are described in the MobaXterm section as their are relative to building dockers).



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="how-to-install">How to install</h1></th></tr></thead><tbody></tbody></table>



Go to : <https://www.jetbrains.com/pycharm/download/#section=windows> and follow the instructions.



**Next step** : specify the python interpreter that should be used :

1.  Go to File > Settings

![Texte de remplacement généré par une machine: Edit Miew Navigate Code Befactor Run kni New Project... New Scratch File Open... Open Recent Close Project Rename Project... Settings... File Properties Alt+lnsérer Ctrl+AIt+Maj+Insérer Ctrl+AIt+S ](../../media/Technical-Documentation-PyCharm-image1.png){width="3.65625in" height="2.6354166666666665in"}





2.  Go to Project : knime2python > Project Interpreter and choose a Project Interpreter



![Texte de remplacement généré par une machine: Settings Appearance & Behavior Version Control Project: knime2python Project Interpreter Project Structure Build, Executiom Deployment Languages & Frameworks Project: knime2python Project Interpreter. Package ato micwrites blinker ca- ce tificates cachetools certifi chardet click colo rama coloredlogs cryptography cycler > Project Interpreter Python 3.7 (knime2pythan) (2) Version 1.4.o 4.1.0 20204.5.1 1.140 3.0.4 7.1.2 0.43 2.9.2 o.10.o For current project Latest version 1.4.o 193.0 3.1.1 20204.5.1 1.140 3.0.4 7.1.2 0.43 2.9.2 o.10.o ](../../media/Technical-Documentation-PyCharm-image2.png){width="7.145833333333333in" height="3.15625in"}





3.  If there is no interpreter to choose, create one (cfr Figure below)



![Texte de remplacement généré par une machine: O New environment Locatio n: Base interpreter. Inherit global site-packages Make available to all projects Ex isting environment Interpreter: CNO interpreter> Make to all projects ](../../media/Technical-Documentation-PyCharm-image3.png){width="6.46875in" height="3.28125in"}









<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="how-to-use">How to use</h1></th></tr></thead><tbody></tbody></table>



All the codes used for the deployment are set in a folder named knime2python.

![Disque local XCaIc Nom knime2pythan webtaal Modifié le D8-CU-20 11:44 06-04-20 11:32 12-02-20 DSB2 ](../../media/Technical-Documentation-PyCharm-image4.png){width="4.958333333333333in" height="2.65625in"}

Files that are inside the knime2python folder are :
-   Config : set the configuration needed by the dockers to build containers (cfr [MobaXTerm](onenote:#MobaXTerm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={D305507A-BF69-46A1-AB14-BDD34B4CF290}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)). It contains information that change between projects (baseyear, output nodes used for the webtool/powerBI, predefined scenarii, ...)
-   Db : it is used to initialize a new server (set the database, the password, ...). **Should not be changed !**
-   Scripts : all the codes used in the deployments (tests, converter, API, ...)
-   Src : contains all the codes needed for the converter
-   Tests : codes to test our codes and Knime workflow before deploying



![knime2python config init.sql scripts old & drafts becalc-app.py becalc_scenarios.xlsx eliacalc-app.py eu20SO-app.py googlesheet.py gtap_scenarios.py install docker.sh interactive.py precalculate.py quickscan.py vlaio-app.py data workflow _init_.py logging.ini utils.py .gitignore CHANGELOG.md docker-compose.yml README.md requirements.txt ](../../media/Technical-Documentation-PyCharm-image5.png){width="2.34375in" height="5.0in"}





In the following subpages, we described only codes that are used to deployment, as well as tests made before deployment.



PyCharm use a color legend for each files :
-   Files written in blue = files that have been modified but are not pushed yet
-   Files written in green = files that will never be pushed as they are in the gitignore file
-   Files written in red = new files.







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="how-to-run-a-code">How to run a code</h1></th></tr></thead><tbody></tbody></table>



If you want to **run a specific code**, right click on it (on the left frame) and select "Run (...)".



![bec elia• goo gtaF u inst prel quil san test test v Iai. Cut Copy Copy Path... Paste Find usages Inspect Code... Befactor Clean Python Compiled Files Ctrl+X Ctrl+C Ctrl*V Alt*F7 Add to Fgvorites Eeformat Code Optimize Imports Debug 'becalc-app' Ctrl+Alt+L Ctrl+Alt+O Supprimer ](../../media/Technical-Documentation-PyCharm-image6.png){width="3.53125in" height="3.65625in"}





**Run only a part of a code**, on the left side of your code, there are green arrow that allow you to run only a specific part of your code.



![- if n ame warnings. filter-warnings ignore " ) # Extract command line arguments parser argparse.ArgumentParser() parser. add _ argument ( -c , - -configuratio default---os . path . join( ](../../media/Technical-Documentation-PyCharm-image7.png){width="3.9375in" height="2.09375in"}





**Run one of the last run we have made** (either a full code or only a part of the code), on the top right menu, you have a list of the last run you made (here, only becalc-app is available). Select the code (or part of the code) you want to run again, and then click on the green arrow.



![becalc Edit Configurations... Save 'becalc-app' Configuration Configurt becalc ](../../media/Technical-Documentation-PyCharm-image8.png){width="3.9375in" height="1.5in"}







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="how-to-push-knime2python-folder-when-tests-are-alright">How to push knime2python folder when tests are alright ?</h1></th></tr></thead><tbody></tbody></table>



Simply use the PyCharm console to do so. Command line to add/commit/push are the same than used in git (cfr User Manual > GIT).



![Texte de remplacement généré par une machine: n le.o.17763.4371 (c) 2016 Microsoft Corporation. Tous droits réservés. ](../../media/Technical-Documentation-PyCharm-image9.png){width="5.5in" height="2.1875in"}









<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="tips">Tips</h1></th></tr></thead><tbody></tbody></table>



## Go to the definition of an element used in python (eg a function, a class, ...) :

CTRL + right click on this element.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="logs-in-tests">LOGS in TESTS</h1></th></tr></thead><tbody></tbody></table>



Select UnitTests as Default test runner

![_ processing.py lg.DataProcessingTestCase.test_... •e' Test I min 29 sec proc ' Python tests for x build-docker-files.sh x # df_of_knime_out # df_0f_ configuration.py "Years" "Years" " subsector" " subsector " "buildin t "buildin t "renovationcate or "renovationcate or tc Set-tings Key map Editor > Version Control > Project: knime2python > Build, Executiom Deploy ment Languages & Frameworks Tools Space Actions on Save Web Browsers and Preview External Tools Terminal Code With Me Diff & Merge External Documentation Features Trainer Pandoc Python Integrated Tools Server Certificates Settings Repository Shared Indexes Startup Tasks > Tasks Advanced Settings thon_out Tools > Python Integrated Tools Packaging Package requirements file: Pipenv Path to Pipenv executable: requirements.txt Testing Default test runner: Docstrings Docstring format: Unittests reStructuredText " end-use" " end-use" ivot"] ivot"] Gra (g122öJ Analyze Python code in docstrings Render external documentation for stdlib reStructuredText Sphinx working directory: Treat txt files as reStructuredText 2.825 merge [854 frames hidden] pandas, tests, 1.500 select_dtypes pandascoreframe [724 frames hidden] pandas, tests, -p no: logging - Cancel • PY:57 numpy, copy .py•.3477 numpy, abc, typing ](../../media/Technical-Documentation-PyCharm-image10.png){width="8.229166666666666in" height="4.6875in"}



Add Additional Arguments in your test configuration

-s



![](../../media/Technical-Documentation-PyCharm-image11.png){width="7.09375in" height="3.5833333333333335in"}











