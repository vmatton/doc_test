<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Deploy XCalc (HOW TO)](#deploy-xcalc-how-to)
- [File hierarchy](#file-hierarchy)
- [Web server](#web-server)
- [Docker](#docker)
  - [Install](#install)
    - [Windows](#windows)
    - [OSX](#osx)
  - [Run](#run)
  - [Usefull commands](#usefull-commands)
  - [Common issues](#common-issues)
    - [](#)
    - [SSH keys not found](#ssh-keys-not-found)
    - [File path are wrong](#file-path-are-wrong)
    - [Bad Interpreter on Windows](#bad-interpreter-on-windows)
    - [Ports are not available](#ports-are-not-available)
    - [](#-1)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Deploy XCalc (HOW TO)

Created: 2021-03-25 15:40:34 +0100

Modified: 2021-09-07 09:41:58 +0200

---


To deploy a complete XCalc solution you will have to :
-   Run a web server
-   Run docker with the following containers

    -   a model container

    -   a db container

    -   A mysql container



# File hierarchy

Xcalc

|--- dev

| |--- _interactions

| |--- [sector module]

|---knime2python

|---webtool





# Web server



The web server is the service which is responding to the browser. We are using [REACT](http://REACT) (Java Script) to deploy the service. REACT is a java script library for building user interfaces.



The webtool is versioned on [Bitbucket](https://bitbucket.org/climact/webtool/src/master/). To switch between the BE and the EU version two branches are used.



To install the required packages:

>>> npm install packages.json



To build the service in development mode run in the folder 'webtool' :

>>> npm start



Please note that this deploy a development service which is recompiling on each change. This is can not be used to deploy a web server in production.



To deploy the web server in production you have to :
-   Create a screen which will stay alive after your ssh exit. This will create a fake screen named run.

>>> screen -S [screen name]
>>> screen -S run
-   Build webtool (npm (deprecated) or yearn)
    >>> npm <run> build
-   >>> yarn build
-   Build the service from the folder 'webtool'. This will serve the web server service on the port 3000.

>>> serve -s build -l 3000
-   Switch in detached head mode. This will exit the screen "run" that you've just created.

CTRL + A followed by D



To see the open ports use :

>>> sudo netstat -tulpn | grep LISTEN



To see the running screen use :

>>> screen -ls



To switch to a running screen use :

>>> screen -r [name]

If your screen is named "run" then use:

>>> screen -r run



To kill a screen switch to it and then use:

CTRL + A followed by K



To check which are the running process linked to a given screen:

>>> ps -fx





# Docker



[Docker](https://www.docker.com/) use containers (yes, just like the ones on boats) to ship code in a multi platform environment. To deploy XCalc, three containers are used:
-   Model container: this is the container with the python model. This model is served through an API.
-   DB container: this is the container which is managing the caching system. When a scenario is computed this scenario is stored. When a scenario is asked the cache is first checked.
-   MySQL container : this is the database storing the data used by the caching system.



The code used by the dockers is versioned on Bitbucket. Two repositories are used:
-   [Knime2Python](https://bitbucket.org/climact/knime2python/src/master/) : containing all python (converter, API, misc)
-   [_interactions](https://bitbucket.org/climact/_interactions/src/master/) : containing the model in KNIME (XML files)



## Install



### Windows

Virtualization is not possible on virtualized OS. EC2 machine (i.e. Windows servers on AWS) are virtualized and do not allow docker to run. If server is needed, use a Linux one.


-   Download and install Docker Destkop for Windows ([here](https://docs.docker.com/docker-for-windows/install/))
-   ~~Go to Control Panel (Windows > Control Panel)~~
-   ~~Navigate to Programs > Programs and Features > Turn Windows features on or off~~
-   ~~Enable Hyper-V feature~~
-   Restart your marchine
-   When launching Docker you will have an error saying that you need to install WSL2.
-   Install Linux bash on Windows using Windows Subsystem for Linux (WSL 2) (Follow the link proposed on the docker popup or [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10#step-4---download-the-linux-kernel-update-package) : follow "Download the Linux kernel update package" - and [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10#manual-installation-steps) if needed). Not installing Ubuntu is not necessary.
-   ~~Open Docker configuration > General~~

    -   ~~Tick "Use the WSL 2 based engine"~~

    -   ~~Tick "Expose daemon on tcp://localhost:2375 without TLS"~~
-   ~~Open Docker configuration > Resources > WSL Integration~~

    -   ~~Tick "Enable integration with my default WSL distro"~~

    -   ~~Tick "Ubuntu"~~
-   Install SSH keys (see [SSH keys not found](onenote:#Deploy%20XCalc%20(HOW%20TO)&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BD9B884-63AB-1E47-8CF5-9AF13449B4ED}&object-id={E91DBE91-CBDA-D029-A131-115E5EBF18A1}&40&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))



To start and build the dockers you will use the Windows Subsystem for Linux and search "Ubuntu" in the search menu.



### OSX

Use the following command :

>>> brew install docker



Then, Install SSH keys (see [SSH keys not found](onenote:#Deploy%20XCalc%20(HOW%20TO)&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BD9B884-63AB-1E47-8CF5-9AF13449B4ED}&object-id={E91DBE91-CBDA-D029-A131-115E5EBF18A1}&40&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))



## Run

If you want a clean start then these three dockers are deployed through the following command (from the folder 'knime2python'):

>>> ./scripts/build-docker-files_reset-db_run.sh [docker config file] [docker version tag]

If you want to deploy the version 1.4 of XCalc EU use:

>>> ./scripts/build-docker-files_reset-db_run.sh config/config_docker_eu2050.yml 1.4



To see the running dockers use:

>>> docker ps

You should see three containers running (as explained above).



## Usefull commands
-   List images

>>> docker images
-   Delete image

>>> docker rmi [hash]
-   See log

>>> docker logs [container name] -f
-   See disk usage

>>> docker system df
-   To clean up the dockers files:

    -   Remove dangling images

>>> docker image prune
-   Remove dangling volumes:

>>> docker volume prune





## Common issues

### 

### SSH keys not found

You should check that the path configured in "knime2Python/scripts/build-docker-files.sh" for the "docker_build_rsa" is correct. If you want to deploy it locally you will have to copy the private key from the server. The path of the key on the server is : "~/docker_build_rsa".



On Windows, with WSL, the pathway to .ssh of a user is :

/mnt/c/Users/[User]/.ssh/docker_build_rsa



It may happen that windows is having issue pasting the ssh key while creating docker. For this issue, replace the line

RUN echo"${SSH_KEY}">/root/.ssh/id_rsa

By

COPY docker_build_rsa/root/.ssh/id_rsa



### File path are wrong

Depending on your local configurations the referenced paths in the code could be wrong. Here are some of the key files to check:
-   knime2Python/scripts/eu2050-app.py : check YML configuration file
-   knime2Python/scripts/build-docker-files.sh : check path to access SSH keys



### Bad Interpreter on Windows

If you have the following error :

>>> /bin/bash^M: bad interpreter: No such file or directory



Open Bash command shell (Windows menu > Ubuntu)
-   Do

>>> cd /mnt/c/XCalc/knime2python

>>> sed -i -e 's/r$//' scripts/*.sh



=> this error typically arise when using the build-*.sh files



If you have the following error :

>>> standard_init_linux.go:228: exec user process caused: no such file or directory



Open the Bash command shell (bash in command line terminal)
-   Do

>>> cd /mnt/c/XCalc/knime2python

>>> dos2unix scripts/*.sh



=> this error typically arise when using the entrypoint_*.sh files



More details:
-   <https://stackoverflow.com/questions/51508150/standard-init-linux-go190-exec-user-process-caused-no-such-file-or-directory>
-   <https://forums.docker.com/t/standard-init-linux-go-175-exec-user-process-caused-no-such-file/20025/2>





### Ports are not available 

If you have the following error :

>>> Ports are not available: listen tcp 0.0.0.0:3306: bind: Only one usage of each socket address (protocal/network address/port) is normally permitted



Open Task Manager and stop mysql.exe process



Docker is using all the memory of your computer

<https://medium.com/@lewwybogus/how-to-stop-wsl2-from-hogging-all-your-ram-with-docker-d7846b9c5b37>

### 


