<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Test : Quickscan](#test--quickscan)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Test : Quickscan

Created: 2020-04-15 09:04:54 +0200

Modified: 2020-05-04 16:05:26 +0200

---


| Introduction |
|---------------|



Quickscanisusedto test asectormetanodes**beforeupdatinginteractions inKnime**. Thisstepwasmade toavoidupdatingKnimewithmetanodesthatuseKnimenodethatare notimplementedin theconverter.



This codeisin the "script" folder.



![](../../../media/Technical-Documentation-Test---Quickscan-image1.png){width="2.5in" height="3.4375in"}





| How to use |
|-------------|



Beforerunningthiscode,makesureyougetthelatestversion ofeachsector(pull).



This codereadall themetanodesthataredescribedin thefollowingdictionaries;

Foreachprojectsyoushouldhavetwodictionaries:
-   ***Project_name***_workflow_dict:definesthesectorthatshouldbeanalyzed
-   ***Project_name***_metanode_dict:definesthesharedmetanodesthatshouldbetestedforeachsector(eg. In agriculture,twometanodesshouldbetested; 4.1 Land-Use and 4.3 Agriculture)



![](../../../media/Technical-Documentation-Test---Quickscan-image2.png){width="6.520833333333333in" height="8.104166666666666in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p></p><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If new module</strong>: you should add this module in bothdictionaries;wokflow_dictandmetanode_dict(for eachcalculator concerned by this new module).</p><p>Most of the time, each new project is based onbecalccalculator (named alsoxcalc) and we do not need to add adictionary. For some reasons, if a project is done on a branch ofbecalc, you should add two othersdictionariesfor this project (eg.VLAIO).</p><p></p></th></tr></thead><tbody></tbody></table>





Reading ofeachsectorsismade asfollowed. Youshouldspecifywhichprojectyouwantto test (cfrredframes).



![](../../../media/Technical-Documentation-Test---Quickscan-image3.png){width="8.09375in" height="6.4375in"}





TheWorkflowBuilderandQuickscanfunctionalitiesaredefinedin the workflow_builder.py code (cfrsrc folder).



![](../../../media/Technical-Documentation-Test---Quickscan-image4.png){width="3.40625in" height="1.625in"}





The variable «workflow_path» corresponds to theworkflow.knimefile available in each sector.

![](../../../media/Technical-Documentation-Test---Quickscan-image5.png){width="5.989583333333333in" height="1.7708333333333333in"}







Whenthe runismade andeverythingisalright



![](../../../media/Technical-Documentation-Test---Quickscan-image6.png){width="7.90625in" height="3.71875in"}





Whenthe runismade andtherearesomeerrors



Whenwerun the code andtherearesomeerrorsrisingup,theyarewrittenin the console (cfrimagebelow) aswellas in log file (one file bysector, set in the "scripts" folder where the Quikscan code is also set).

Theerrormessageindicateswhichnodenumberisconcerned.Thesemassagesshouldbesendtoeachmodelerssothattheycanrevisetheirworkflow.

Note : the log files are indicated in the gitignore file. Therefore, there are only available in local, where the quikscan has been run.



![](../../../media/Technical-Documentation-Test---Quickscan-image7.png){width="6.25in" height="1.4375in"}







