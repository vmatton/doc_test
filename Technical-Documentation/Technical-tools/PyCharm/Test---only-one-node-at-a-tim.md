<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Test : only one node at a time](#test--only-one-node-at-a-time)
- [How to do ?](#how-to-do-)
  - [When a new node is created](#when-a-new-node-is-created)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Test : only one node at a time

Created: 2020-06-02 09:13:00 +0200

Modified: 2020-06-02 10:17:12 +0200

---




This could be useful when you create a new node and you want to test its conversion in python or when you update Knime version and want to check if the conversion of specific node is still alright.





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="how-to-do">How to do ?</h1></th></tr></thead><tbody></tbody></table>



The easiest way to test a node is to create a workflow with only this node and use the test "test_workflow_runner_basic".



![Texte de remplacement généré par une machine: data comman workflow workflow _lmt_.py test workflow runner.ov ](../../../media/Technical-Documentation-Test---only-one-node-at-a-time-image1.png){width="3.5729166666666665in" height="3.4375in"}





All the node that already have a workflow are set in "knime2python/test/data". **This folder should be updated each time _common was pushed with modifications ! TO DO : FIND AN AUTOMATIC WAY TO DO THIS !**

When you open Knime, you should choose, as a workspace, knime2python folder.



## When a new node is created



1.  Create a workflow for this node and save it in knime2python > test > data

2.  Add this node to "test_workflow_runner_basic" (cfr Figure below)



![Texte de remplacement généré par une machine: def test that the workflow 14 ColumnResorter can be read and run from file(self): os . path . , 'workflow', os . path . 'workflow. knime get_workspace_dir() workspace wf = workspace) graph, global_vars = Vif . build() # nx.draw(graph, # plt.show() runner = WorkflowRunner(graph, global_vars) runner . run() def test the workflow 15 DatabaseReaderV/riter can be read and run from file(self): . Onl desi nec/ to work data are resent in the database o the user: db name 'levers # FIXME table name: os . path . , 'workflow', os . path . 'workflow. knime get_workspace_dir() workspace wf = workspace) s tra rei ht load- actor graph, global_vars = wf.build() OSON Reader data_file os . path . , with as f : json . load(f) data 'file', ' lever_position . json # format OSON input to be like flow variables in Knime {d: ('INTEGER', for d in levers_position global_vars runner = WorkflowRunner(graph, global_vars) runner . run() ](../../../media/Technical-Documentation-Test---only-one-node-at-a-time-image2.png){width="12.322916666666666in" height="6.0625in"}










