<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Test : Knime to Python converter](#test--knime-to-python-converter)
- [Common errors](#common-errors)
- [Content](#content)
  - [Initial parameters](#initial-parameters)
  - [](#)
  - [Class DataProcessingTestCase](#class-dataprocessingtestcase)
  - [Test_current_data_processing_for_debug_excel_reader](#test_current_data_processing_for_debug_excel_reader)
  - [Test _data_processing_output_comparison](#test-_data_processing_output_comparison)
  - [Test _data_processing_multi_run](#test-_data_processing_multi_run)
  - [Multirun is used to determine the running time of each node](#multirun-is-used-to-determine-the-running-time-of-each-node)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Test : Knime to Python converter

Created: 2020-04-15 09:05:28 +0200

Modified: 2020-07-28 09:44:12 +0200

---


The full converter is set in the src section. Here, we only test some of the functionalities of the converter. These tests are set in the tests folder and is named test_data_processing.py. Some other tests exist but they are only set to test a specific node or functionality and not the whole process of the converter. You will find all the test codes in the tests folder.

![data workflow init .ov test_data_processing.py test_update_vs_run.py ](../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image1.png){width="3.3125in" height="2.6458333333333335in"}



The test_data_processing contains three main tests that are described below.



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="common-errors">Common errors</h1></th></tr></thead><tbody></tbody></table>

Most of the time there is differences between python and Knime outputs for the following reasons :
-   Math formula have been not properly copied in python (cfr converter : src > nodes > math_formula)
-   The initial database is not the same. It happens when we update and run data-preprocessing but we did not run again data-processing. In this case data-processing use an old version of input data while python use the proper one. To solve it, rest data-processing and run it again.

*Example : error rise up when table do not have the same size*

![DataFr.ne (lefti : (ze, (r1ghtJ: (32, 2342) ı ](../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image2.png){width="2.4375in" height="1.25in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="content">Content</h1></th></tr></thead><tbody></tbody></table>



## Initial parameters

In this section we import all the required libraries. We also defined the workspace and the prefix of the google sheet. **Depending on the project you are working on and the calculator you relied on, you should change these two last parameters** (cfr red frames on the image below).



![- import import import import import import import J son logging logging.config os sys unittest coloredlogs pandas as pd warnings default_timer as from src . workflow. workflow builder import f r" Sr-C runner import from tests .utils import get _ test data dir, get _ test dir, get _ test data file dir - from pandas. util. testing import assert_frame equal IORkSPACE • ' a 'dev') GOOGLESHEETPREFIX = •BE ](../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image3.png){width="6.114583333333333in" height="3.6666666666666665in"}



## 

## Class DataProcessingTestCase

This class allows us to test some of the functionalities of the converter :
-   Compare Knime output with python output (anywhere one the workflow) : *test_current_data_processing_for_debug*
-   Compare final Knime output with pytjon output (only at the end of the workflow) : *test_data_processing_output_comparison*
-   Test if an update of a scenario gives same results : *test_data_processinf_multi_run*



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="test_current_data_processing_for_debug_excel_reader">Test_current_data_processing_for_debug_excel_reader</h2><p></p><p>This test allows us to compare the values inside an Excel table created in Knime with a table created in Python.</p><p></p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image4.png" style="width:7.8125in;height:1.40625in" alt="e emption S " /></p><p></p><p>This code read the Knime workflow and stop when there is a Write to Excel node. It will compare the table it has before this node with the table written (thanks to this node) while running data-processing in Knime.</p><p><strong>The Write to Excel node should be the one that is “deprecated” and there should be only one Write to Excel node at the same time in data-processing workflow.</strong></p><p><strong>Each time you want to compare a table</strong> with the output in python, you should link this table with a Write to Excel node, run this node to create the Excel table. Then <strong>save Knime process</strong> (if not, python will not be able to compare its results with the appropriate table !). Finally, run this part of the code.</p><p>Usually, when we use this test, this is because there is a problem with the final output of python compared to Knime (eg. math formula where not properly written in python leading to mismatch of results). While trying to detect which is the node leading to a mismatch of output, we use the back and forward method (cfr image below) ;</p><ul class="incremental"><li><p>If the output are the same between python and Knime : go forward in the workflow</p></li><li><p>If the output are not the same : go back in the workflow</p></li></ul><p></p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image5.png" style="width:9.15625in;height:2.15625in" /></p><p></p><p></p><p>Sometimes, it is easier to create another workflow with only few metanodes avoiding too much time consuming. If the case, you should save your workflow (eg. As data_processing_debug) and set in the python code you want to use another workflow. This is done here :</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image6.png" style="width:7.40625in;height:0.84375in" alt="warnings. ignore &quot; • as. path. join (WRKSPACE, • processing • os rode_path, " /></p><p></p><p>Your Write to Excel node writes a table in the directory “_interaction / output” and this table is named “debug_output” (cfr frame red 1). <strong>Be sure, you set the proper parameter to the Write to Excel node in Knime workflow</strong>.</p><p>To make the comparison easier, we first sort the value of each category columns (cfr string columns). Usually, these columns are only “Country” and “Years”, but sometimes, the table we want to compare have much more category columns in it. In this case, we should precise in the code what are the column that should be sorted (cfr red frame 2 ; df_of_knime_output and df_of_python_output). Therefore, <strong>be sure you set the appropriate column names here</strong>.</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image7.png" style="width:6.67708in;height:3.375in" alt="os.path.join(WRKSPACE, &quot; as. path. ix_ i _PY_OUI . x as. path. prefix_filename + • df_Of knime Output • values(S df_OQnÅæoutput . thon_output. sort , inplace•True) " /></p><p></p></th></tr></thead><tbody></tbody></table>





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="test-_data_processing_output_comparison">Test _data_processing_output_comparison</h2><p>This test allows us to compare the final values given by Knime compared to the ones given by Python. This code stop running while there is a Write to CSV node (&lt;&gt; test above, where it depends on an Write to Excel node).</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image8.png" style="width:7.8125in;height:1.15625in" alt="def e by " /></p><p></p><p>Before using this code, make sure you run the data-processing workflow completely. The Write to CSV node should have run. It creates a csv table named output_debub.csv in the _interactions/data folder.</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image9.png" style="width:2.77083in;height:3.21875in" alt="i Pc Export &quot;abase and 39009 " /></p><p></p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image10.png" style="width:5in;height:0.48958in" alt="path_output_csv = os . path . join (WORKSPACE , interactions&#39; , &#39;data&#39;, " /></p><p></p><p>As it is the case in the previous test, we should precise here which workflow is used for the test. Usually, there is no need to change that here.</p><p>We also define what are the metanodes to be tested in the output_nodes array.</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image11.png" style="width:5.8125in;height:3.25in" alt="workflow node_path = os . path . join WORKSP,ACE interactions&#39; , &#39;workflows , data-processing&#39;) workflow_path = os . path . join (workflow node_path, &#39;workflow. knime &#39; ) output_nodes = C # Buildings 1276 cut 3839 cut 3847 cut 3787 cut 3722 cut 3789 cut 3836 cut node node node node node node node node node node node a cut I 384a 37ß4 3855 cut cut cut Lifestyle Transport Industry Agriculture Land use Climate Emissions energy demand by sector compared to RES # balance " /></p><p></p><table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If new module</strong> : you should add this module in the output_node array. Cfr in MobaXTerm &gt; Configuration Files to see how to get the node number of your metanode.</p><p></p></th></tr></thead><tbody></tbody></table><p></p></th></tr></thead><tbody></tbody></table>





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="test-_data_processing_multi_run">Test _data_processing_multi_run</h2><p>This test allows us to see if we obtain same results while updating a scenario on the web application. In other words, we test if the "update" functionality of each node gives the same result than the "build" functionality.</p><p><strong>Therefore, each time we modify the "update" function of a node, this test should be run !</strong></p><p></p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image12.png" style="width:6.84375in;height:1.19792in" alt="def : &quot; Test that running multiple times the converter is producing the same results : return: None " /></p><p></p><p></p><p>Example : Update function of the node "joiner" :</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image13.png" style="width:7.09375in;height:2.60417in" alt="Texte de remplacement généré par une machine: def update(self): start = timer() self . df left self. &#39; UPDATE&#39;) columns self . tnorj-self . joiner, on=self . Ieft_input) self . df_merged 10 er - timer() start self . &quot; ENO &quot; ) " /></p><p></p><p>The multirun code runs two scenario two times :</p><ul class="incremental"><li><p>Run 1 : scenario 1 (build are used)</p></li><li><p>Run 2 : scenario 2 (build are used)</p></li><li><p>Run 3 : scenario 1 (update are used if any)</p></li><li><p>Run 4 : scenario 2 (update are used if any)</p></li></ul><blockquote><p></p></blockquote><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image14.png" style="width:7.09375in;height:5.77083in" alt="• {d, d in dotale)) • • oba s rs_l) . tsuR1)&quot;, start run • • - start ae1f.Iosser.infoCC:3as}: 2 • 23 fer d &quot;aux_2 &quot;RW&#39;_3 &quot;RIA_2 fiel in self. self. logger.in start run 4 tier() • Start_ u self { (suai)&quot; , &quot; } &quot; .for.tCRLm_3 (START)&quot; , " /></p><p></p><p>At the end, it compares values coming from run 1 and 3, and from run 2 and 4 :</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image15.png" style="width:5in;height:2.4375in" alt="for key, df in items(): if Bf nnt for key, df in if df is not None: for key, df in items(): if df is not None: s e If. a s u a I (df , " /></p><p></p><p>As it was done in the previous test, we should precise here which workflow is used for the test and also define what are the metanodes to be tested in the output_nodes array.</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image16.png" style="width:5in;height:2.84375in" alt="warnings . filterwarnings ignore interactions&#39; , &#39;workflows , 1276 cut 3839 cut 3847 cut 3787 cut 3722 cut 3789 cut 3836 cut workflow node_path = output_nodes = C os . path . join WORKSP,ACE data -processing&#39;) workflow_path = os . path . join (workflow node_path, &#39;workflow. knime &#39; ) node node node node node node node node node node node a cut I 384a 37ß4 3855 cut cut cut # Buildings Lifestyle Transport Industry Agriculture Land use Climate Emissions energy demand by s # balance tor compared to RES " /></p><p></p><table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>WHAT TO DO ?</strong></p><p></p><p><strong>If new module</strong> : you should add this module in the output_node array. Cfr in MobaXTerm &gt; Configuration Files to see how to get the node number of your metanode.</p><p></p></th></tr></thead><tbody></tbody></table><p></p><p></p><h2 id="multirun-is-used-to-determine-the-running-time-of-each-node">Multirun is used to determine the running time of each node</h2><p>While running multirun, you have in the console the running time for each node.</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image17.png" style="width:6.30208in;height:1.375in" alt="Texte de remplacement généré par une machine: 2e-N-3e 2e-e4-3e 2e.a4-3e 20-04-33 2e-N-3e 2e-e4-3e 2e.a4-3e rs:oa 7uavau 7uavGN • v6N EC2A&quot;AZ- 7uFLV6u src. 7uFLvau 7uavau 7uavGN • v6N INFO RUN RUN (0755) (#3882) Building (te) 3 la Industr ( *3840) (P387S) Agricult (Z37S9) use 3 lb u (#3874) ENO : ENO : ENO ENO END : EMD ( 7.8SB4tsj) 6.442E[sl) ( e.8216tsJ) ( 1.4e13tsj) " /></p><p></p><p>This information is also saved in a text file.</p><p>To set where the text file is saved (check time to time if still the case) : Go to Edit Configurations…</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image18.png" style="width:2.25in;height:1.80208in" alt="Texte de remplacement généré par une machine: Elia -app Edit Configurations... r.• quickscan Unittests e• Unittests for " /></p><p></p><p></p><p>Look for the multi-run code (cfr red frame below) ans check "save console output to file". Here destination file is the Desktop and file name is log_multirun.txt.</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image19.png" style="width:8.16667in;height:4.21875in" alt="Texte de remplacement généré par une machine: RurJDebug Configurations pyttm Pyttm ni&quot;er,&#39;, LJ n itteE t Log be ls File Entry ej SM c o utput to SFu ghvcs Ski p printed standard printed &#39;tamiard " /></p><p></p><p>Once the code has been run and the text file has been updated. Open it (with Sublime text). Look for "RUN_4" (use CTRL + F) and delete all the content above :</p><p><img src="../../../media/Technical-Documentation-Test---Knime-to-Python-converter-image20.png" style="width:6.30208in;height:6.52083in" alt="Texte de remplacement généré par une machine: C : N becalc : / Ice: ( (EPTY) Assemble all output •etanode *874 - XML : C : : / test_data_pr : . For input to run ;iru C : : / estCase. Icee [32m«apped &#39;OdeG87S): No descendants - &quot;02* ing for input to cum 0 : parent%del ;iru ( SS.712311s11)ln&#39; parent&#39;Ode1d.&#39;2•1 (00) (START) c  : tCase. tes [32mSTART : oataReader OTS metanode •3864 - mL: C : : / estCase. I ( DataReader OTS metanode 0864 - XML : c Ibecalc estCase. tes [32nSTART • DataReader OTS metanode *3854 - C : : / /test_data_processing.DataProcessingTestCase. cent I ( e.•gltsl]): DataReader OTS metanode Ogs4 - XML : C : becalc : / estCaSe. - I [32nSTART : DataReader OTS metaru&gt;de *3853 - »tL: ( DataReader OTS metanode •3SS3 XML : C :  becalc  : / [32m«apped No descendants - for input to cum t • l(eln• parentNode1d=•: «exlt•-l [32nSTART : (E&#39;VTY) API Interface *tanode •S6S . XML : C : : / estCase. ceylt I [32nEt•O ( (EPTY) API Interface #86S - XML : C / test_data Buildings (&quot;44) upOATE Buildings (*444): START *444 - XML : Aa &amp; " /></p><p></p><p>Save this file. It will be used in PowerBI to have the time by node.</p><p></p></th></tr></thead><tbody></tbody></table>




















