<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Test : Module Specific Knime to Python test](#test--module-specific-knime-to-python-test)
- [Introduction](#introduction)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Test : Module Specific Knime to Python test

Created: 2021-02-24 12:17:57 +0100

Modified: 2021-02-24 12:21:04 +0100

---


<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="introduction">Introduction</h1></th></tr></thead><tbody></tbody></table>

The full converter is set in the src section. Here, we only test some of the functionalities of the converter. These tests are set in the tests folder and is named test_module_converter.py. Some other tests exist but they are only set to test a specific node or functionality and not the whole process of the converter. You will find all the test codes in the tests folder.



![](../../../media/Technical-Documentation-Test---Module-Specific-Knime-to-Python-test-image1.png){width="3.4895833333333335in" height="1.7395833333333333in"}

The test_module_converter contains one test. It compares the output of Knime and Python (see *Test_current_data_processing_for_debug_excel_reader* in *Test : Knime to Python converter* for further information).

