<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Release Support](#release-support)
- [Support to _interactions data-processing module update](#support-to-_interactions-data-processing-module-update)
- [](#)
- [Support to docker configuration](#support-to-docker-configuration)
- [Support to update "Scenario Benchmarking Tool"](#support-to-update-scenario-benchmarking-tool)
  - [Configuration file](#configuration-file)
- [Xcalc library](#xcalc-library)
  - [Google Sheet tools](#google-sheet-tools)
  - [KNIME parser](#knime-parser)
  - [Scenarii Importer](#scenarii-importer)
  - [Workflow Update](#workflow-update)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Release Support

Created: 2021-03-12 11:46:34 +0100

Modified: 2021-04-08 15:35:13 +0200

---


# Support to _interactions data-processing module update

Path : KNIME2Python/scripts/interactions_modules_update.py



When we need to update all the module of '_interactions data-processing', the operation could be slow and not reliable. To circonvent this this script is offering a way to automatically udpate all shared nodes of the workflow (and his depending test workflows).



Here are the execution steps :
-   Read nodes ID in '_interactions data-processing' workflow module (see [KNIME parser](onenote:#Release%20Support&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&object-id={039ACEAA-A60C-5302-3B72-8779FDF433D4}&55&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))
-   For each node, update it (KNIME udpate of a shared node).
-   For each node, update the related test workflow

#  







# Support to docker configuration

Path : KNIME2Python/scripts/configure_config_docker.py



The configuration of docker files is being increasingly time consuming. For each release we want to be able to update scenarii for each country and to update module nodes. To do so and avoid manual manipluation (and errors) the script automate the whole process.



The configuration files are build based on templates stored at : KNIME2Python/config/templates. For the EU2050 project we have two configuration files :
-   template_config_eu2050.yml
-   Template_config_docker_eu2050.yml





Here are the execution steps :
-   Read the templates
-   Import scenarii from "Scenario builder Interne" Google Sheet (see [Scenarii Importer](onenote:#Release%20Support&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&object-id={039ACEAA-A60C-5302-3B72-8779FDF433D4}&74&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))
-   Add scenarii to configuration files
-   Read nodes ID in '_interactions data-processing' workflow module (see [KNIME parser](onenote:#Release%20Support&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&object-id={039ACEAA-A60C-5302-3B72-8779FDF433D4}&55&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))
-   Add nodes to configuration files
-   Write the configuration files









# Support to update "Scenario Benchmarking Tool"

Path : KNIME2Python/scripts/scenario_benchmarking_tool.py



To keep a track of the alignement between our scenarii (defined in our Google Sheet [Scenario Builder Interne](https://docs.google.com/spreadsheets/d/1N_RgOZicYfZ32pyxzNfFeRKBgL1dE74LkBPAmOD5oD0/edit#gid=588445613)) and the model results the [Scenario Benchmarking Tool](https://docs.google.com/spreadsheets/d/16PjYadUx4LqwTVnKEe4EhS2YEe6JsMWJRy4sYuykYLU/edit#gid=1254357624) has been designed. This is a compilation of pivot tables to allow actors to have a view on the results.



The manual update of this sheet is time consuming. The script gives the opportunity to automatically update the follow Google Sheet tab :
-   Emissions data
-   Energy data
-   Costs data



Theses tabs contains data from two sources: XCalc results (through API requests) and officials scenarii. The official scenarii are read in the following tabs:
-   Emissions official
-   Energy official
-   Costs official



An output given by the model (through API request) is called a RESULT.

The graph we are looking at (through API request) is called a DIMENSION.

The ventilation of data contained in a graph is called a VARIABLE.

The combination of levers is called a SCENARIO.



The complete update of the data is configured by a configuration file (KNIME2Python/config/config_scenario_benchmarking.yml) (see [Configuration file](onenote:#Release%20Support&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&object-id={FC18BC79-C7F3-0129-AE84-8F63CA7A2BE8}&5D&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



A template of API request is used to build the requests sent to XCalc API (KNIME2Python/config/templates/template_request.json).



Here are the execution steps:
-   Load configuration file
-   Import scenarii from "Scenario builder Interne" Google Sheet (see [Scenarii Importer](onenote:#Release%20Support&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&object-id={039ACEAA-A60C-5302-3B72-8779FDF433D4}&74&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one))
-   Read API template request
-   For each dimension :

    -   Get results from each scenarii from API

    -   Load corresponding official data

    -   Crunch the results to fit output Google Sheet format

    -   Upload the changes on Google Sheet





## Configuration file

Path : KNIME2Python/config/config_scenario_benchmarking.yml



This file define all the necessary configurations to build the updated data. Here are the sections used :


-   dimensions : list of dimensions to update in benchmarking tool

    -   For each dimension :

        -   unit

        -   variable : list of the variables corresponding to a given graph to load
-   api: configurations linked to API

    -   url: URL to access the API

    -   token : Token files names

        -   read : read only token

        -   write : edit token

    -   scopes : the scopes are defining which rights are used to access the Google Sheet

        -   /! IF YOU MODIFY THESE SCOPES : DELETE the token file

        -   read : read only scope

        -   write : edit scope
-   sheets : Google Sheets configurations

    -   spreadsheet : List of spreadsheets ID

    -   range : List of range that has to be accessed

        -   For dimension :

            -   output : write range

            -   official : read range for official data
-   map : Mapping configuration which is defining if the data are ventilated by vector or sector
-   sector mapping : Mapping between variable and displayed name

    -   For each dimension :

        -   total : This has to exist for each dimension (will be computed automatically)

        -   List of variables defined in [Dimensions : list of dimensions to update in benchmarking tool](onenote:#Release%20Support&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&object-id={FC18BC79-C7F3-0129-AE84-8F63CA7A2BE8}&C9&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)
-   vector mapping : Mapping between variable and displayed name

    -   For each dimension :

        -   total : This has to exist for each dimension (will be computed automatically)

        -   List of variables defined in [Dimensions : list of dimensions to update in benchmarking tool](onenote:#Release%20Support&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7DFAB973-C8C5-6647-B44F-39A05CFEEAF4}&object-id={FC18BC79-C7F3-0129-AE84-8F63CA7A2BE8}&C9&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)
-   scope map : Mapping between a dimension and displayed scope

    -   For each dimension :

        -   Feedstock:

        -   Bunker Av:

        -   Bunker Ma:

        -   Ambiant heat:

        -   Gases:

        -   Biomass:









# Xcalc library

Path : KNIME2Python/scripts/xcalc_lib



## Google Sheet tools

Path : KNIME2Python/scripts/xcalc_lib/google_sheets_tools.py



To allow easier manipulation of Google Sheets, two functions (a getter and a setter) are available to manipulate Google Sheets :
-   "get_sheet_data" : this function get values from a given Google Sheet
-   "set_sheet_data" : this function update values on a given Google Sheet (overwrite the given range)



## KNIME parser

Path : KNIME2Python/scripts/xcalc_lib/knime_parser.py



This class eases the operation to get node IDs of each module in a KNIME workflow. The method "parse_xml" will parse the XML and select node IDs of module.



## Scenarii Importer

Path : KNIME2Python/scripts/xcalc_lib/scenarii_importer.py



This class automatically fetches scneraii from the corresponding Google Sheet ([Scenerio Builder Interne](https://docs.google.com/spreadsheets/d/1N_RgOZicYfZ32pyxzNfFeRKBgL1dE74LkBPAmOD5oD0/edit#gid=588445613)). The data are crunched to fit the format of a docker configuration file.



## Workflow Update

Path : KNIME2Python/scripts/xcalc_lib/workflow_update.py



When we need to update all the module of a workflow, the operation could be slow and not reliable. To circonvent this this class is offering a method to automatically udpate all shared nodes of the given workflow.
