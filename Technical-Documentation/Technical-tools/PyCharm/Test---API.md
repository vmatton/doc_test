<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Test : API](#test--api)
- [Content of the API](#content-of-the-api)
  - [Build_app function](#build_app-function)
- [How to use](#how-to-use)
  - [When to use ?](#when-to-use-)
  - [How to test a request ?](#how-to-test-a-request-)
  - [How to add rested if not already installed ?](#how-to-add-rested-if-not-already-installed-)
  - [](#)
  - [Test in local or test the server ?](#test-in-local-or-test-the-server-)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Test : API

Created: 2020-04-15 09:07:41 +0200

Modified: 2021-08-10 11:30:53 +0200

---




The API is used to receive request from web users and send an appropriate response.



The API is set in the scripts folder. There is one API python code by project : ***project_name***-app.py.



![_old& drafts becalc-app.py becalc_scenarios.xlsx eliacalc• app.py eu2050•app.py googlesheet.py gtap_scenarios.py install_docker.sh interactive. py precalculate.py quickscan.py test ani result.sh vlaio-app.py ](../../../media/Technical-Documentation-Test---API-image1.png){width="2.9375in" height="3.5in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="content-of-the-api">Content of the API</h1></th></tr></thead><tbody></tbody></table>



API is build based on the build_app code (cfr import build_app from src.api.app).



![Texte de remplacement généré par une machine: ---init_.py a pp.py database.py ](../../../media/Technical-Documentation-Test---API-image2.png){width="3.5416666666666665in" height="1.1770833333333333in"}



The API use some configuration arguments to start the application. **You should precise what is the config file you want to use** (here : config_elia.ylm).

And you can decide if you want to bypass the model or not, as well as if some extra scenario are wanted.

![Texte de remplacement généré par une machine: • import import import argparse os . path warnings from src.api.app import build_app # This script allows to initialize the database • if n ame warnings . filterwarnings ( " ignore # Extract command line arguments argparse. Argumentparser() parser = parser. -c", " - -configuration" and start the API for ELICaIc. cf confi u ration file defaults tc confi / confi defauLt=os . path . join (os . path . dirname(os . path . realpath( file_)), , config' ' confi Elia. Elia. I)) t ue") parser. -nc-extra -scenarics" parser. parse_args run args = run args_dict = vars (run_args) # Load configuration, tc get parameters to run the API with open ' configuration cfg = yaml. load(f) # Run the API app = not run extra scenarics, just the first cne" dictC ccnfigur-aticn 'l, th-eaded=FaIse, a Pl pc ext -a hcs+: scenanzcs = 'a.a.a.a') not run args.no extra_scenarios) # For production 'api app.run e rue, pc ](../../../media/Technical-Documentation-Test---API-image3.png){width="8.760416666666666in" height="6.125in"}



## Build_app function

By default, bypass_model and extra_scenarios arguments are set to True).

Configuration arguments contains a lot of information (cfr blue frame). They are retrieved from the configuration file (cfr [Configuration Files](onenote:#Configuration%20Files&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={74BD6C1F-8EF9-4961-B3AC-B17C62138312}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)) :
-   What is the workspace
-   What are the output nodes that should be used
-   ...



![Texte de remplacement généré par une machine: îdef extra_scenarios : : param cfg_fiLe: configuration file path : param bypass_modeL: boot to bypass the model : param extra scenarios: boot : return: fLask application ni th endpoints # FIXME : remove the fi L ter and sot ve the python warnings instead warnings . filterwarnings ( " ignore " ) with open (cfg_file, cfg = yaml. load(f) # Parameters from configuration file workspace = cfgC 'model 'workspace ' ) output_nodes = cfgC 'model pr-cd 'cutput_ncdes ' ) model_workflow = cfg[ 'model pr-cd 'model_workflow'] max cache size = cfgC 'model ' ) = cfgC ' = cfgC after cube CcfgC 'model 'JC 'inter-pclaticn start 'l, cfg[ 'model 'JC 'interpolaticn_end country codes = cfgC api country _ codes = cfg[ ' JC 'update at start ](../../../media/Technical-Documentation-Test---API-image4.png){width="7.96875in" height="5.375in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="how-to-use">How to use</h1></th></tr></thead><tbody></tbody></table>



Run the code. Once it has properly run it return a message like this (meaning it can receive request from a web user) :



![, 「 " , 0 quit) ](../../../media/Technical-Documentation-Test---API-image5.png){width="7.6875in" height="1.7395833333333333in"}



## When to use ?

When something wrong with the website (eg. A results does not display or is out of expected ranges). When it happens, we test this specific request in local to see where does the problem come from.



## How to test a request ?

1.  Go on Firefox

2.  Click on Rested symbol (if you do not have rested, cfr section below to see how to add it)

![](../../../media/Technical-Documentation-Test---API-image6.png){width="2.09375in" height="0.9270833333333334in"}



3.  A window opens. Enter the port of your website and choose request = "POST".

    -   ***Local Host : 127.0.0.1***

    -   ***Port : 5000***



![New Tab (b RESTED Client Extension (RESTED) moz-c G etting Stu-ted EU2050 - - RESTED Collections ](../../../media/Technical-Documentation-Test---API-image7.png){width="4.177083333333333in" height="2.8958333333333335in"}



![Headers Basic m'th Request Local host pot t S 000 ](../../../media/Technical-Documentation-Test---API-image8.png){width="6.78125in" height="2.1875in"}





4.  In the Request Body : set which output variable you want to display (here two variables : agr_emissions_GHG... and dhf_emissions_GHG...)

![:utC02e , ](../../../media/Technical-Documentation-Test---API-image9.png){width="4.59375in" height="2.09375in"}



5.  In the Request Body : set what is the position of each lever (here, they are all set to 1)

![](../../../media/Technical-Documentation-Test---API-image10.png){width="8.510416666666666in" height="1.9479166666666667in"}







Request results are return like this :



1.  Asked outputs are returned with all their parameters



![200 OK Headers > -outputs": riculture••, 2aes, 2012, ](../../../media/Technical-Documentation-Test---API-image11.png){width="4.927083333333333in" height="4.333333333333333in"}



2.  KPI is always retuned even if we didn't ask for it.

![](../../../media/Technical-Documentation-Test---API-image12.png){width="3.875in" height="4.5in"}



3.  If there is no result for our request, we look into PyCharm console to see where does come the problem.

*Example : metric does not exist (is not found)*

![](../../../media/Technical-Documentation-Test---API-image13.png){width="8.302083333333334in" height="0.96875in"}









<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h2 id="how-to-add-rested-if-not-already-installed">How to add rested if not already installed ?</h2><p></p><ol class="incremental" type="1"><li><p>On the menu of Firefox, select "Add-ons"</p></li></ol><p><img src="../../../media/Technical-Documentation-Test---API-image14.png" style="width:2.94792in;height:4.89583in" alt="Texte de remplacement généré par une machine: Iil  Sign in to Firefox Privacy Protections New Window New Private Window Restore Previous Session Zoom Libraty IDO% Lo ins and Passwords Add- ans Customize... Open File... Save Page As... Ctrl+N Ctrl+Shift+P Ctrl+Shift+A Ctrl+O Ctrl+S " /></p><p></p><blockquote><p></p></blockquote><ol class="incremental" start="2" type="1"><li><p>Search "Rested", select it and add it to Firefox</p></li></ol><blockquote><p><img src="../../../media/Technical-Documentation-Test---API-image15.png" style="width:5.11458in;height:0.76042in" alt="Texte de remplacement généré par une machine: Find more add-ons rested e " /></p><p></p><p><img src="../../../media/Technical-Documentation-Test---API-image16.png" style="width:6.30208in;height:1.1875in" alt="Texte de remplacement généré par une machine: Search resutts A REST the req us Espen H users " /></p><p></p><p><img src="../../../media/Technical-Documentation-Test---API-image17.png" style="width:5.23958in;height:1.8125in" alt="Texte de remplacement généré par une machine: RESTED by Espen H A REST Client the rest of us + Add to " /></p><p></p></blockquote></th></tr></thead><tbody></tbody></table>





## 

## Test in local or test the server ?

You can test your code in local or you can directly make a request to a specific server.

Usually, while testing the API before a release, each test is made locally. The local host number for that is ***127.0.0.1*** and the port is ***5000***.

![Texte de remplacement généré par une machine: Request Headers > Basic auth > Request bodyv htep;/ 121.O.O.1:SOOO ](../../../media/Technical-Documentation-Test---API-image18.png){width="5.71875in" height="1.96875in"}





But, for some reasons, we would like to make a request directly to a specific server. In this case, change the local host number by your server Public IP number (the port number is still 5000 !) ([Amazon Web Services (AWS)](onenote:#Amazon%20Web%20Services%20(AWS)&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={0A729CE1-374D-4CC5-B587-D730CC4FDD82}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



![Texte de remplacement généré par une machine: Request Headers Basic auth > Request bodr Wpe Custom ](../../../media/Technical-Documentation-Test---API-image19.png){width="6.03125in" height="2.21875in"}



While testing your API, in the PyCharm console, you have the following indications :
-   Running on <http://0.0.0.0:5000/> : waiting for a request (port is 5000).

![Texte de remplacement généré par une machine: ni def„lt ](../../../media/Technical-Documentation-Test---API-image20.png){width="8.3125in" height="2.3541666666666665in"}






















