<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Python Node Structure](#python-node-structure)
- [Introduction](#introduction)
- [Def : __init__](#def--__init__)
- [Def : init_ports](#def--init_ports)
- [Def : build_node](#def--build_node)
- [Def : run](#def--run)
- [Def : update](#def--update)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Python Node Structure

Created: 2020-06-02 10:22:42 +0200

Modified: 2020-06-16 14:28:44 +0200

---


<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="introduction">Introduction</h1></th></tr></thead><tbody></tbody></table>



All python node are constructed based on the same structure ;
-   Initialisation (cfr __init__)
-   Init_ports
-   Build_node
-   Run
-   In some case (depends on the node) : update



All these functions are described in the following sections.



In addition, all nodes are related to a parent (called Node, cfr Figure below). Therefore, they all have the same characteristics described in the Node class (cfr [Node.py](onenote:#Converter%20Knime%20to%20Python&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={A46CDECD-F45F-4573-B211-A191CAD31398}&object-id={C556BB2B-3894-0632-1F54-93E677AFF4CB}&27&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



![Texte de remplacement généré par une machine: class RowFiIterNode(Node) : ](../../../media/Technical-Documentation-Python-Node-Structure-image1.png){width="2.3541666666666665in" height="0.4375in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="def-__init__">Def : __init__</h1></th></tr></thead><tbody></tbody></table>



This step initialise the node :
-   it defines all the local variables
-   It uses the properties of the parent class to make the initialisation (cfr super().__init__)



![Texte de remplacement généré par une machine: def self self self self self self self self self self self self self self self self (self, Sq, xml_file, . filter_type None . pattern None . case_sensitive None None None . that_column = None node_type, . deep_filtering - . type_lowerbound . t y pe_yppgçppyng None None None None None . Iower_bound None . upper_bound None . include None . start_index None . end_index None super(). _ (id, xml_file, node_type, knime_workspace) ](../../../media/Technical-Documentation-Python-Node-Structure-image2.png){width="6.03125in" height="4.083333333333333in"}



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="def-init_ports">Def : init_ports</h1></th></tr></thead><tbody></tbody></table>



Defines the number of input and output ports. This number is fixed and defined by node.



If a node offers the possibilities to add several input ports (eg. Concatenate), this is not yet implemented in the converter. Therefore, an error should be raise in the Quickscan to force the use of input/output port number as it is defined here.



Example : Row filter ; it has one input port and one output port.

![Texte de remplacement généré par une machine: def init_ports(self): . in_ports self .out_ports = {1: self None} None} ](../../../media/Technical-Documentation-Python-Node-Structure-image3.png){width="2.7916666666666665in" height="0.78125in"}





Exception is made for each module metanode. As the number of input and output ports varies from one module to another, it is not possible to have a fix number defined here. Therefore, où sont-il définis ?? ==> quand ça change, il faut le corriger manuellement



Note : with the new Knime version, it is possible to add some input/output port manually to a node (eg. Concatenate). This is not implemented in the converter so this option should not be used in Knime !





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="def-build_node">Def : build_node</h1></th></tr></thead><tbody></tbody></table>



This part of the code will read the xml file, create an "image" of the node and put it in cache to be ready when the ruin will start.



![Texte de remplacement généré par une machine: def - timer() start self .xmlns + "config model = model.find(self .xmlns + config config.find(self .xmlns + .get('value') self . filter_type if self .filter_type self . that_column ' StringComp_RowFi1ter ' config.find(self .xmlns + config. find(self.xmlns + . include self config. find(self .xmlns + self . pattern - config. find(self .xmlns + .get('value') .case_sensitive = serf config.find(self .xmlns + self . deep_filtering config.find(self .xmlns + self .ls_reg_exp - config. find(self .xmlns + . get('value') self if self .deep_filtering 'true' self . error( "Deep Filtering has not been implemented for row filtering! Node's xml file is: {Y" .format( self if self . . xml_file)) - 'true' ](../../../media/Technical-Documentation-Python-Node-Structure-image4.png){width="8.958333333333334in" height="4.510416666666667in"}





In this function are described all the exceptions and Knime functionalities that are not implemented in Python (example below). They are set as error message and do not stop the running process of the code (make it easiest while doing a release).

All these error messages arise while using quikscan (more details : [Test : Quickscan](onenote:#Test%20%20Quickscan&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={DEC080F0-83A1-4C22-9E64-5321074C4129}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).



![Texte de remplacement généré par une machine: try: = model.find(self .xmlns + .find( except AttributeError as e : self . Node . id)+" raise Exception(e) # Re ex t e of filterin if - 'name_pattern' is empty. (I' *self . id_xml+ model.find(self .xmlns + .find( entry self.xmlns + if entry .find(self.xmlns + self . 'Regex' 'Regex' entry .find(self .xmlns + . get('value') case_sensitive entry.find(self.xmlns + pattern self. attern Node. atternresha e erse : self . logger . column self.xml_file) + ")" ) "entry ' pattern' ] . get(' value attern case_sensitive self filtering is not implemented (node + str(id) + .xml file + st ( ](../../../media/Technical-Documentation-Python-Node-Structure-image5.png){width="8.947916666666666in" height="4.0625in"}





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="def-run">Def : run</h1></th></tr></thead><tbody></tbody></table>







![Texte de remplacement généré par une machine: def run(self): - timer() start self 'START') df = self . if self.filter_type - if self . try: self . . pattern] [1] self.pattern = except KeyError: # FIXME: use the flow vars in a more generic wag Row filtep is using flow variables that wepe convepted into values in the "initialisation phase of the converter. VIe are now using the value from the" "initiallisation without using flow variables. ") dftself . that_column] . dtype if dfVself.that_coIumn] .dtype.name "category": # category type does not exist in numpy dtypes self .pattern = .type(self .pattern) if self.deep_fiutering 'tpue•: raise Filtering has not been implemented for # Cast attern into the ri ht row filtering! Node's xml file e for com arison .form t(self .xml_file)) ](../../../media/Technical-Documentation-Python-Node-Structure-image6.png){width="10.0625in" height="4.09375in"}







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="def-update">Def : update</h1></th></tr></thead><tbody></tbody></table>



![Texte de remplacement généré par une machine: def update(self): timer() start = self. log_timerCNone, self No need to check for existence, we can use propertV directlv df = df . take(self . axis=l) self . logger - timer() - start self . ](../../../media/Technical-Documentation-Python-Node-Structure-image7.png){width="8.104166666666666in" height="2.7916666666666665in"}









