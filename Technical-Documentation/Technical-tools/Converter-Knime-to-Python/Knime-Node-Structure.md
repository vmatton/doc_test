<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Knime Node Structure](#knime-node-structure)
- [Introduction](#introduction)
- [Xlm Files](#xlm-files)
  - [Where to find it ?](#where-to-find-it-)
  - [How it is converted in python ?](#how-it-is-converted-in-python-)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Knime Node Structure

Created: 2020-06-15 10:11:34 +0200

Modified: 2020-06-15 13:50:29 +0200

---


<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="introduction">Introduction</h1></th></tr></thead><tbody></tbody></table>



Knime workflow are written in xlm files. Therefore, for each node and/or workflow created, there will be a corresponding xlm file.

These files are read in the converter.





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="xlm-files">Xlm Files</h1></th></tr></thead><tbody></tbody></table>



The xlm file structure is the same for each node that have the same type. Therefore, it is quite easy to convert one node in python as the xlm structure will not change from one node to another one of the same type.



**Note : if there is an update of Knime, the xlm file structure could change ! This could avoid the converter to run !**

**Therefore, before using a new version of Knime, it is necessary to test each python node with this new version (time consuming !) on one computer only (keep another one available for the release process !) and when everything is alright apply the change of Knime version to all users !**



## Where to find it ?



On local, choose a node inside the folder "module_name" (eg. Agriculture) > metanodes > ...

Inside each node folder (eg. Here Column Filter - id 3098), you will find a settings.xml file.



![Texte de remplacement généré par une machine: xcalc dev agriculture m Eta Nom 3. lb Industry Ammonia 8ECaIc Dat (#1356) settings.xml Column Filter Modifié le 16-01-20 11:02 ](../../../media/Technical-Documentation-Knime-Node-Structure-image1.png){width="7.927083333333333in" height="1.4375in"}









## How it is converted in python ?



In this section, we just give an example to see how a xml file could be used in python. More details are found here : [Python Node Structure](onenote:#Python%20Node%20Structure&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={3DB91899-84BB-4B6E-B8DC-CBDF3E92791C}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



**Example : Column Filter node.**



In the build function, we are looking for a config with "model" as key (cfr red frame 1). If it exists, we will look if there is, inside this config, a config with "column-filter" as a key and inside it is there is an entry with "filter-type" as a key. If yes, we will get the value of this entry.



![Texte de remplacement généré par une machine: def start = timer() model = .xmlns + try: type_of_filter model.find(self.xmlns + find( ' filter-type'] I') . get(' value') self . xmlns except AttributeError as e : self . logger . Node .id)+" is empty. ("+self .id_xml+")") raise Exception(e) ](../../../media/Technical-Documentation-Knime-Node-Structure-image2.png){width="7.708333333333333in" height="2.4791666666666665in"}



In the xlm file, we find a config with key = model, inside there is a config with key = column-filter and the value for the config (with key = filter-type) is "STANDARD". This value is saved in the python variable named "type_of_filter" (cfr previous image).

![](../../../media/Technical-Documentation-Knime-Node-Structure-image3.png){width="10.729166666666666in" height="7.34375in"}











