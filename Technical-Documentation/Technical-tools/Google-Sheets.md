<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Google Sheets](#google-sheets)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Google Sheets

Created: 2020-04-15 08:54:08 +0200

Modified: 2020-05-06 17:11:12 +0200

---

|--------------|

Google sheets are used to several purpose. For each project (even if there are based on XCalc) create a google sheet folder. It will contains, at least, four google sheets :


-   One or more sheet(s) for data collection (cfr *User Manual > Google Sheet > Data Collection*) for more information about these pages)
-   One sheet for data calibration (cfr *User Manual > Google Sheet > Data Calibration*) for more information about this page)
-   One sheet for interface
-   One sheet for scenario builder



As data collection and data calibration were already be seen on the *User Manual* section, only interface and scenario builder are explored in this section.


















