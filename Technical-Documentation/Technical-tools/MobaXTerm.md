<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [MobaXTerm](#mobaxterm)
- [Install](#install)
- [Using MobaXTerm](#using-mobaxterm)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# MobaXTerm

Created: 2020-04-15 09:09:51 +0200

Modified: 2020-08-18 14:46:43 +0200

---




Each time we modify our data, interface and so on, we need to update dockers on our remote Linux server. In our case, Linux servers are servers without a "win" suffix in their name (more information on [Amazon Web Services (AWS)](onenote:#Amazon%20Web%20Services%20(AWS)&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={0A729CE1-374D-4CC5-B587-D730CC4FDD82}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).

MobaXTerm is used to easily work on these remote Linux servers.





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="install">Install</h1></th></tr></thead><tbody></tbody></table>



Go to : <https://mobaxterm.mobatek.net/download.html>. Choose the free version.

![1. Download the Home Edition of MobaXterm. Make sure to select the Installer edition rather than the Portable edition. 2. Click on the downloaded zip file to open it, and then click on the MobaXterm installer msi file to begin the installation. 3. Once the install has finished, open the MobaXterm app. In the main window, you will see a list of saved sessions in the left-hand column. If you have previously installed and used PuTTY, any saved PuTTY sessions will also be listed. 4. From here, you will want to start a local terminal and install nano. You can start a local terminal by clicking on the "Start local terminal" button in the MobaXterm main window. ](../../media/Technical-Documentation-MobaXTerm-image1.png){width="7.15625in" height="2.75in"}



**Further steps** : go to [Amazon Web Services (AWS)](onenote:#Amazon%20Web%20Services%20(AWS)&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={0A729CE1-374D-4CC5-B587-D730CC4FDD82}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) (section : "How to open each server -- For Linux Server").



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="using-mobaxterm">Using MobaXTerm</h1></th></tr></thead><tbody></tbody></table>



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>Before working on a sever, make sure it has been started (for more details :</strong> <a href="onenote:#Amazon%20Web%20Services%20(AWS)&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={0A729CE1-374D-4CC5-B587-D730CC4FDD82}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Amazon Web Services (AWS)</a><strong>).</strong></p><p></p><p><strong>If you are working on a new project</strong>, make sure a AWS server was created for this purpose.</p><p></p></th></tr></thead><tbody></tbody></table>



When you open MobaXTerm, all the servers you already worked on it appears on the left frame (cfr "session"). If the server you want to work on it is not there, cfr AWS Section to see how to open a Linux server.

![MobaXterm Terminal Sessions Session Servers View Tools X server Tools Games Settings Macros Help Games Sessions Vie n Split MulbExec Tunneling Packages Settings Help Quick connect... user sessions aecalc4WS(vvebtooI) Vlaio•ws ßonerbi) ](../../media/Technical-Documentation-MobaXTerm-image2.png){width="6.4375in" height="4.208333333333333in"}



To use a server, choose it on the "User session" frame and double click on it. A command line appears on the right side. On the left side, you are redirected to the sftp section that indicates you all the folders that are set in your server.

![](../../media/Technical-Documentation-MobaXTerm-image3.png){width="8.5625in" height="4.395833333333333in"}









<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>We always work with the knime2python folder (all the needed files are there).</strong></p><p><strong>So first thing to do consists in changing directory to be on this folder (cfr cd eucalc / knime2pyton)</strong></p></th></tr></thead><tbody></tbody></table>







| Which files are read in command lines ? |
|-----------------------------------------|

Files read with command lines are :
-   Configuration files : cfr [Configuration Files](onenote:#Configuration%20Files&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={74BD6C1F-8EF9-4961-B3AC-B17C62138312}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more details
-   DockerFiles : cfr [Dockers](onenote:#Dockers&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={85EEF5DB-52C1-40CA-856A-338C31D0E7C7}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more details

These files relied on python codes. They are described in this section : [PyCharm](onenote:#PyCharm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={639AC9EA-A995-42D6-A3CC-782AF1E25F1F}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one).





| How to update a project ? |
|---------------------------|

Once all your modifications were done (cfr all the steps here : [Updating existing project](onenote:#Updating%20existing%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={687D9522-BD6D-49C2-8DF3-23154AA366D9}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)), you need to update your server. This is done with the following steps (cfr the corresponding command lines below) :
-   Get the latest version of knime2python folder from bitbucket (if modified should have been pushed on bitbucket before)
-   Build a docker for the db
-   Build a docker for the model
-   Delete mysql and build it again



![Update server update knime2python folder update api db docker (pon 5001) Lags update the version vx_x with the correct version and the docker-compose.yml file accordingly ---build---arg SSH (cat " ---C cL±macC/ZecaLc: update api model docker (port 5000) update the version --- ---Z u ± Ld---arg with the correct version and the docker-compose_yml file accordingly MCDEL SSE EEY="; (cat " ---C climact/bacalc: mode L docker---czmpzse up ---d Lags reset mysql - tind the mysql_id using mysgl command => to do only when data or model changes (no need to reset mysql if it is only 'cosmetical' changes) ](../../media/Technical-Documentation-MobaXTerm-image4.png){width="10.8125in" height="3.5625in"}

**Note** : no need to pull _interactions. Its is done in the dockerfile (cfr [Dockers](onenote:#Dockers&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={85EEF5DB-52C1-40CA-856A-338C31D0E7C7}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one) for more informations).



**Note** : Container know which image is has to encapsulate thanks to the docker-compose.yml file ([Dockers](onenote:#Dockers&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={85EEF5DB-52C1-40CA-856A-338C31D0E7C7}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)). Usually, when we build a container, the image is also rebuilt. But it appears sometimes that the image used by the containers is not an updated image version but the last version it has run (as image name has not changed in the docker-compose.yml, it does not always get when it has to update the image or not).

When building dockers for api_db and api_model, if they run very quickly that means they use the latest image version. In this case, we should delete the last image version and then create the new one to be sure docker will use the appropriate image to build its containers.

For this purpose :
-   Docker-compose down (stop all dockers that are running)
-   Docker rmi image_id (remove an image of "mysql" based on its id)
-   Docker build (...) (rebuild the docker as done in command lines above)
-   Docker-compose up -d (create a docker by image as defined in the configuation file (cfr [Configuration Files](onenote:#Configuration%20Files&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={74BD6C1F-8EF9-4961-B3AC-B17C62138312}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)) and run them)





What does that means (explanation of each command lines)?



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p>Update api db docker and model docker</p><p></p><p>db docker and model dockers are quite similar (cfr <a href="onenote:#Dockers&amp;section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&amp;page-id={85EEF5DB-52C1-40CA-856A-338C31D0E7C7}&amp;end&amp;base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one">Dockers</a>). Therefore, the way we create their image is also similar.</p><p>Differences come from the names we used for the version tag, the docker file used and the docker name (cfr yellow below).</p><p></p><p><img src="../../media/Technical-Documentation-MobaXTerm-image5.png" style="width:9.8125in;height:1.70833in" alt="update api do do:ker (port 5001) - update theversionvx_xwitn me correctversionand update agi model docker (port 5000) • " /></p><p></p><p><strong>First command line</strong> : <em>build an image from a dockerfile (more details on : <a href="https://docs.docker.com/engine/reference/commandline/build/">https://docs.docker.com/engine/reference/commandline/build/</a>)</em></p><ul class="incremental"><li><p><strong>Docker build .</strong> : we build a docker based on a file</p></li><li><p><strong>-f Dockerfile_api_db</strong> : indicates where to find teh file.</p></li><li><p><strong>--build-arg MODEL_VERSIO</strong>N=master : set which argument (named MODEL_VERSION) should be used (here : master, but we could used, vlaio or other branch depending on the project we are working on)</p></li><li><p><strong>--build-arg SSH_KEY="$(… /docker_build_rsa)"</strong> : set where is the repository for the SSH key (this repository is inside the docker and is not accessible somewhere else). Here repository is named docker_build_rsa.</p></li></ul><blockquote><p><img src="../../media/Technical-Documentation-MobaXTerm-image6.png" style="width:3.875in;height:3.625in" alt="ome/ec2-user/ config bash _histor y bash dog out bash _profile bashrc docker build rsa Size (KB) Last modified 2019-02-27 2020-02-05 2019-02-27 2020-04-08 . 2019-02-27 2020-03-03 2020-02-14 2019-04-10 . 2020-04-06 2020-04-09 2018-07-27 2018-07-27 2018-07-27 2020-04-09 . 2020-01-17 " /></p><p></p></blockquote><ul class="incremental"><li><p><strong>-t climact/becalc:version_db</strong> : tag the image (cfr –t command). The repository name will be climact/becalc folder and the tag will be <strong>version_db</strong></p></li></ul><blockquote><p></p></blockquote><table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><em>How to choose an appropriate <strong>version</strong> ?</em></p><p>If only data values have chaned : keep the same version than the last one. Otherwise, change the version number. There are no specific rules for the number to use yet.</p><p></p><p><em>How are used the –build-arg ?</em></p><p>Build-arg are values that are linked to arguments defined in the dockerfile (work like a function).</p><p>In the dockerfile, we defined arguments (MODEL_VERSION and SSH_KEY) and used them to make the dockerfile run properly (cfr <a href="onenote:#Dockers&amp;section-id={dbbca033-3622-43f0-af9d-c2c20d95d3c8}&amp;page-id={12b2d252-4779-44aa-90a6-1947318b5e6a}&amp;end">Dockers</a> (<a href="https://climact.sharepoint.com/sites/PROSPECTIVE/_layouts/15/Doc.aspx?sourcedoc=%7b60844ae0-fb83-4ccc-b7b6-d425c90ac8f0%7d&amp;action=edit&amp;wd=target%28Technical%20documentation.one%7Cdbbca033-3622-43f0-af9d-c2c20d95d3c8%2FDockers%7C12b2d252-4779-44aa-90a6-1947318b5e6a%2F%29&amp;wdorigin=703&amp;wdpreservelink=1">Affichage web</a>)). MODEL_VERSION has already a "default value" (set to master), but in the commadn lines, we precise the real value to be used (ex. In dockerfile, MODEL_VERSION is always master, but with the command line we can decide if we really want to use the master or if we want to use another branch.</p><p></p></th></tr></thead><tbody></tbody></table><p></p><p><strong>Second command line</strong> : <em>create a container for the previously built image (more details on : <a href="https://docs.docker.com/compose/reference/up/">https://docs.docker.com/compose/reference/up/</a>)</em></p><ul class="incremental"><li><p><strong>Docker compose-up</strong> : create docker and run it</p></li><li><p><strong>-d :</strong> detached mode ; run containers in the background. Therefore, if you want to see what is the status of your docker, use log command line (cfr below in the "main command lines" section and in third command lines).</p></li></ul><p></p><p><strong>Third command line</strong> : <em>fetch the logs of a container (more details on : <a href="https://docs.docker.com/engine/reference/commandline/logs/">https://docs.docker.com/engine/reference/commandline/logs/</a>)</em></p><ul class="incremental"><li><p><strong>Docker logs name_of_the_containers</strong> : fetch the logs of the container named <strong>name_of_the_containers</strong> (to find the name of your container, cfr below in the "main command lines" section)</p></li><li><p><strong>-f</strong> : follow log output</p></li></ul><p></p></th></tr></thead><tbody></tbody></table>



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p>Reset mysql</p><p>Mysql is defined in the configuration file ; there is no specific dockerfile for it (&lt;&gt; api_db and api_model presented above). This docker contains all the scenarii that have been preloaded (1) or been asked with a request on the web application (2).</p><p>If update consists only in changes of "cosmectical" apsects of the web application, no need to delete mysql !</p><p>If update consists in model change (that implies modifications in output values), mysql should be reset !</p><p><img src="../../media/Technical-Documentation-MobaXTerm-image7.png" style="width:2.42708in;height:0.75in" /></p><p></p><p><strong>First command line</strong> : docker stop docker_name : stop running this docker.</p><p><strong>Second command line</strong> : docker rm docker_name : delete this docker</p><p><strong>Third command line</strong> : docker rmi image_id_of_mysql : delete the image based on its id (to find the id of your image, cfr below in the "main command lines" section</p><p><strong>Fourth command line</strong> : docker-compose up –d (cfr api db and model docker, above)</p><p></p><p><strong>Note</strong> : as to delete mysql we need to stop each docker, it is better to do it at the end. First we build each docker, and only after that we stop each docker, delete mysql and start again each docker. This avoids to have dockers stopped for a long time and so the website down for a long time as well.</p><p></p></th></tr></thead><tbody></tbody></table>







| Main command lines |
|--------------------|



How to get the name of your dockers ?

Use the **docker containers ls** command. Only running docker are shown.

Here, we can see that :
-   Container for api_model (name = knime2python_api_model_1) is set on port 5000 and **use image climact/becalc:v5.3**
-   Container for api_db (name = knime2python_api_db_1) is set on port 5001 and **use image climact/becalc:v5.3_db**
-   Container for mysql (name = knime2python_db_1) is set on port 3306 and **use image climact/mysql:5.7**



![[ec2-user@1p TNTAINER ID ia49b6425522 nca58f865d6 n)7979e096ed L ecz -us e rap -172-31-13-150 docker container Is IMAGE COMAND climact/becalc : v5 .3 "gunlcorn src.apl.apm" climact/beca1c:v5.3 db "gunlcorn •src.apl.am" mysq1:5.7 "docker-entrypoint • sm" -172 -31-13-156 CREATED 3 days ago 3 days ago 6 days ago STATUS Up 3 days Up 3 days Up 6 days PORTS 0.0.0.€: O. O. O. O: 5001->5001/tcp 33€6€/tcp NAMES knime2python_api_modeI_1 knime2python_api db 1 knime2python db ](../../media/Technical-Documentation-MobaXTerm-image8.png){width="12.21875in" height="1.1458333333333333in"}



How to get the id of an image ?

Use the **docker images** command. All images that have already been created are shown.

![docker Images REPOSITORY c Imact/ eca c eca c climact/becalc climact/becalc climact/becalc TAG db v5.4 5.7 vs.3 v5.3 db vs.2 v5.2 db PY on python python 813478442400 813478442400 813478442400 [ec2-user@ip . dkr. ecr. eu- cent ral -1 . amazonaws.com/eucalc . dkr. ecr. eu- central -1 . amazonaws.com/eucalc . dkr. ecr. eu -wes t -3 . amazonaws.com/eucalc_db -172 -31-13-150 db IMAGE ID c37c615acgf5 413be204egc3 8571f7ee3e9c 92a2285be3b3 368cd7efeb74 dfaaf7fd43f4 d3ae39a2a3a1 55b4e40b95fc b75ddbd45c17 off3adof6056 off3adof6056 off3adof6056 CREATED 42 hours ago 42 hours ago 9 days ago 9 days ago 9 days ago 9 days ago 9 days ago 6 weeks ago 2 months ago 11 months ago 13 months ago 13 months ago 13 months ago SIZE 1.616B 1.61GB 456MB 1.566B 1.566B 1.566B 1.566B 174MB 174MB 138MB 791MB 791MB 791MB ](../../media/Technical-Documentation-MobaXTerm-image9.png){width="9.28125in" height="2.28125in"}



How to analyse the status of a docker ?

Use the **docker logs docker_name_to_be_logged** command.

![](../../media/Technical-Documentation-MobaXTerm-image10.png){width="7.8125in" height="3.0833333333333335in"}



Stop several dockers at the same time

**$docker stop *NAME1 NAME2* ...** (set asmany names as needed, NAME1 and NAME2 are docker name, cfr how to get dockers names section).



Delete several dockers at the same time

**$docker rm *NAME1 NAME2* ...** (set asmany names as needed, NAME1 and NAME2 are docker name, cfr how to get dockers names section).



How to open a file with command line and change its content ?

1.  Open the file using the vim command. You should set the directory to this file.

![](../../media/Technical-Documentation-MobaXTerm-image11.png){width="7.15625in" height="0.25in"}



2.  Press "I" to change the file (your are in insert mode). Insert mode is precised as followed :

![file: api.log apl: port: 5000 country _ codes : brussel: belgium: BE "landers: VL wallonia: WL rest of the wor INSERT ](../../media/Technical-Documentation-MobaXTerm-image12.png){width="1.6875in" height="2.5833333333333335in"}



3.  Do your changes and then quit the insert mode by pressing ESCAPE (esc) key.

4.  Save and quit this file with the **:wq** command

![](../../media/Technical-Documentation-MobaXTerm-image13.png){width="1.5625in" height="0.4895833333333333in"}





When it has finished to run, it shoud look like :

![Texte de remplacement généré par une machine: 3333333333333333333333333333333233333333333333333 ](../../media/Technical-Documentation-MobaXTerm-image14.png){width="6.302083333333333in" height="1.0in"}





| Tips and tricks |
|-----------------|



**Find a specfic command already use** :

As in Linux and command lines, use CTRL+R, enter word you are looking for, and then do CTRL+R to go back in command history until you find the proper command.



**Show history** :

Enter "history" in command line.
















