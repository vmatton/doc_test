<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Webtool](#webtool)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Webtool

Created: 2020-04-15 12:15:43 +0200

Modified: 2021-08-03 17:21:10 +0200

---


>< PowerBI : pas tous les graphs disponibles car dépend de la librairie APEX/React

>< PowerBI : Graphes ne sont pas connectés entre eux lorsqu'ils ont les mêmes métriques (donc ne réagissent pas à sélection dans un graph ??)

Certains sceanrio sont déjà loadé (prédéfini) : lesquels? Les mêmes que PowerBI ? Où et comment les défini-t-on ?



Ici pour mettre en remote, on utilise GitHub et non bitbucket (si toujours d'actualité : faire tuto là-dessus aussi !!)



Note : mise en page du site Web point de vuedéfinition des leviers, ... reprend la même mise en page que l'Excel (ex. saut à la ligne).



Structure :

![Levers Display Map control Map display index.html > app.js > main.jsx > levers > map control > map display > header.jsx > footer.jsx ](../../media/Technical-Documentation-Webtool-image1.png){width="8.072916666666666in" height="3.9479166666666665in"}





Questions :
-   Région = désactivée ==> comment les activer ?
-   Quand régions n'ont pas de données, données non validées, ... ==> ajouter un comment "data not corrected" ?
-   Warnings : à mettre ou déjà fait !!!!
-   Comment faire realease point de vue pratique ? On prévient tout le monde ... ?
-   Dash sign in colum name : interpreté comme des "moins" : toujours d'actualité ? ==> sinon, à mettre dans les best practices de Knime mais ça change tout !!
-   Help : avoir du texte qui précise ce que veut dire 1, 2, 3, 4 dans les niveaux de leviers : fait ? Normalement oui, mais dans PowerBI aussi ?

