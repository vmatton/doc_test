<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Converter Knime to Python](#converter-knime-to-python)
- [Introduction](#introduction)
- [Config](#config)
- [Db and Tests](#db-and-tests)
- [Scripts](#scripts)
- [Src](#src)
  - [Folder API](#folder-api)
  - [Folder Nodes](#folder-nodes)
  - [](#)
  - [](#-1)
    - [Node.py](#nodepy)
    - [Node_builder.py](#node_builderpy)
    - [File for each node](#file-for-each-node)
  - [](#-2)
  - [](#-3)
  - [](#-4)
  - [Folder Workflow](#folder-workflow)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Converter Knime to Python

Created: 2020-04-15 09:09:05 +0200

Modified: 2020-07-28 09:40:41 +0200

---


<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="introduction">Introduction</h1></th></tr></thead><tbody></tbody></table>



The overall structure is given in [How to use](onenote:#PyCharm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={639AC9EA-A995-42D6-A3CC-782AF1E25F1F}&object-id={8A39882E-E329-09CA-2C50-7FD2C8709F85}&4F&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one). In this section, we will analyse in details what is the content of each folders (src, scripts, ...) and show what is the common structure of each node.



**We use PyCharm to modify and test the converter (cfr [PyCharm](onenote:#PyCharm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={639AC9EA-A995-42D6-A3CC-782AF1E25F1F}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)).**





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="config">Config</h1></th></tr></thead><tbody></tbody></table>



The config folder contains all the configuration files ; two by project (one for the development in local and one for the remote server).

More information here : [Configuration Files](onenote:#Configuration%20Files&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={74BD6C1F-8EF9-4961-B3AC-B17C62138312}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="db-and-tests">Db and Tests</h1></th></tr></thead><tbody></tbody></table>



These two folders are fully described here : [How to use](onenote:#PyCharm&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={639AC9EA-A995-42D6-A3CC-782AF1E25F1F}&object-id={8A39882E-E329-09CA-2C50-7FD2C8709F85}&4F&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="scripts">Scripts</h1></th></tr></thead><tbody></tbody></table>







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="src">Src</h1></th></tr></thead><tbody></tbody></table>



This folder contains three folders :
-   **Api** : all codes useful to interact with web users
-   **Nodes** : all codes needed to convert Knime node in python
-   **Workflow** : all codes needed to read and run a workflow



![Texte de remplacement généré par une machine: nodes workflow _init_.py ](../../media/Technical-Documentation-Converter-Knime-to-Python-image1.png){width="1.8020833333333333in" height="1.0625in"}





## Folder API



This folder contains all the codes needed for the API. It has been mainly made by Amaury Anciaux.

A terme : le décrire !!





## Folder Nodes



This folder contains several files :
-   Node.py
-   Node_builder.py
-   A file by Knime node (used in converter)



![Texte de remplacement généré par une machine: meta_node.py metaconnector_node.py mvsal connector node.ov node.py node_builder.py pivoting_nade.py _node.py python_l _2_node.py _node.py ](../../media/Technical-Documentation-Converter-Knime-to-Python-image2.png){width="3.96875in" height="3.3854166666666665in"}

## 

## 

### Node.py

It is used as the parent class of each node.

==> décrire un peu plus



### Node_builder.py







### File for each node

Each node is constructed with the same python structure. More details here : [Python Node Structure](onenote:#Python%20Node%20Structure&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={3DB91899-84BB-4B6E-B8DC-CBDF3E92791C}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



## 

## 

## 

## Folder Workflow



As workflows are made in Knime and we need to convert them in code to create a web application, a converter was created.

This converter is called with a build and a run function that looks like :

![Texte de remplacement généré par une machine: wf = workflow node_path, but Ld = timer( sel f. logger. info graph, global_vars = " . (START) , ' t = tlmer start self. logger. info(" { wf. build() :aas}: { :5s} " . format( # RUN lobal vars = * *global vars, runner = WorkflowRunner(graph, self. logger. run_output runner self. logger. info(" { . run (catpat :aas}: { :5s} * * levers sition} global_vars ) ". format( START n cde s ) . format( "BUILD (END)", ' "'RUN (END)", " t)) t)) ](../../media/Technical-Documentation-Converter-Knime-to-Python-image3.png){width="8.3125in" height="3.4791666666666665in"}



WorkflowBuilder and WorkflowRunner are defined in the src / workflow folder :

![Texte de remplacement généré par une machine: nodes workflow _init_.py workflow_builder.py worHIow_runner.py ](../../media/Technical-Documentation-Converter-Knime-to-Python-image4.png){width="3.15625in" height="1.7395833333333333in"}



Overall, all codes linked to the converter are in this src folder.







Quand nœud mis à jour ==> le changer aussi dans les nœuds python



![Texte de remplacement généré par une machine: : DataReader S metanode ML • C: ERROR DEBUG DEBUG The template of the BUILD ( START EUCarc DataReader FTS metanode was DataReader FTS metanode #539 DataReader OTS metanode #542 - XML : - XML : updated. Please check the version that you're using in Dat Dat ](../../media/Technical-Documentation-Converter-Knime-to-Python-image5.png){width="10.979166666666666in" height="0.75in"}



A METTRE DANS MOBAXTERM : quand api_db va trop vite ==> utilise des cache (l'autre api_model = ok) ==> ne va pas ; qu'est-ce qu'il faut faire ??

![Texte de remplacement généré par une machine: ec2-user@ip-172-31-13-150 knime2python]$ docker build ;ending build context to Docker daemon 222. 7MB ;tep 1/21 : FROM python:3.6-s1im 51fbad121fdb ;tep 2/21 : ARG MODEL VERSION=master Using cache 3414aad3f65a 3 21 : ARG SSH KEY Uslng cac e 5782822e165b ORKD /eucalc Using cache 40c29f955b48 UN PIP install -upgrade setuptools ep Using cache e8gasad1acd4 COPY requi rements . txt ;tep 6/21 Using cache 8df14f2b1a51 ;tep 7/21 : RUN pip install -r requi rements .txt Ocke Using cache 85d368d26fd8 COPY ./scripts ./scripts ;tep 8/21 Using cache 6c9dfe94a01e ;tep 9/21 COPY ./src ./src Using cache 5120839807b6 COPY ./config ./config ;tep 10/21 Using cache edfc7167ef3a ;tep 11/21 : RUN apt-get update && Using cache 3ea497851356 ;tep 12/21 : RUN mkdir -p /root/ .ssh Using cache apt-get install -y git chmod 0700 / root/ .ssh && -build-arg MODEL VERSION=masteI ssh-keyscan bitbucket.org > /root/ .s! ](../../media/Technical-Documentation-Converter-Knime-to-Python-image6.png){width="9.041666666666666in" height="5.75in"}






