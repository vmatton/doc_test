<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Add HTTPS (Letsencrypt)](#add-https-letsencrypt)
- [To add a site](#to-add-a-site)
- [SSL](#ssl)
- [Wildcard certificate with Gandi (XCalc)](#wildcard-certificate-with-gandi-xcalc)
- [Other informations](#other-informations)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Add HTTPS (Letsencrypt)

Created: 2021-07-05 07:56:05 +0200

Modified: 2021-09-20 08:39:58 +0200

---


Let's Encrypt : A nonprofit Certificate Authority providing TLS certificates to 260 million websites. (<https://letsencrypt.org/>)



We use this authority to authenticate BeCalc and Xcalc site.



It uses Certbot to get the certificates and renew them. (<https://certbot.eff.org/>)





# To add a site



1.  Créer le site NGINX (avec HTTPS mais sans lien vers les clés SSL)

    a.  Create files following file : */etc/nginx/sites-available/[URL].conf*

    b.  Go to sites-enabled : *cd ../sites-enabled*

    c.  Create symbolic link in sites-enabled : *sudo ln -s ../sites-available/[URL].conf [URL].conf*

    d.  Reload : *sudo service nginx reload*

```{=html}
<!-- -->
```
1.  (Ou voir en dessous pour wildcard) Créer les clés SSL avec
    *sudo certbot certonly --webroot -d becalc-detailed.netzero2050.be --email it-*itadmin@climact.com *-w /var/www/_letsencrypt -n --agree-tos --force-renewal*

2.  Ajouter les clés dans la configuration du site NGINX
    e.g. :

# SSL

ssl_certificate /etc/letsencrypt/live/pathwaysexplorer.com/fullchain.pem;

ssl_certificate_key /etc/letsencrypt/live/pathwaysexplorer.com/privkey.pem;

ssl_trusted_certificate /etc/letsencrypt/live/pathwaysexplorer.com/chain.pem;



4.  Reload NGINX : *sudo service nginx reload*



# Wildcard certificate with Gandi (XCalc)



Follow these instructions : <https://github.com/obynio/certbot-plugin-gandi>



Install command: *sudo /opt/certbot/bin/pip install certbot certbot-nginx*



Command to execute : *sudo certbot certonly --authenticator dns-gandi --dns-gandi-credentials /etc/letsencrypt/gandi/gandi.ini -d *.pathwaysexplorer.climact.com -d pathwaysexplorer.climact.com*



You should always reload the nginx after changes in certbot : *sudo service nginx reload*



API key : ZK6hlfCDdas30GlbMJmx1h2t

ShareID : e13d67d8-2f3d-11e7-b28c-00163ec31f40



To get API key from Gandi website : User settings > Manage the user account and security settings > Security > Generate API key (current : ZK6hlfCDdas30GlbMJmx1h2t)



To get shareid from API : <https://api.gandi.net/docs/organization/#get-v5-organization-organizations>

Use RESTED



![](../../../media/Technical-Documentation-Add-HTTPS-(Letsencrypt)-image1.png){width="5.0in" height="2.0833333333333335in"}



Response



![" id": "e13d67d8-2f3d-11e7-b28c-øø163ec31f40" , " email" : "hdm@climact.com" , "type": "company" , " reseller" : false, "orgname ' 'CLIMACT scri", " lastname": "de Meulemeester" , "vat number : ' " 'BEØ892272118" , "firstname": "Hugues" , "name": "tgilon", "name . "CLIMACT san, "corporate": false "type": "individual", " ema i l" : "tgi@climact.com" , " id": "6b4e1d9e-c857-11eb-aØfØ-ØØ163e9db362" , " reseller" : false, "corporate": false ](../../../media/Technical-Documentation-Add-HTTPS-(Letsencrypt)-image2.png){width="5.0in" height="2.6666666666666665in"}



# Other informations



How to delete a certificate : <https://mhagemann.medium.com/correct-way-to-delete-a-certbot-ssl-certificate-e8ee123e6e01>



Creation of a wildcard certificate : <https://computingforgeeks.com/using-letsencrypt-wildcard-certificate-nginx-apache/>



(not used - should be automatically renewed) To enable auto renewal (DNS wildcard) : <https://developerinsider.co/how-to-create-and-auto-renew-lets-encrypt-wildcard-certificate/>


