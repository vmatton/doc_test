<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Common debugging steps](#common-debugging-steps)
- [General debugging in Knime](#general-debugging-in-knime)
- [Errors while running Quickscan](#errors-while-running-quickscan)
- [Errors while running _interactions](#errors-while-running-_interactions)
- [Errors while testing Knime to Python converter](#errors-while-testing-knime-to-python-converter)
- [Errors while testing the API](#errors-while-testing-the-api)
- [Errors while releasing with MobaXTerm](#errors-while-releasing-with-mobaxterm)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Common debugging steps

Created: 2020-04-15 08:35:49 +0200

Modified: 2021-03-26 20:53:15 +0100

---




The whole process to go from Input Data (Google Sheets) to a Web Application or a PowerBI looks like :

<table><colgroup><col style="width: 45%" /><col style="width: 54%" /></colgroup><thead><tr class="header"><th><p><img src="../media/Technical-Documentation-Common-debugging-steps-image1.png" style="width:5.61458in;height:3.125in" alt="Texte de remplacement généré par une machine: GOOGLE SHEETS Module I Module N Input data : - Historical - Levers - Constants KNIME File Reader File Reader Column Filter Column Filter xlm files interactions Module workflows " /></p><p></p></th><th><p><img src="../media/Technical-Documentation-Common-debugging-steps-image2.png" style="width:6.71875in;height:3.01042in" alt="Texte de remplacement généré par une machine: PYTHON Tests : : read xim files - Knime nodes used are available in the converter - Output values are the same in Knime and the converter - Output values are the same for two runs of a unique scenario - Website shows properly the results If yes to all : realease REMOTE LINUX SERVER MOBAXTERM ocker CONVERTER ocker Knime (xml) —+ Python ocker WEBSITE POWERBI " /></p><p></p></th></tr></thead><tbody></tbody></table>

In each steps, errors can be set and that will block the converter and the release of the web tool / PowerBI.



Before making a release, we test the converter with different codes (cfr dark blue frame above). Many errors will be detected there. We summarize some of them here. Therefore, it is not an exhaustive list of all the cases that could occurs.





# General debugging in Knime



a.  When push a workflow, sometimes it does not work because files are open on local desktop (ex. Input csv file). Close all these files and restart the push process.



b.  Check reset is properly done (sometimes, while using general reset, it does not reset all the nodes...). To be sure : **CRTL + A (select all node) and then "reset selected nodes"**

![Help excel •0 0 00 air_pollution Reset selected nodes ](../media/Technical-Documentation-Common-debugging-steps-image3.png){width="5.0in" height="0.75in"}









<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="errors-while-running-quickscan">Errors while running Quickscan</h1></th></tr></thead><tbody></tbody></table>



**Quickscan should be always run before updating _interactions with any metanodes !**

**More information :** [Test : Quickscan](onenote:#Test%20%20Quickscan&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={DEC080F0-83A1-4C22-9E64-5321074C4129}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



Errors rise up when some nodes are used in a metanodes and are not converted in the converter.

Logs files are written to indicate in which modules there is any problem and what the problem is. Send them to the owner of each concerned module.



**Pay attention : some Knime node are not converted but however pass the quickscan test (eg. Rule based row filter). When testing the converter, keep this in mind, it could be a reason for rising up errors there**







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="errors-while-running-_interactions">Errors while running _interactions</h1></th></tr></thead><tbody></tbody></table>



Once Quickscan run without error messages, you can update _interactions and run it.



**Make sure you have modified the json file for the lever position !** More information : [Lever_position.json](onenote:#Lever_position.json&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E8945928-71D4-43AA-A17B-E6EC07067912}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)

**Make sure you get the latest version of interface !** More information : [Interface](onenote:#Interface&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={7BC1480A-6DB9-47A4-9E7A-E611C53E33DA}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)

**If the release is for a PowerBI visualization : make sure you properly extract the json files of each scenario**. More information : [Scenario Builder](onenote:#Scenario%20Builder&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={E0C26574-D2CA-4B89-B165-ADED2D317096}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



Note : when the validation of data with interface (you already get the latest version !), if some errors rise up, this could come from the fact the interface google sheet was not updated. This happens when we change a module but we did not run the "Update Interface" metanode.





**If output result are not updated** although input data and/or model was updated :


-   Check if you run the data-preprocessing for these modules to be sure you get the latest input data up-to-date

Example when data-preprocessing has not been run : it cannot find some column names :

![File "C:  return self. engine . get loc (self. maybe cast indexer (key) ) File "pandas/ Iibs/index.pyx", line 108, in pandas. Iibs . index. IndexEngine . get File "pandas/ Iibs/index.pyx", line 132, in pandas. I ibs . index. IndexEzwine . File "pandas/ Iibs/hashcable class helper .pxi" line 1601, in pandas. I ibs . has " , I ine 1608, in pandas. Iibs.has electricity calibration' ](../media/Technical-Documentation-Common-debugging-steps-image4.png){width="5.0in" height="1.3125in"}


-   Check if metanodes for these projects where shared (if not, your update will not really be an update...).
-   Be sure you pull the latest version of these modules
-   Be sure you updated each metanodes (Cfr CTRL + ALT + U).



**If errors in Knime while running it** :


-   Sometimes, modelers do **modifications outside a shared metanode** (for instance, they add a row filter just before it -- or --there is a new input/output port), therefore this is not automatically updated in _interactions.

Change it in their module workflow : modifications should be as much as possible always set in a metanode.

Change _interactions to reflect this modifications :


-   **Data-pre-processing** :

Create a new csv file if required. **/! before saving at table in a csv file that will be used in data-processing, add "DataTypeSelection" metanode** (in _common/lib/metanodes) and specify the data type (eg. ll, ots or cp). Cfr : [Recommendations for pre-processing metanode](onenote:https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/User%20Manual/User%20Manual%20Doc.one#Best%20practices&section-id={E8711A28-6764-4229-9950-3955E233C55E}&page-id={5F05C40F-9FEA-43F2-9F8E-4A6C16B24526}&object-id={7B5A53E6-70D4-0BD5-1D6E-AE1AD6918F1B}&52).

**Note** : *Data writer (CSV)* node need a flow variable named "table_name" for running. Make sure this flow variable exists (right click on the table > Flow Variables > Lookfor table_name in Name column).

![Table •default' -Rows: 92 Spec - Columns: 190 Proper bes Flow Variables 0 4:30: 1455 0 4:30: 1455 0 4:30: 1455 0 4:30: 1455 0 4:30: 1455 0 4:30: 1455 table name googe_ eet_pre x • db schema levers • db_port S• db host test mode country_fiter seyear knime vvorkspace eet name bld_fixed-assumptions levers localhost IrKV4Q6J5VRhMjHF4BlE8M1v-fi ](../media/Technical-Documentation-Common-debugging-steps-image5.png){width="5.0in" height="1.3958333333333333in"}


-   **Data-processing** :

    -   In DB component : read the new csv file (if any) and create a new output port if required

    -   In DB : add some other nodes if required (eg. *Convert double (when read from csv) to integer (when read in string manipulation)* with a python node)

    -   Link new module node to new output/input ports in any




-   In data import part (cfr data-preprocessing), **/! before saving at table in a csv file that will be used in data-processing, add "DataTypeSelection" metanode** (in _common/lib/metanodes) and specify the data type (eg. ll, ots or cp). Cfr : [Recommendations for pre-processing metanode](onenote:https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/User%20Manual/User%20Manual%20Doc.one#Best%20practices&section-id={E8711A28-6764-4229-9950-3955E233C55E}&page-id={5F05C40F-9FEA-43F2-9F8E-4A6C16B24526}&object-id={7B5A53E6-70D4-0BD5-1D6E-AE1AD6918F1B}&52).

This should be done in the module workflow and be shared in their metanodes !


-   Some errors are not detected by Quickscan, such as use of wrong regex.

In this case, look which node is responsible of stopping _interactions running and fix it.

Example : When a Math Formula gives wrong results (in python compared to Knime) or does not work, this often comes from a Column Filter using a wrong regex just before the Math Formula. Change the regex to be more readable and/or inclusive.





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="errors-while-testing-knime-to-python-converter">Errors while testing Knime to Python converter</h1></th></tr></thead><tbody></tbody></table>



<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><p><strong>Tips</strong>:</p><p>When error pop up, check the node number and not the node name (as sometimes the node name is not the right one).</p><p></p><p><strong>Example</strong> : Here, there is an exception with DB - Power S (<strong>3866</strong>) / AIR_FINAL_OT (<strong>0</strong>) /…</p><p>In fact node 3866 corresponds to DB_AIR_QUALITY. Therefore, in Knime, look for "<strong>3866</strong>" (with CTRL + F), open the node and then look for "<strong>0</strong>", and so on.</p><p></p><p><img src="../media/Technical-Documentation-Common-debugging-steps-image6.png" style="width:12.25in;height:0.82292in" /></p><p></p></th></tr></thead><tbody></tbody></table>


-   Math Formula (multiple columns) : while using all columns in a REGEX, Knime will only consider integer columns <> converter which will consider all columns included string type. In this case, change the regex in Knime to make it more specific to the columns that need to be used (eg. .* will include "Years" and "Country", make a regex that exclude these two columns).


-   Math formula (or other type of formula) are not properly written in the Python codes :

When a math formula is used, it should be written here (self.specific_expression and in the definition of math_multi_columns function) :

![Texte de remplacement généré par une machine: datareader_ots.py # Preparing the evaluaticn cf self. specific_expressions gtap_node.py joiner_node.py meta_node.py metaconnector_node.py the expression by renaming cclumns and validate math operaticns COLUMN$$) "1-if($$CURRENT COLUMN$$) "if isNaN a if islnfinite URRENT COLUMN URRENT COLUMN "I-$$CURRENT COLUMN$$", "$$CURRENT COLUMN$$", "$$CURRENT COLUMN$$*IBBB", "$$CURRENT COLUMN$$*1e-12", "$$CURRENT COLUMN$$*1e6/36aa", URRENT C LUMN "$$CURRENT COLUMN$$*1e9", "$$CURRENT COLUMN$$*1e-9", "$$CURRENT COLUMN$$/IBBB", "if($$CURRENT COLUMN$$) "I-(I-$$CURRENT COLUMN$$)n5", "(I-$$CURRENT COLUMN$$)n5", "$$CURRENT COLUMN$$/5", "$$CURRENT COLUMN$$*1e3", "$$CURRENT COLUMN$$* "$$CURRENT COLUMN$$* "if isNaN URRENT "$$CURRENT COLUMN$$ URRENT COLUMN ulaticn in abitants CURRENT COLUMN COLUMN loa ca rat e em URRE T COLUMN "$$CURRENT "$$CURRENT "$$CURRENT "$$CURRENT "$$CURRENT "$$CURRENT "$$CURRENT COLUMN$$* COLUMN$$* COLUMN$$* COLUMN$$* COLUMN$$* COLUMN$$* ==I a if islnfinite COLUMN URRENT * 8.766", COLUMN$$) Scal rate Scal rate Scal rate $tra_fuel-mix Scal Scal rate rate 10 1 ese ](../media/Technical-Documentation-Common-debugging-steps-image7.png){width="10.270833333333334in" height="3.6041666666666665in"}



![Texte de remplacement généré par une machine: elif self. expression df.IocC:, cols) elif self. expression df.IocC:, cols) elif self. expression df.IocC:, cols) elif self. expression df.IocC:, cols) elif self. expression df.IocC:, cols) elif self. expression df.IocC:, cols) elif self. expression df.IocC:, cols) elif self. expression df.IocC:, cols) elif self. expression df.IocC:, cols) elif self. expression dfCcoIs) = dfCcoIs) elif self. expression "$$CURRENT COLUMN$$*IBBB": = df.IocC:, cols) * "$$CURRENT COLUMN$$*1e-12": = df.IocC:, cols) "$$CURRENT COLUMN$$*1e6/36aa": = df.IocC:, cols) * Les / "$$CURRENT COLUMN$$*1e9": = df.IocC:, cols) "$$CURRENT COLUMN$$*1e-9": = df.IocC:, cols) * Le-9 "$$CURRENT COLUMN$$/IBBB": = df.IocC:, cols) / "I-(I-$$CURRENT COLUMN$$)n5": (I - df.loc[:, cols)) * "(1 -$$CURRENT COLUMN$$) - df.IocC:, cols)) * "$$CURRENT COLUMN$$/5": = df.IocC:, cols) / 5 "$$CURRENT COLUMN$$*1e3": "$$CURRENT ](../media/Technical-Documentation-Common-debugging-steps-image8.png){width="5.1875in" height="4.458333333333333in"}




-   If "Assemble all Pathway Explorer" component was modified to add/remove an input port (eg. Occurs when new module), it should be specify also in the python code named "**assemble_all_input_metanodes.py**".

Example : New input port (for node 874) : should be precised in **assemble_all_input_metanodes.py - class assembleAllOutputNode**



![Visut semMe Pathway output ("74) ](../media/Technical-Documentation-Common-debugging-steps-image9.png){width="1.8958333333333333in" height="1.9583333333333333in"}



![class : (self, u, •L file, node _ type, (i d, xml_file, node_type, 2: None, 3: S: None, 7: None, 8 : 'kne, g : None, lø:uone, 12 : def start timer() t • start self. (t, "01JTL0", ' ' ( EMPTY) 811 Output ](../media/Technical-Documentation-Common-debugging-steps-image10.png){width="10.114583333333334in" height="3.46875in"}



If not done, error message rise up : Empty node_874_in_12 (= node number 874 for input port number 12) connected to node_3886_out_1 (output port 1 of the node number 3886) :

![2e2ø-a4-ø3 1m 10:09 Traceback (most recent call last): src. n INFO RUN 6 2 Air self. Ntput, update) line Exception: Empty edge ( ' node_3886_ out 1', 'node 874 in 12') (#3886) n run node " • self. xml_file) During handling of t ceback recent app = S: 1000 p exception occ call last): •p t line 28, in e. 61131 bypass _ 2019.3.4 available // Updete„ (to extra_scenarios = not run _ args Console day 1204) ](../media/Technical-Documentation-Common-debugging-steps-image11.png){width="7.447916666666667in" height="3.1041666666666665in"}




-   Node number are not properly set in python code. This occurs when we update a module shared metanode manually. This action could change the node number and so you have to change this number in python code as well.


-   When **outputs are different between Knime and python** :

    -   _interactions has not been saved : if not saved, python will read a xlm file that is not the up-to-date one. Therefore, the comparison between output in Knime and python will rise up some errors. In this case, save _interactions and run the test again.

    -   Some REGEX / Math Formula are not properly written leading to some wrong calculations in Python (eg. In the definition of math_multi_columns function : df[cols] = ... where ... is not the right expression)





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="errors-while-testing-the-api">Errors while testing the API</h1></th></tr></thead><tbody></tbody></table>



WHAT ?





<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="errors-while-releasing-with-mobaxterm">Errors while releasing with MobaXTerm</h1></th></tr></thead><tbody></tbody></table>



**Before releasing a new version of Knime process, make sure :**
-   **You pushed knime2python folder and _interactions on remote (from your working environment)**
-   **You pulled knime2python folder on the remote server used for this project (_interactions will be pulled automatically here).**





WHAT ?











