<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Read me](#read-me)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Read me

Created: 2020-04-15 08:31:58 +0200

Modified: 2021-09-13 15:15:40 +0200

---


This section is about all the tools and actions required to go from a Knime process to a web application/PowerBI.

It contains :
-   Common debugging steps that occur while deploying and updating our calculator : [Common debugging steps](onenote:#Common%20debugging%20steps&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={AEAD3A3D-8DEA-4DAA-95C4-0F8C234BBE1A}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)
-   What are the steps when you create a new project : [Create new project](onenote:#Create%20new%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={2BBA0D6C-CB72-46C2-B669-136224CC3529}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)
-   What are the steps when you want to update your calculator (new lever, new metrics, others graphs, new module, ...) : [Updating existing project](onenote:#Updating%20existing%20project&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={687D9522-BD6D-49C2-8DF3-23154AA366D9}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)
-   How to deploy your web application/PowerBI from Knime structure to a user friendly interface : [Deployment](onenote:#Deployment&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={CEC2EE8D-7D22-45A4-B450-DFEEEF0C3DCD}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)
-   What are the tools we need ? How to use them ? What are the best practices, ... ? : [Technical tools](onenote:#Technical%20tools&section-id={A08983CB-CB3C-45A7-8C14-FDA21CE44311}&page-id={FE269F73-9506-41E8-8023-C83EC9DD49F3}&end&base-path=https://climact.sharepoint.com/sites/PROSPECTIVE/Documents%20partages/XCalc/Technical%20Tools/Technical%20Documentation.one)



On the figure below, you have a brief view of all the steps that are required for the deployment.

More details are described in the following sections and are also furnished at (XCalc Architecture) : <https://drive.google.com/file/d/1aIIfk3mpnqURMAxECwpbaQff70rQc4lB/view>





![LOCAL DESKTOP User 1 REMOTE LOCAL DESKTOP User 2 REMOTE Modification in workflow (ex. bld) /! Shared nodes when modifications are done /! Commit and Push modified workflow (ex. bld) Remote server Pull modified workflow (ex. bld) Updates interactions nodes In order to catch modifications in shared nodes (ex. bld) Run data-preprocessing Run data-processing Check it properly runs Save data-processing Check converter (knime2python) properly runs Commit and Push interactions Commit and Push knime2python (if modified) Remote server Run all the process to update PowerBi/web site ](../media/Technical-Documentation-Read-me-image1.png){width="6.520833333333333in" height="5.3125in"}





The whole process to go from Input Data (Google Sheets) to a Web Application or a PowerBI looks like :

<table><colgroup><col style="width: 45%" /><col style="width: 54%" /></colgroup><thead><tr class="header"><th><p><img src="../media/Technical-Documentation-Read-me-image2.png" style="width:5.61458in;height:3.125in" alt="Texte de remplacement généré par une machine: GOOGLE SHEETS Module I Module N Input data : - Historical - Levers - Constants KNIME File Reader File Reader Column Filter Column Filter xlm files interactions Module workflows " /></p><p></p></th><th><p><img src="../media/Technical-Documentation-Read-me-image3.png" style="width:6.71875in;height:3.01042in" alt="Texte de remplacement généré par une machine: PYTHON Tests : : read xim files - Knime nodes used are available in the converter - Output values are the same in Knime and the converter - Output values are the same for two runs of a unique scenario - Website shows properly the results If yes to all : realease REMOTE LINUX SERVER MOBAXTERM ocker CONVERTER ocker Knime (xml) —+ Python ocker WEBSITE POWERBI " /></p><p></p></th></tr></thead><tbody></tbody></table>



