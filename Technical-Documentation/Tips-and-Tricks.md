<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Tips and Tricks](#tips-and-tricks)
  - [Add new member to a project](#add-new-member-to-a-project)
  - [](#)
  - [](#-1)
  - [](#-2)
  - [](#-3)
  - [Clone/Pull/Push automatically for several modules](#clonepullpush-automatically-for-several-modules)
- [PyCharm](#pycharm)
  - [Create a function based on a part of a code](#create-a-function-based-on-a-part-of-a-code)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Tips and Tricks

Created: 2020-05-04 16:33:52 +0200

Modified: 2020-05-04 16:52:50 +0200

---




## Add new member to a project

Go to the repository "Climact" > Members

![Texte de remplacement généré par une machine: O Climact Repositories Projects Snippets Mem bers Settings Climact Members Vincent Matton ](../media/Technical-Documentation-Tips-and-Tricks-image1.png){width="4.625in" height="2.7604166666666665in"}



Then choose Manage User and Add Users

![Texte de remplacement généré par une machine: Manage users Q Find members ](../media/Technical-Documentation-Tips-and-Tricks-image2.png){width="2.375in" height="1.2708333333333333in"}

## 

![Texte de remplacement généré par une machine: User groups Add member Add group With user groups, you can organize workspace members, specify workspace permissions, and grant access to workspace repositorles. When you add user groups to a private repository your workspace owns, those users count towards your monthly plan. Learn more Group Administra tors Developers Members ADMIN Repositories 17 17 ](../media/Technical-Documentation-Tips-and-Tricks-image3.png){width="7.385416666666667in" height="2.6041666666666665in"}

## 

## 

## 

## Clone/Pull/Push automatically for several modules

If you want to execute an identical command line for several modules, you can use several batch code that have been created on this purpose (C:/becalc/scripts).

Before running them, pay attention you change what is needed to be changed (ex. commit message !).



Example : add all, commit and push. Here, the code will look for each folder in C:becalcdev and for each of them, it will add --all, commit the same message (cfr red frame) and then push.

![Texte de remplacement généré par une machine: His Accueil Téléchargem mages Partage Ce PC Affichage Gérer Outils d'application Disque local becalc GIT ADD COMMIT PUSH.bat GitPULL.bat Œ] GitRESETandPULL.bat scripts scripts Modifié le 27-04-20 1218 06-12-1909:40 06-12-19 11:01 Rechercher clans : scripts Type Fichier de comma... Fichier de comma... Fichier de comma... Taille I Ko GIT ADD COMMIT PUSH.bat Bloc-notes Fichier Edition Format Affichage Aide FOR in Do call git add -all call git ommit -am "Release 1 call git push call git tag o v6.1&c ](../media/Technical-Documentation-Tips-and-Tricks-image4.png){width="9.135416666666666in" height="2.5625in"}





To use them, open a command line window. Then drag and drop the needed bat file there.

For each code, do it twice to be sure you get the error messages if any. Indeed, with the first run, there are so many comments that an error can be easily skipped. With the second run, only errors will appear.







<table><colgroup><col style="width: 100%" /></colgroup><thead><tr class="header"><th><h1 id="pycharm">PyCharm</h1></th></tr></thead><tbody></tbody></table>



## Create a function based on a part of a code

If you want to create a function based on a part of a code (because you need to use more than once this part of this code) :

1.  Select the part of the code you want to encapsulate

2.  Right Click > REFACTOR > Extract Method

3.  Choose the method_name : corresponds to the name of the function you want to create








