<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [New module => prototype](#new-module--prototype)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# New module => prototype

Created: 2021-01-26 09:57:39 +0100

Modified: 2021-02-12 12:49:28 +0100

---


Dans Quickscan : ajouter le module (cfr 2 x transport-prototype)



![Texte de remplacement généré par une machine: = {'agriculture' • 'metanodes', 'buildings' • 'metanodes', 'climate'• 'metanodes', 'electricity_supply'• 'metanodes' , 'industry'• 'metanodes', 'lifestyles'• 'metanodes', 'macroeco'• 'metanodes', 'transport' • 'metanodes', 'minerals' • 'metanodes' ' technology " 'metanodes', 'transport_prototype'• ' metanodes' , 'air_pollution'• 'metanodes', = {'agriculture' ['4.1 Land-use', ['2.1 Buildings'], ' buildings' '4.3 Agriculture', '4.6 Balance (after power)' , ['1.2 Climate', '1.4 Climate Emissions'], ' climate' ['5.@ Power Supply'], ' electricity_supply' ['3. la Industry', '3. lb Industry Ammonia'], 'industry' ['1.1 Lifestyle'], 'lifestyles' ['6.5 Macro Economy'], 'macroeco' ['1.3 Technology'], ' technology' ['2.2 Transport'], ' transport' ['2.2 Transport'], ' transport_prototype ' ['6.2 Air pollution'], ' air_pollution' ['4.2 Minerals']} ' minerals' ](../media/Technical-Documentation-New-module-=--prototype-image1.png){width="10.104166666666666in" height="5.364583333333333in"}





![Texte de remplacement généré par une machine: x calibration. py src.nodes.api_interface_metanode import APIinterfaceNode node_builder.py -from from from from from from from from from from from from from from from from from from from from from from import AssembleA110utputNode import BuildCubeFormatNode src.nodes. import CalibrationMuItiDimensionNode src.nodes.calibration_node import CalibrationNode import CellSpIitterNode import ColumnAggregatorNode import ColumnAppenderNode import ColumnMergerNode src.nodes.constant_import_node import ConstantImportNode import ConstantVaIueCoIumnNode import CostCaIcuIationNode src.nodes.cross_joiner_node import CrossJoinerNode import DataVaIidationNode src.nodes.database delete node imoort DatabaseDeIeteNode . nodesl. datareader_cp import DataReaderCP src src.nodes.datareader_fts Import DataReaderFTS src.nodes.datareader_ots import DataReader0TS import DoubleToIntNode import ExcelReaderNode import ExcelWriterNode import ColumnRenameNode import ColumnRenameRegexNode ](../media/Technical-Documentation-New-module-=--prototype-image2.png){width="9.822916666666666in" height="6.125in"}



![Texte de remplacement généré par une machine: x x calibration.py node_builder.py if node_type # Standard Metanode (not wrapped) "MetaNode" return MetaNode(id, relative_path, node_type, knime_workspace, self .db_host, self . db_port, self . self .db_password, self .db_schema, elif node_type # Components Metanode (wrapped) " SubNode " relative_path . split(' /settings ' ) [0] workflow_path os . 'workflow. knime') tree ET. root = tree.getroot() subnode_name = root.find(self .xmlns + # The following Components are coded directly in the converter to allow faster conversion. self . db_user, It allows if subnode_name on in the converter "Tree aggregator (Java)" return TreeAggregatorNode(id, relative_path, node_type, knime_workspace) return TreeMergeNode(id, relative_path, node_type, knime_workspace) elif subnode_name - Tree parallel operation" " EUcauc return TreeParaIIe10perationNode(id, relative_path, node_type, knime_workspace) elif subnode_name - Tree split"• " EUcauc return TreeSpIitNode(id, relative_path, node_type, knime_workspace) elif subnode_name "Oatalmport" return DataImportNode(id, elif subnode_name " EUcauc relative_path, node_type, knime_workspace) Data Validation (All Countries and Years)" ](../media/Technical-Documentation-New-module-=--prototype-image3.png){width="11.197916666666666in" height="6.03125in"}





Quand on fait tourner quickscan => indique si nœud existe ou pas (ne fait pas tjs tous les neouds en 1 fois => il faut le refaire tourner plusieurs fois !)

Ici : Column Selection Configuration - numéro 14



![- transport_prototype 2.2 Transport ERROR Error while parsing the following metanode . 2.2 Transport Unknown node type: Column Selection Configuration ERROR Column Selection Configuration - air_pollution 6.2 Air pollution - ](../media/Technical-Documentation-New-module-=--prototype-image4.png){width="11.447916666666666in" height="1.125in"}



Vient des noeuds créés mais non détectés comme metanodes : => Cfr ci-dessus : il faut les ajouter dans node_builder (// à EUCalc - Tree parallel operation, ...)

![de la recherche dans 2.2 Transport e lentes car l'indexation n'est pas en cours d'exécution. Cliquez pour activer l'indexation... Column Selection Configuration (#14) Modifié 10-02 21 09:15 Column Selection Configuration (#14) Column Selection Configuration (#14) Modifié 10-02 21 0915 Column Selection Configuration (#14) TransportExport Varia (#5902) Transport Export Varia (#5901) TransportExport Varia (#5900) TransportExpart Varia (#5899) ](../media/Technical-Documentation-New-module-=--prototype-image5.png){width="10.4375in" height="2.5729166666666665in"}



![Texte de remplacement généré par une machine: n ode_builder.py x fuel_switch.py fuel_mix.py calibration.py # that won't be converted in the converter return QualityCheckNode(id, relative_path, node_type, knime_workspace) f "Temp_node" in subnode_name: # The JÊ!upnpgÊ is an empty node in the converter. I allows the user to make tests converted in the converter r urn TempNode(id, relative_path, node_type, knime_workspace) euif ring_Manipu1ation" in subnode_name: retu elif "Tim return #elif "6.5 elif subno retur StringManipuIationMetaNode(id, relative_path, node_type, Benchmark" in subnode_name: mpNode(id, relative_path, node_type, knime_workspace) in subnode name: Variable" knime_workspace) UseVariab1eNode id, relative_path, node_type, knime_workspace) ellf subnode_name " MCO" • return MCDNode(id, relative_path, node_type, knime_workspace) elif subnode_name "Export Variable" return ExportVariabIeNode(id, relative_path, node_type, knime_workspace) elif subnode_name - Calibration" " xcauc return XCaLcCaLibrationNode(id, relative_path, node_type, knime_workspace) ](../media/Technical-Documentation-New-module-=--prototype-image6.png){width="8.135416666666666in" height="4.875in"}



![Texte de remplacement généré par une machine: quickscan.py import node_builcler.py ail use_variable.py fuel_switch.py x fuel_mix.py x calibration.py clas UseVariabIeNode(Node) : def _ (self, id, xml_file, npAÊ.gypÊ, self . super(). (id, xml_file, node_type, knime_workspace) def init_ports(self): self.in_ports = {1: None} {1: None} self. out_ports = def - timer() start model = self .xmlns + Find values of the flow variables provided by user ](../media/Technical-Documentation-New-module-=--prototype-image7.png){width="7.59375in" height="4.875in"}



Les classes de nœuds sont à importer au début du code node_builder :

![Texte de remplacement généré par une machine: src.nodes.group_by_node import GroupByNode src.nodes.concatenate_node import ConcatenateNode src.nodes.tree_split_node import TreeSpIitNode src.nodes. import TreeAggregatorNode src.nodes.tree_merge_node import TreeMergeNode src.nodes import TreeParaIIe10perationNode ode import DataImportNode 110 quickscan.py trom from from from from from from from from from from from from from from nocIE_buiIcIEr.py fuel_switch.py x fuel_mix.pyx calibration.py orter_noae Import coLumnResorterNoae src.nodes.write_to_dat base_ teTODBN0de src.nodes. use_variable impor UseVariab1eNode src.nodes.export_variable import ExportVariabIeNode src.nodes.mcd import MCDNode src.nodes. calibration import XCaIcCaIibrationNode src.nodes.fuel_mix import FuelMixNode src.nodes.fuel_switch import Fuel SwitchNode import os ](../media/Technical-Documentation-New-module-=--prototype-image8.png){width="7.458333333333333in" height="4.177083333333333in"}





Suite des erreurs :

Nœuds utilisés qui ne sont pas implémentés (String Manipulation => remplacé par nœud python pour le moment -- Math formula à noter dans le nœud python Math formula)

+ Nœud 665 = nouveau nœud non convertit en python (lié à Fuel Switch et Fuel Mix)



![Texte de remplacement généré par une machine: - transport_prototype 2.2 Transport - The String Manipulation node is not yet implemented. The xmI file is TransportLFS ERROR The String Manipulation node is not yet implemented. The xmI file is TransportLFS ERROR The operation if( has not been implemented yet (Math formula node #4971) ERROR The operation > has not been implemented yet (Math formula node #4971) ERROR The operation has not been implemented yet (Math formula node #4971) ERROR The operation has not been implemented yet (Math formula node #5019) ERROR The operation < has not been implemented yet (Math formula node ERROR The operation has not been implemented yet (Math formula node ERROR The operation < has not been implemented yet (Math formula node ERROR The Merge Variables node is not yet implemented. The xmu file is Transportxcauc ERROR ERROR Error white parsing the following metanode . 2.2 Transport Unknown node type: Couron Selection Configuration .xrau Couran Selection Configuration .xrau ERROR to TRA TRA (#3853)/String Manipulation Manipulation Fuel Variables air_pollution 6.2 Air pollution ](../media/Technical-Documentation-New-module-=--prototype-image9.png){width="16.875in" height="4.59375in"}



Pour les math formula :

![Texte de remplacement généré par une machine: n.py• node_builder.py self. specific_expression fuel_switch.py fuel_rnix.py calibration.py "if vehicle-fleet-historical number vehicle-fleet-historical number quickscan veh- leet-future number New trans ont "max in ar s @ dh enep "max ln "mln ln ar s @ floor-area floor-area- revlous ar sl floor-area -area trans ort rotot e -demand Tl'lh eap dh enep -denand heat-contpibution T eap -increase MP12 + demolished-area Mn2 -old-stock [Mm2] S-Sf100r-area-01d-stock-renovated [Mm2] S) " , * 1+ demolition-rate exi eriod-duration -1 +-min in ar s @ constructed-area-acc Mrn2 + renovated-area-acc H 2 -demand Tl'lh eap floor-area-increase Mn2 ](../media/Technical-Documentation-New-module-=--prototype-image10.png){width="13.510416666666666in" height="2.4166666666666665in"}



![Texte de remplacement généré par une machine: X node_builder.py x fuel_switch.py fuel_rnix.py s calibration.py rename dict ** 'NEW COLUMN' df . rename self.re laced column if self. convert_to_int 'true' df[self . replaced_column] df[self . replaced_column] . erse : "if Svehicle-fleet-historical nur.lber if self.splitted @ Svehicle-fleet-histopical_ nunbep veh-flee -future nu ber . rep ace _co umn mask df . Loc mask self.re laced column df.loc mask ellf self. splitted df aself. replaced_couumn] mask df . Loc [mask, self. replaced_column] - elif self. splitted orica num er "veh-fleet-future number New . trans ort rotot ](../media/Technical-Documentation-New-module-=--prototype-image11.png){width="11.291666666666666in" height="3.4791666666666665in"}





![Texte de remplacement généré par une machine: Column Selection Configuration (#665) Modifié 10-02-21 0937 Column Selection Configuration (#665) Modifié 10-02 21 0937 Column Selection Configuration (#665) Modifié 10-02-21 0937 Column Selection Configuration (#665) Modifié 10-02-21 0937 TransportXCaIc TransportXCaIc Q  TransportXCaIc TransportXCaIc Fuel (#5096) Fuel (#5067) Fuel (#5061) Fuel (#5060) ](../media/Technical-Documentation-New-module-=--prototype-image12.png){width="8.78125in" height="2.1979166666666665in"}



Pour le Fuel Switch / Mix

C'était lié au nom mal écrit dans node_builder => correction du code = ok

![Texte de remplacement généré par une machine: - Fuel Switch F•.Â Switch fff•.XE to Ef•XE ](../media/Technical-Documentation-New-module-=--prototype-image13.png){width="4.114583333333333in" height="1.125in"}



![Texte de remplacement généré par une machine: 50611 - Fuel Mix F•.Â fff•.XE to (2 SS OM 0611 ](../media/Technical-Documentation-New-module-=--prototype-image14.png){width="3.8854166666666665in" height="0.9791666666666666in"}





Quand erreur peu détaillée comme ci-dessous en rouge (seul message affiché = "Error while parsing the following metanode - NoneType object has no attribute find", il peut être utile de faire un print du nom des nœuds pour voir exactement lequel est problématique :

![_init_.py tests quickscan build build build build build build build while parsing the following metanode . 2.2 'NoneType' object has no attribute 'find' token.pickle .•øaram knime_workspace: the path to the knime workspace db :return: return the python version of the knime node KNIME_project build", relative_path) nodes workflow _init_.py workflow_builder.py workflow_runner.py Transport!iMath Formula Transportstring To Number if node_type "MetaNode" # Standard Metanode (not wrapped) return etaNode(id, relative_path, node_type, knime_workspace, self . db_ self .db_password, self .db_schema, self . google_stl etif no e_type # Components Metanode (wrapped) - "SubNode" KnimeNode8uiIder buildO ) elf "Merge Variables" in node... Transportcolumn Rename _Regex_ Transportcolumr. Rename _Regex_ Rename _Regex_ Transportcolumn Rename _Regex_ Transportpwthon Script _1_1_ limn r #595 Transport . xmI (#5951) /settings . xml (#5952) /settings . xmI (#5953) /settings . xmI air_pollution start 6.2 Air pollution ](../media/Technical-Documentation-New-module-=--prototype-image15.png){width="12.791666666666666in" height="6.125in"}



=> Ici, lié au fait que String to Number = deprecated or dans prototype, on utilise le nouveau string to number => il faut utiliser le deprecated = celui qui est effectivement convertit dans le convertisseur !







TEST MODULE CONVERTER :

![Error Traceback (most recent call last): File "C: runner .øv", self . output, output_nodes, update) File runner .øv", line 63, line 89, . This is usually caused by unconnected node (in or out)" Exception: Empty node_@_in_2: edge , During handling of the above exception, another exception occurred: Traceback (most recent call last): File yield File testMethod() in sequential_run in activate_node . File: + self line 59, in testPartExecutor tine 615, in run File module converter.Dv", line 70, in ](../media/Technical-Documentation-New-module-=--prototype-image16.png){width="11.40625in" height="3.7291666666666665in"}


















